<?php

namespace App\Listeners;

use App\Events\DeleteUserAccount;
use App\Models\ActivityNotification;
use App\Models\ApplicationNotification;
use App\Models\ArchiveItem;
use App\Models\BlockUser;
use App\Models\Favorite;
use App\Models\Feedback;
use App\Models\Follower;
use App\Models\Item;
use App\Models\ItemPhoto;
use App\Models\Rack;
use App\Models\ReportUser;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use ZipStream\Option\Archive;

class StartDeleteProcess
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DeleteUserAccount  $event
     * @return void
     */
    public function handle(DeleteUserAccount $event)
    {

        DB::beginTransaction();
        try {
            //ApplicationNotification
            ApplicationNotification::where('user_id',$event->user->id)->delete();
            \Log::info("ApplicationNotification deleted");
            //Item
            $items=Item::where('user_id',$event->user->id)->get();
            foreach($items as $item)
            {
                updateFirebaseItemDocumentDelete(Item::find($item->id));
            }
            Item::where('user_id',$event->user->id)->delete();
            \Log::info("Item deleted");
            //ItemPhoto
            ItemPhoto::where('user_id',$event->user->id)->delete();
            \Log::info("ItemPhoto deleted");
            //ReportUser
            ReportUser::where('user_id',$event->user->id)->delete();
            \Log::info("ReportUser deleted");
            //ActivityNotification
            ActivityNotification::where('user_id',$event->user->id)->delete();
            \Log::info("ActivityNotification deleted");
            Rack::where('user_id',$event->user->id)->delete();
            \Log::info("Rack deleted");
            ArchiveItem::where('user_id',$event->user->id)->delete();
            \Log::info("ArchiveItem deleted");
            BlockUser::where('user_id',$event->user->id)->delete();
            \Log::info("BlockUser deleted");
            Favorite::where('user_id',$event->user->id)->delete();
            \Log::info("Favorite deleted");
            Feedback::where('user_id',$event->user->id)->delete();
            \Log::info("Feedback deleted");
            //Follower 
            Follower::where('from_user_id',$event->user->id)->delete();
            \Log::info("Follower deleted");
            // Following 
            Follower::where('to_user_id',$event->user->id)->delete();
            \Log::info("Follower deleted");
            //ApplicationNotification
            ApplicationNotification::where('user_id',$event->user->id)->delete();
            \Log::info("ApplicationNotification deleted");

            $user=User::where('id',$event->user->id)->delete();
            \Log::info("json En".json_encode($event->user));
            if(isset($event->user->firebaseId))
            {
                \Log::info("Firebase deleted");
                updateFirebaseDeleteUser($event->user->firebaseId);
            }
            User::where('id',$event->user->id)->delete();
            \Log::info("User deleted");
            DB::commit();
            
        } catch (\Exception $e) {
            \Log::info($e);
            DB::rollback();
        }
    }
}
