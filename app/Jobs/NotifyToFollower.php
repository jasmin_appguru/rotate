<?php

namespace App\Jobs;

use App\Models\ActivityNotification;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyToFollower implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user;
    public $item;
    public function __construct($user,$item)
    {
        $this->user=$user;
        $this->item=$item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $followers=ActivityNotification::where('user_id',$this->user->id)->whereNotIn('user_id',SelectedUserHiddenUserList($this->user->id))->get()->pluck('notify_id');
        $users=User::where('status',User::ACTIVE)->whereIn('id',$followers)->get();

        if ($users->isNotEmpty()){
            foreach ($users as $user){
                SendNotification(__('api.followed_user_new_item',['USERNAME' => $this->user->user_name]),$user->id,true,Notification::FOLLOWED_USER_ADD_ITEM,"Default",$this->item->id,$this->user->user_name,$this->item->roles_list);
                \Log::info(json_decode($this->item));
            }
        }
        // Marie Hayes uploaded a new item
    }
}
