<?php

namespace App\Jobs;

use App\Models\Favorite;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyFavoritesUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $item;
    public function __construct($item)
    {
        $this->item=$item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fav=Favorite::where('item_id',$this->item->id)
        ->whereNotIn('user_id',SelectedUserHiddenUserList($this->item->user_id))
        ->get()->pluck('user_id');
        $users=User::where('status',User::ACTIVE)->whereIn('id',$fav)->get();
        // \Log::info(json_encode($users->toArray()));

        
        if ($users->isNotEmpty()){
            foreach ($users as $user){
                SendNotification(__('api.favorite_item_on_sale',['CATNAME' =>$this->item->SubCategory->name]),$user->id,true,Notification::FAVORITE_ITEM,"Default",$this->item->id,null,$this->item->roles_list);
            }
        }

    }
}
