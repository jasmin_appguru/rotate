<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Item;

class changeIteamStatusFirebase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $userId;
    public $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$action)
    {
        $this->userId=$userId;
        $this->action=$action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $item=Item::where('user_id',$this->userId)->get();
        foreach($item as $it)
        {
            if($this->action=="show")
            {
                \Log::info(json_encode($it));
                updateFirebaseItemDocument($it);
            }
            else
            {
                \Log::info(json_encode($it));
                updateFirebaseItemDocumentHide($it);
            }
        }
        //\Log::info(json_encode($item));
    }
}
