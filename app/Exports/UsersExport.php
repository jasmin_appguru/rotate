<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use App\Models\User;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements  FromCollection, WithHeadings, WithMapping
{
    protected $sr;

    public function __construct()
    {
        $this->sr = 0;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::get();
    }

    public function map($user): array
    {
        $this->sr++;
        return [
            $this->sr,
            $user->first_name,
            $user->last_name,
            $user->email,
            (string)$user->mobile_number,
        ];
    }

    public function headings(): array
    {
        return [
            'Sr. No',
            'First Name',
            'Last Name',
            'Email',
            'Mobile',
        ];
    }
}
