<?php

namespace App\Console;

use App\Console\Commands\BirthdayNotification;
use App\Console\Commands\PostingFirstItemReminderNotification;
use App\Console\Commands\PostingItemReminderNotification;
use App\Console\Commands\ReferralReminderNotification;
use App\Console\Commands\UnblockUser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\DailyQuote::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('queue:work', [
            '--max-time' => 300
            ])->withoutOverlapping();

        // $schedule->command('quote:daily')->everyMinute();
        
        $schedule->command(UnblockUser::class)->daily();
        $schedule->command(BirthdayNotification::class)->daily();
        $schedule->command(ReferralReminderNotification::class)->daily();
        $schedule->command(PostingItemReminderNotification::class)->daily();
        $schedule->command(PostingFirstItemReminderNotification::class)->daily();
        $schedule->command(LikeSummaryReminderNotification::class)->weekly()->mondays()->timezone('Australia/Brisbane')->at('12:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
