<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UnblockUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:unblock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unblock User if Blocked period is over';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $users = User::where('status', User::BLOCKED)->whereDate('blocked_until','<=', Carbon::today())->get();
            if ($users->isNotEmpty()){
                foreach ($users as $user){
                    $user->status = User::ACTIVE;
                    $user->blocked_until = null;
                    $user->save();
                    if(!empty($user->firebaseId))
                    {
                        firebaseBlock($user->firebaseId,0);
                        \Log::info($user->firebaseId."  UNBlock By Admin ");
                    }
                }
            }
        } catch(ModelNotFoundException $e){
            $this->error('User Not Found!!');
        }
    }
}
