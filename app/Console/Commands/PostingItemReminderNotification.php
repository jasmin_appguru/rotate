<?php

namespace App\Console\Commands;

use App\Models\ApplicationNotification;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostingItemReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:PostingItemReminderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder for posting item for existing user  (Remind user if not posted any item in 30 days)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $date = Carbon::today();
            $alreadySent = ApplicationNotification::whereBetween('created_at', [$date->subDays(30), $date])
                ->where('type', Notification::POSTING_ITEM_REMINDER_NOTIFICATION)
                ->distinct()
                ->pluck('user_id')
                ->toArray();
            $users = User::where('status', User::ACTIVE)->whereNotIn($alreadySent)->get();
            if ($users->isNotEmpty()) {
                foreach ($users as $user) {
                    if (empty($user->lastItemPosted) || $user->lastItemPosted->created_at == $date) {
                        SendNotification(
                            __('api.post_item_reminder_message'),
                            $user->id,
                            true,
                            Notification::POSTING_ITEM_REMINDER_NOTIFICATION
                        );
                    }
                }
            }
        } catch (ModelNotFoundException $e) {
            $this->error('User Not Found!!');
        }
    }
}
