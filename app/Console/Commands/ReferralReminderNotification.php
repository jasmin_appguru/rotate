<?php

namespace App\Console\Commands;

use App\Models\ApplicationNotification;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class ReferralReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:ReferralReminderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder for referring a friend (notfiy once every month until they refer at least one friend)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $date = Carbon::today();
            $alreadySent = ApplicationNotification::whereBetween('created_at', [$date->subDays(30), $date])
                ->where('type', Notification::REFERRAL_REMINDER_NOTIFICATION)
                ->distinct()
                ->pluck('user_id')
                ->toArray();
            $users = User::where('status', User::ACTIVE)->whereNotIn($alreadySent)->get();
            if ($users->isNotEmpty()) {
                foreach ($users as $user) {
                    if ($user->referredFriends->count() == 0) {
                        SendNotification(
                            __('api.referral_reminder_message'),
                            $user->id,
                            true,
                            Notification::REFERRAL_REMINDER_NOTIFICATION
                        );
                    }
                }
            }
        } catch (ModelNotFoundException $e) {
            $this->error('User Not Found!!');
        }
    }
}
