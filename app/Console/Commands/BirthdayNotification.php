<?php

namespace App\Console\Commands;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BirthdayNotification extends Command
{
   /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:BirthNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Birthday Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $today = now();
            $users = User::where('status', User::ACTIVE)
                ->whereMonth('dob',$today->month)
                ->whereDay('dob',$today->day)
                ->get();
            if ($users->isNotEmpty()){
                foreach ($users as $user){
                    SendNotification(__('api.birthday_wish',['USERNAME' => $user->user_name]),$user->id,true,Notification::BIRTH_NOTIFICATION);
                }
            }
        } catch(ModelNotFoundException $e){
            $this->error('User Not Found!!');
        }
    }
}
