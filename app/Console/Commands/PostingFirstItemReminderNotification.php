<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ApplicationNotification;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostingFirstItemReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:PostingFirstItemReminderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder for posting item to a new user (Remind every 7 days after joining if not added any item in wardrobe)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $date = Carbon::today();
            $alreadySent = ApplicationNotification::whereBetween('created_at', [$date->subDays(7), $date])
                ->where('type', Notification::POSTING_FIRST_ITEM_REMINDER_NOTIFICATION)
                ->distinct()
                ->pluck('user_id')
                ->toArray();
            $users = User::where('status', User::ACTIVE)->where('created_at', '<=', $date->subDays(7))
                ->whereNotIn($alreadySent)
                ->get();
            if ($users->isNotEmpty()) {
                foreach ($users as $user) {
                    if ($user->Item->isEmpty()) {
                        SendNotification(
                            __('api.post_first_item_reminder_message'),
                            $user->id,
                            true,
                            Notification::POSTING_FIRST_ITEM_REMINDER_NOTIFICATION
                        );
                    }
                }
            }
        } catch (ModelNotFoundException $e) {
            $this->error('User Not Found!!');
        }
    }
}
