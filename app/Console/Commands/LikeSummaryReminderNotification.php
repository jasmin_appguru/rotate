<?php

namespace App\Console\Commands;

use App\Models\Favorite;
use App\Models\Item;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class LikeSummaryReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:LikeSummaryReminderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification on every Monday(12 PM - Midday) instead of individual item like notification because sending notification on each like will be annoying.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
          
            $users=User::where('is_profile_setup',1)->get();
            foreach($users as $user)
            {
                $date = \Carbon\Carbon::today();
                $item=Item::where('user_id',$user->id)->get()->pluck('id');
                $fev=Favorite::whereIn('item_id',$item)->where('created_at', '>=',$date->subDays(7))->get();
                if(count($fev)>0)
                {
                    SendNotification(
                        __('api.like_summary',['CONT'=>count($fev)]),
                        $user->id,
                        true,
                        Notification::LIKE_SUMMARY,
                        "Default"
                    );
                }
            }
        } catch (ModelNotFoundException $e) {
            $this->error('Like Count not Found!!');
        }
    }
}
