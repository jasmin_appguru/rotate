<?php

use App\Models\ActivityNotification;
use App\Models\ApplicationNotification;
use App\Models\ArchiveItem;
use App\Models\BlockUser;
use App\Models\User;
use App\Models\Follower;
use App\Models\Item;
use App\Models\Notification;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Contract\Firestore;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\ServiceAccount;
use PHPUnit\TextUI\XmlConfiguration\Group;

function changeDateFormate($date,$date_format){
    return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);
}

function GetItemListArchived($userId){
    return ArchiveItem::where('user_id',$userId)->groupBy('item_id')->pluck('item_id')->toArray();
}

function GetProfileDetail($id)
{
    return User::find($id);
}
function StatusEnableNotification($requested_user,$to_user)
{
    return (ActivityNotification::where('user_id',$to_user)->where('notify_id',$requested_user)->first()) ? 1 : 0;
}

function MyHiddenUserList()
{
    $me=array(\Auth::user()->id);
    $BlockByMe=BlockUser::where('user_id',\Auth::user()->id)->get()->pluck('to_user_id')->toArray();
    $BlockMe=BlockUser::where('to_user_id',\Auth::user()->id)->get()->pluck('user_id')->toArray();
   return array_unique(array_merge($BlockByMe,$BlockMe));
}

function SelectedUserHiddenUserList($userId)
{
    $BlockByMe=BlockUser::where('user_id',$userId)->get()->pluck('to_user_id')->toArray();
    $BlockMe=BlockUser::where('to_user_id',$userId)->get()->pluck('user_id')->toArray();
   return array_unique(array_merge($BlockByMe,$BlockMe));
}

function IsMyFollower($id)
{
    if(Follower::where('from_user_id',\Auth::user()->id)->where('to_user_id',$id)->first())
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

function ApiPaginationDefault()
{
    return 10;
}

function findNearestRestaurants($latitude, $longitude, $radius = 400)
{
    /*
        * using eloquent approach, make sure to replace the "Restaurant" with your actual model name
        * replace 6371000 with 6371 for kilometer and 3956 for miles
        */
    $restaurants = Item::selectRaw("id,captions, latitude, longitude,
                        ( 6371000 * acos( cos( radians(?) ) *
                        cos( radians( latitude ) )
                        * cos( radians( longitude ) - radians(?)
                        ) + sin( radians(?) ) *
                        sin( radians( latitude ) ) )
                        ) AS distance", [$latitude, $longitude, $latitude])
        ->having("distance", "<", $radius)
        ->orderBy("distance",'asc')
        ->offset(0)
        ->limit(10)
        ->get();
    return $restaurants;
}

function MyFollower($userId)
{
    $folloew=Follower::where('from_user_id',\Auth::user()->id)->where('to_user_id',$userId)->first();

    return $folloew ? true : false;
}


function SendNotification($body,$userid,$push=true,$notificationType = Notification::PUSH_NOTIFICATION,$title="Default",$addData=null,$BoldText=null,$url=null,$from_user=null)
{
    $AddOnPushNotification=array();
    $notification = new ApplicationNotification();
    if($title=="Default")
    {
        $notification->title=__('api.push_notification_title');
    }
    else
    {
        $notification->title=$title;
    }
    $notification->description=$body;
    $notification->user_id=$userid;
    $notification->type=$notificationType;
    if(isset($addData))
    {
        $AddOnPushNotification['postId']=$addData;
        $notification->postId=$addData;
    }
    if(isset($BoldText))
    {
        $AddOnPushNotification['username']=$BoldText;
        $notification->user_name=$BoldText;
    }
    if(isset($url))
    {
        $AddOnPushNotification['url']=$url;
        $notification->url=$url;
    }
    if(isset($from_user))
    {
        $AddOnPushNotification['userId']=$from_user;
        $notification->from_user_id=$from_user;
    }
    $notification->save();
    if($push==true)
    {
        SendPushNotification($userid,$body,$notificationType,$addData,$AddOnPushNotification);
    }
}

function thumbnail($request,$name,$path="app/public/avatars")
{
    $file = $request->file($name);
    $extension = $file->getClientOriginalExtension();
    $username = date('Y_m_d').time().rand(0, 99999).".".$extension;
    $thumb = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
        $constraint->aspectRatio(); //maintain image ratio
    });
    $destinationPath = storage_path($path);
    $file->move( $destinationPath, $username);
    $thumb->save($destinationPath.'/thumb/'.$username);
    return $username;
}

function ItemThumbnail($request,$name,$path="app/public/avatars")
{
    $file = $request;
    
    $extension = $file->getClientOriginalExtension();
    $username = date('Y_m_d').time().rand(0, 99999).".".$extension;
    // dd($file->getRealPath());
    //$image = $manager->make('public/foo.jpg')->resize(300, 200);
    $thumb =\Image::make($request);
    $thumb->resize("", 300, function ($constraint) {
        $constraint->aspectRatio(); //maintain image ratio
    });
    $destinationPath = storage_path('app/public/item/');
    
    $file->move( $destinationPath, $username);
    $thumb->save($destinationPath.'/thumb/'.$username);
    
    return $username;
}

function date_formate_notification($date)
{
    if(count($date)>1)
    {
        return date('d M Y',strtotime($date['0']))." - ".date('d M Y',strtotime($date[count($date)-1])).".";
    }
    else
    {
        return date('d M Y',strtotime($date['0'])).".";
    }
}

function SendPushNotification($userid,$massage,$notificationType,$extra_data=null,$user_name=null)
{
    $user=User::find($userid);
    if($user->device_type=='1')
    {
        if(!empty($user->device_token))
        {
            $name=__('api.push_notification_title');
            $data_send=array("title"=>$name,"body"=>$massage,"type" => 2,'notificationType' => $notificationType);
            if($extra_data!=null)
            {
                $data_send['postId']=$extra_data;
            }
            if($user_name!=null)
            {
                if(is_array($user_name))
                {
                    $temparray=$data_send;
                    $data_send="";
                    $data_send=array_merge($temparray,$user_name);
                    // $data_send['user_name']=$user_name;
                    // $data_send['user_name']=$user_name;

                }
            }
            send_notification_ios($user->device_token,$data_send);
        }
    }
    else
    {
        if(!empty($user->device_token))
        {
            $name=__('api.push_notification_title');
            $data_send=array("title"=>$name,"body"=>$massage,"type" => 2,'notificationType' => $notificationType);
            if($extra_data!=null)
            {
                $data_send['postId']=$extra_data;
            }
            if($user_name!=null)
            {
                if(is_array($user_name))
                {
                    $temparray=$data_send;
                    $data_send="";
                    $data_send=array_merge($temparray,$user_name);

                }
            }
            send_notification_android($user->device_token,$data_send);
        }
    }

}


function send_notification_ios($tokens, $message,$bulk=false)
{
	$message['sound']="Default";
    $key=config('portae_admin.FCM_TOKEN');
	$url = 'https://fcm.googleapis.com/fcm/send';
	$singleID = array($tokens);

    if($bulk==true)
    {
        $singleID=$tokens;
    }

	$fields = array (
	'registration_ids' => $singleID,
	'data' => $message, 
	'notification' => $message,
	);

    \Log::debug("IOS Test Debug IOS New : ".json_encode($fields));

	$headers = array(
	'Authorization: key=' . $key,
	'Content-Type: application/json'
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE)
	{
	//die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
}

function CountsInWord($cont)
{
    if($cont>999) {
        $x = round($cont);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];
        return (string)$x_display;
  }
  else
  {
    return (string)$cont;
  }
}
function send_notification_android($tokens,$message,$bulk=false)
{
	$key=config('portae_admin.FCM_TOKEN');

    $singleID = array($tokens);

    if($bulk==true)
    {
        $singleID=$tokens;
    }
	$url = 'https://fcm.googleapis.com/fcm/send';
    $title=$message['title'];
    $body=$message['body'];
    unset($message['title']);
    unset($message['body']);
    unset($message['type']);
	$fields = array
				(
                    "priority"=> "high",
                    'notification' => [
                        "title"=> $title,
                        "body"=> $body
                    ],
					'registration_ids' => $singleID,
					'data' =>$message,
				);

    \Log::debug("Android Push Test Debug: ".json_encode($fields));
	$headers = array(
	'Authorization: key='.$key,
	'Content-Type: application/json'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);

	if ($result === FALSE)
	{
	die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
}

function firebase_setup()
{
    # code...
}

function deleteFireBaseUser($email)
{
    $factory = (new Factory)->withServiceAccount(__DIR__.'/portae.json');
    $auth = $factory->createAuth();
    try {
        $user = $auth->getUserByEmail($email);
        $deleteUser = $auth->deleteUser($user->uid);
    } catch(Exception $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    }
}

function firebaseSuspend($user,$value)
{
    $file =base_path('portae.json');
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents($file), true)
        ]);
        try {
            $updateData =  $firestore->collection('users')->document($user);
            // systemSuspend
            // systemBlock
            $updateData->update([
                ['path' => 'systemSuspend', 'value' => (int)$value
                ],
            ]);
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
    }
function firebaseBlock($user,$value)
{
    $file =base_path('portae.json');
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents($file), true)
        ]);
        try {
            $updateData =  $firestore->collection('users')->document($user);
            // systemSuspend
            // systemBlock
            $updateData->update([
                ['path' => 'systemBlock', 'value' => (int)$value
                ],
            ]);
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
}

function updateFirebaseUserDocument($uid="",$name="jay")
{	
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../portae.json'), true)
        ]);
    try {
        $updateData =  $firestore->collection('users')->document("YjCg1iCedUaLmaZkFxWepzPg3Xl2");

        // systemSuspend
        // systemBlock
            $updateData->update([
            ['path' => 'firstName', 'value' => "Jess"
            ],
        ]);
    } catch (Exception $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    }
}

function updateFirebasePass($email,$password)
{	
    $file =base_path('portae.json');
    $factory = (new Factory)->withServiceAccount($file);
    $auth = $factory->createAuth();
    try {
        $user = $auth->getUserByEmail($email);
        $updatedUser = $auth->changeUserPassword($user->uid,$password);
    } catch(\Kreait\Firebase\Exception\Auth\InvalidPassword $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    } catch (\Kreait\Firebase\Exception\InvalidArgumentException $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    } catch (\Kreait\Firebase\Auth\SignIn\FailedToSignIn $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    }   
}

function firebaseBaseAdd()
{
    $data = [
        'name' => 'Los Angeles',
        'state' => 'CA',
        'country' => 'USA'
    ];
        $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../portae.json'), true)
        ]);
        try {

            $firestore->collection('users')->document("user")->set($data);

        } catch (Exception $e) {
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
}

function updateFirebaseItemDocument($Item)
{	
    // dd($Item->Url);
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);
        $user=User::where('id',$Item->user_id)->where('visibility',0)->orWhere('wardrobe_visibility',0)->first();

        try {
            $updateData =  $firestore->collection('items')->document($Item->id);
            $snapshot = $updateData->snapshot();
            if ($snapshot->exists())
            {
                if($user)
                {
                    $updateData->update([
                        ['path' => 'isPrivate', 'value' => (int)1],
                        ['path' => 'itemUrl', 'value' => $Item->Url],
                        ['path' => 'itemThumbUrl', 'value' => $Item->ThumbUrl],
                    ]);
                }
                else
                {
                    $updateData->update([
                        ['path' => 'isPrivate', 'value' => (int)$Item->visibility],
                        ['path' => 'itemUrl', 'value' => $Item->Url],
                        ['path' => 'itemThumbUrl', 'value' => $Item->ThumbUrl],
                    ]);
                }
                
            }
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            dd($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function updateFirebaseItemDocumentHide($Item)
{	
    // dd($Item->Url);
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);

        try {
            $updateData =  $firestore->collection('items')->document($Item->id);
            $snapshot = $updateData->snapshot();
            if ($snapshot->exists())
            {
                \Log::info(json_encode($Item->Url));
                $updateData->update([
                    ['path' => 'isPrivate', 'value' => 1],
                    ['path' => 'itemUrl', 'value' => $Item->Url],
                    ['path' => 'itemThumbUrl', 'value' => $Item->ThumbUrl],
                ]);
            }
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            dd($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function updateFirebaseItemDocumentDelete($Item)
{	
    // dd($Item->Url);
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);

        try {
            $updateData =  $firestore->collection('items')->document($Item->id);
            $snapshot = $updateData->snapshot();
            if ($snapshot->exists())
            {
                $updateData->update([
                    ['path' => 'isDeleted', 'value' => (int)1],
                  ]);
            }
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            // dd($e->getMessage());
            //return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function updateFirebaseItemDocumentUnDelete($Item)
{	
    // dd($Item->Url);
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);

        try {
            $updateData =  $firestore->collection('items')->document($Item->id);
            $snapshot = $updateData->snapshot();
            if ($snapshot->exists())
            {
                $updateData->update([
                    ['path' => 'isDeleted', 'value' => (int)0],
                    ['path' => 'itemUrl', 'value' => $Item->Url],
                    ['path' => 'itemThumbUrl', 'value' => $Item->ThumbUrl],
                ]);
            }
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            dd($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function updateFirebaseDeleteUser($userId)
{	
    // dd($Item->Url);
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);

        try {
            $updateData =  $firestore->collection('users')->document($userId);
            $snapshot = $updateData->snapshot();
            if ($snapshot->exists())
            {
                $updateData->update([
                    ['path' => 'isDeleted', 'value' => (int)1]
                ]);
            }
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function AddFirebaseItemDocument($Item)
{	
    
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);
        $user=User::where('id',$Item->user_id)->where('visibility',0)->orWhere('wardrobe_visibility',0)->first();
        try {
            if($user)
            {
                $data = [
                    'isDeleted' => 0,
                    'isPrivate' => (int)1,
                    'itemId' => $Item->id,
                    'userId' => $Item->user_id,
                    'itemUrl' => $Item->Url,
                    'itemThumbUrl' => $Item->ThumbUrl
                ];
            }
            else
            {
                $data = [
                    'isDeleted' => 0,
                    'isPrivate' => (int)$Item->visibility,
                    'itemId' => $Item->id,
                    'userId' => $Item->user_id,
                    'itemUrl' => $Item->Url,
                    'itemThumbUrl' => $Item->ThumbUrl
                ];
            }

            
            $firestore->collection('items')->document($Item->id)->set($data);
        }catch (Exception $e) {
            \Log::info("FIREBASE ERROR");
            \Log::info($e->getMessage());
            dd($e->getMessage());
            return array("status" => 401, "firebaseError" => $e->getMessage());
        }
        return true;
}

function updateFirebaseUserDeletedAtDocument($email)
{	
        
    $firestore = new FirestoreClient([
        'keyFile' => json_decode(file_get_contents(__DIR__.'/../../portae.json'), true)
        ]);
        
    $factory = (new Factory)->withServiceAccount(file_get_contents(__DIR__.'/../../portae.json'));
    $auth = $factory->createAuth();
    try 
    {
        $user = $auth->getUserByEmail($email);
        $deleteUser = $auth->deleteUser($user->uid);
        \Log::info("delete auth user");
    } 
    catch(Exception $e) 
    {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    }
}