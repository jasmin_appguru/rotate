<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AddItemImage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $error=array();
    public function __construct()
    {
        // $this->error=$error;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //$data = json_decode($value, true);

        foreach($value as $req)
        {
            $rules = [
                'imageId' => 'required|numeric',
                'image' => 'mimes:jpg,png,jpeg',
                'orderId' => 'required|numeric',
            ];
            $message= [
                'imageId.required' => 'A Date  is required',
                'image.file' => 'A file  is required',
                'orderId.file' => 'A numeric  is required',
            ];
            $validator = Validator::make($req, $rules,$message);
            if ($validator->passes()) {
                return true;
            }
            else
            {
                // dd($validator->failed());
                $this->error=$validator->failed();
                return false;
            }
        }
        //please join the need to discuses regarding api repose 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    
    public function message()
    {
        // return 'Invalid :attribute object ';
        $message= [
            'imageId' => ['data'=>"ss"],
            'image' => ['uploaded' =>"The :attribute failed to upload."],
            'orderId.file' => 'A numeric  is required',
        ];

        //dd($this);
    
        foreach ($this->error as $key => $val) {
            return $message[$key];
        }
    
        return $message[$this->error];
    }
}
