<?php

namespace App\Http\Middleware\Api;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class ValidateActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if(\Auth::check())
        {
            if (auth()->user()->status != User::ACTIVE) {
                $handleStatus = new BaseController();
                $message = 'Your account is '.auth()->user()->status.'. Please contact administrator.';
                //auth()->logout();
                if(auth()->user()->status==User::BLOCKED)
                {
                    return $handleStatus->handleError($message,3);
                }
                else
                {
                    return $handleStatus->handleError($message,2);
                }
            }
        }

        return $response;
    }
}
