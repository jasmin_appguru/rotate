<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\ReportUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReportUserController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RegisterReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'reason' => 'required|string',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $exist=ReportUser::where('from_user_id' , \Auth::user()->id)->where('user_id' , $request->userId)->first();
        if($exist)
        {
            $exist->reason=$request->reason;
            $exist->otherReason = $request->otherReason ?? '0';
            $exist->save();
        }else
        {
            $report_user=ReportUser::firstOrCreate([
                'from_user_id' => \Auth::user()->id,
                'user_id' => $request->userId,
                'reason' => $request->reason,
                'otherReason'=>$request->otherReason ?? '0'
            ]);

        }
        

        return $this->handleResponse("",__('api.reported_success',['USERNAME'=>GetProfileDetail($request->userId)->user_name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }
}
