<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\ActivityNotification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActivityNotificationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ActivityNotifications = ActivityNotification::all();
        return $this->handleResponse(\App\Http\Resources\ActivityNotificationResource::collection($ActivityNotifications), 'ActivityNotifications List',200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'notificationStatus' => 'required|numeric'
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        if($request->notificationStatus=='0')
        {
            $Favorite = ActivityNotification::create([
                'notify_id' => \Auth::user()->id,
                'user_id' => $request->userId,
            ]);
            ActivityNotification::where('notify_id',\Auth::user()->id)->where('user_id',$request->userId)->delete();
            return $this->handleResponse("",__('api.notification_on_off',['USERNAME' => GetProfileDetail($request->userId)->user_name,'STATUS' => 'off']));
        }
        else
        {
            $ActivityNotification=ActivityNotification::where('notify_id',\Auth::user()->id)->where('user_id',$request->userId)->first();
            if(empty($ActivityNotification))
            {
                $activity=new ActivityNotification();
                $activity->notify_id=\Auth::user()->id;
                $activity->user_id=$request->userId;
                $activity->save();
            }
            return $this->handleResponse("",__('api.notification_on_off',['USERNAME' => GetProfileDetail($request->userId)->user_name,'STATUS' => 'on']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityNotification  $activityNotification
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityNotification $activityNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityNotification  $activityNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityNotification $activityNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityNotification  $activityNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityNotification $activityNotification)
    {
        //
    }
}
