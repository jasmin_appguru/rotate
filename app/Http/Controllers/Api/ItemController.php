<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Item\ItemDetailResource;
use App\Http\Resources\Item\ItemListResource;
use App\Http\Resources\Item\ItemsList;
use App\Jobs\NotifyFavoritesUsers;
use App\Jobs\NotifyToFollower;
use App\Models\ArchiveItem;
use App\Models\Brand;
use App\Models\Favorite;
use App\Models\Item;
use App\Models\ItemPhoto;
use App\Models\User;
use App\Rules\AddItemImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Scopes\ActiveScope;
use Carbon\Carbon;
use ZipStream\Option\Archive;

class ItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //getItemList
        // 1: Trending
        // 2: Following
        // 3: All
        // 4: Near By 
        $validator = Validator::make($request->all(), [
            'filterType' => 'required|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'radius' => 'required|numeric',
        ]);

        if($validator->fails()){
            return $this->handleError(implode(" ,",$validator->errors()->all()));
        }

        $index=$request->pageIndex ?? 0;
        if($request->filterType=="4")
        {
            $user=\Auth::user();
            $user->radius=$request->radius;
            $user->save();
            $user=User::where('visibility','1')
            ->where('wardrobe_visibility','1')
            ->whereNotIn('id',MyHiddenUserList())
            ->JsNearMe($request->latitude,$request->longitude,$request->radius)->get();
            $Item=Item::orderBy('id','desc')
            ->whereIn('user_id',$user->pluck('id'))->JsPagination($index)->get();
        }
        elseif($request->filterType=="2")
        {
            if(isset(\Auth::user()->FollowingCount) && \Auth::user()->FollowingCount != '0')
            {
                $Item=Item::whereHas('User',function($q){
                    $q->where('visibility','1');
                    $q->where('wardrobe_visibility','1');
                    $q->whereNotIn('id',MyHiddenUserList());
                })
                ->orderBy('id','desc')->whereIn('user_id',\Auth::user()->Following->pluck('to_user_id'))->JsPublish()->JsPagination($index)->get();
            }
        }
        elseif($request->filterType=="1")
        {
            $fev=Favorite::where("created_at",">",Carbon::now()->subDay())->where("created_at","<",Carbon::now())->pluck('item_id');
            $Item=Item::whereHas('User',function($q){
                $q->where('visibility','1');
                $q->where('wardrobe_visibility','1');
                $q->whereNotIn('id',MyHiddenUserList());
            })->orderBy('id','desc')->whereIn('id',$fev)->JsPublish()->JsPagination($index)->get();
        }
        else
        {
            $Item=Item::whereHas('User',function($q){
                $q->where('visibility','1');
                $q->where('wardrobe_visibility','1');
                $q->whereNotIn('id',MyHiddenUserList());
            })->orderBy('id','desc')->JsPublish()->JsPagination($index)->get();
        }
        
        return $this->handleResponse(ItemListResource::collection($Item ?? []),__('api.item_fetch_success'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'itemId' => 'required',
            'brandId' => 'required|exists:brands,id',
            'isSell' => 'required',
            'categoryId' => 'required',
            'subCategoryId' => 'required',
            'rackId' => 'required',
            'priceType' => 'required',
            'price' => 'required',
            'captions' => 'required',
            'photos' => ['required' , new AddItemImage()],
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->itemId==0)
        {
            $item=new Item();
            $item->brand_Id=$request->brandId;
            $item->is_sell=$request->isSell;
            $item->subCategoryId=$request->subCategoryId;
            $item->category_id=$request->categoryId;
            $item->rackId=$request->rackId;
            $item->user_id=\Auth::user()->id;
            if ($request->has('clothCountry'))
            {
                $item->clothCountry=$request->clothCountry;
            }
            if($request->has('shoesCountry'))
            {
                $item->shoesCountry=$request->shoesCountry;
            }
            if($request->has('clothData'))
            {
                $item->clothData=$request->clothData;
            }
            if($request->has('shoesData'))
            {
                $item->shoesData=$request->shoesData;
            }
            if($request->has('shopLink'))
            {
                $item->shopLink=$request->shopLink;
            }
            if ($request->has('color'))
            {
                $item->color=$request->color;
            }
            $item->priceType=$request->priceType;
            $item->latitude=0;
            $item->longitude=0;
            $item->price=$request->price;
            $item->captions=$request->captions;
            $item->save();

            foreach($request->photos as $image)
            {
                $file="";
                $file=$image['image'];
                
                $this->imageStore($item->rackId,$item->subCategoryId,$image['imageId'],$image['orderId'],$item->id,$file);
            }
            //$item->category_id=$request->rackId;
            NotifyToFollower::dispatch(\Auth::user(),$item);
            AddFirebaseItemDocument($item);
            return $this->handleResponse("",__('api.new_data_add'));
        }
        else
        {
            $item=Item::where('id',$request->itemId)->first();
            $item->brand_Id=$request->brandId;
            if($request->isSell=="1")
            {
                NotifyFavoritesUsers::dispatch($item);
                // dispatch(new NotifyFavoritesUsers($item));
            }
            $item->is_sell=$request->isSell;
            $item->subCategoryId=$request->subCategoryId;
            $item->category_id=$request->categoryId;
            $item->rackId=$request->rackId;
            $item->user_id=\Auth::user()->id;
            if ($request->has('clothCountry'))
            {
                $item->clothCountry=$request->clothCountry;
            }
            if($request->has('shoesCountry'))
            {
                $item->shoesCountry=$request->shoesCountry;
            }
            if($request->has('clothData'))
            {
                $item->clothData=$request->clothData;
            }
            if($request->has('shoesData'))
            {
                $item->shoesData=$request->shoesData;
            }
            if($request->has('shopLink'))
            {
                $item->shopLink=$request->shopLink;
            }

            $item->color=$request->color;
            $item->priceType=$request->priceType;
            $item->latitude=0;
            $item->longitude=0;
            $item->price=$request->price;
            $item->captions=$request->captions;
            $item->save();

            ////DELETeImaghe 
            $imgi=$request->photos;
            $userNames = [];
            array_map(function ($imgi) use (&$userNames){
                if($imgi['imageId']!="0")
                {
                    $userNames[]=$imgi['imageId'];
                }
            }, $imgi);
            $eleteitem=ItemPhoto::where('item_id',$request->itemId)->whereNotIn('id',$userNames)->delete();
            foreach($request->photos as $image)
            {
                if(isset($image['image']))
                {
                    $file="";
                    $file=$image['image'];
                    $this->imageStore($item->rackId,$item->subCategoryId,$image['imageId'],$image['orderId'],$item->id,$file);
                    unset($image['image']);
                }
                else
                {
                    $this->imageUpdateSeq($image['imageId'],$image['orderId'],$item->id,$item->rackId,$item->subCategoryId);
                }
                // $image
            }
            updateFirebaseItemDocument($item);

            return $this->handleResponse("",__('api.update_data'));
        }
        
    }

    public function imageStore($rack_id,$cat_id,$image,$orderId,$itemId,$image_file)
    {
        if($image=="0")
        {
            $imageIOb=new ItemPhoto();
            $imageIOb->name=ItemThumbnail($image_file,'item',config('CONSTANT.ITEM_IMAGE_PATH'));
            //$imageIOb->name=$image_file->store( 'item', 'public');
            $imageIOb->category_id=$cat_id;
            if($rack_id!="0")
            {
                $imageIOb->rack_id=$rack_id;
            }
            else
            {
                $imageIOb->rack_id=null;
            }
            $imageIOb->item_id=$itemId;
            $imageIOb->user_id=\Auth::user()->id;
            $imageIOb->seq=$orderId;
            $imageIOb->save();
        }
        else
        {
            $imageIOb=ItemPhoto::where('id',$image)->first();
            $imageIOb->name=ItemThumbnail($image_file,'item',config('CONSTANT.ITEM_IMAGE_PATH'));
            //$imageIOb->name=$image_file->store( 'item', 'public');
            $imageIOb->category_id=$cat_id;
            $imageIOb->item_id=$itemId;
            if($rack_id!=0)
            {
                $imageIOb->rack_id=$rack_id;
            }
            else
            {
                $imageIOb->rack_id=null;
            }
            $imageIOb->user_id=\Auth::user()->id;
            $imageIOb->seq=$orderId;
            $imageIOb->save();
           
        }
        // if($request->hasFile('image')){
        //     foreach($request->image as $image)
        //     {
        //         $path = $image->file('profilePic')->store(
        //             'item', 'public'
        //         );
        //         $path;
        //     }
        // }
    }

    public function imageUpdateSeq($image,$orderId,$rack_id,$category_id)
    {
            $imageIOb=ItemPhoto::where('id',$image)->first();
            if($imageIOb)
            {
                $imageIOb->seq=$orderId;
                $imageIOb->rack_id=$rack_id;
                $imageIOb->category_id=$category_id;
                $imageIOb->save();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'itemId' => 'required|exists:items,id',
        ]);
        if ($validator->fails()) {
            return $this->handleError($validator->errors()->first());
        }
        $item=Item::where('id',$request->itemId)->withoutGlobalScope(ActiveScope::class)->first();
        //dd($item);

        
        $daya=Item::where('id',$request->itemId)->JsPublish()->whereHas('User',function($q)
        {
            $q->where('visibility',1)->where('wardrobe_visibility','1');

        })->first();

        if($item)
        {
            if($item->user_id==\Auth::user()->id)
            {
                $daya=$item;
            }
        }
        

        if(empty($daya))
        {
            if(empty($item))
            {
                return $this->handleError("This item is no longer available.");
            }
            return $this->handleError("This item hase been made private. If you wish to access it, Please contact the owner.");
        }
        return $this->handleResponse(new ItemDetailResource($daya), __('api.item_details_fetch_success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    public function deleteItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'itemId' => 'required|exists:items,id',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $ar=ArchiveItem::where('item_id',$request->itemId)->delete();

        $fv=Favorite::where('item_id',$request->itemId)->delete();

        $itemphoto=ItemPhoto::where('item_id',$request->itemId)->delete();
        
        updateFirebaseItemDocumentDelete(Item::find($request->itemId));

        $item=Item::find($request->itemId)->delete();

        return $this->handleResponse([],__('api.item_deleted_success'));
    }

    public function searchItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'searchItem' => 'required|string',
            'pageIndex' => 'required|string',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $item=Item::where("captions",'like',"%{$request->searchItem}%")->where('user_id',"!=",\Auth::user()->id)->get();

        return $this->handleResponse(ItemListResource::collection($item),__('api.item_deleted_success'));
    }

    public function changeItemType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isWardrobeItemPublic' => 'required',
            'itemId' => 'required|exists:items,id',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $item=Item::find($request->itemId);
        if($request->isWardrobeItemPublic=='1')
        {
            $item->visibility='0';
        }
        else
        {
            $item->visibility='1';
        }
        $item->save();
        updateFirebaseItemDocument($item);
        return $this->handleResponse("",__('api.wardrobe_item_privacy_success'));
    }

    public function getSizeList(Request $request)
    {
        $jayParsedAry = [
            "clothingCountry" => [
                  [
                     "countryId" => 1, 
                     "name" => "AU" 
                  ] ,
                  [
                     "countryId" => 2, 
                     "name" => "UK" 
                  ] ,
                  [
                     "countryId" => 3, 
                     "name" => "FR & ESP" 
                  ] ,
                  [
                     "countryId" => 4, 
                     "name" => "DE" 
                  ] ,
                  [
                     "countryId" => 5, 
                     "name" => "IT" 
                  ] ,
                  [
                     "countryId" => 6, 
                     "name" => "USA" 
                  ] ,
                  [
                     "countryId" => 7, 
                     "name" => "JPN" 
                  ]
               ], 
            "shoeCountry" => [
                        [
                           "countryId" => 1, 
                           "name" => "AU" 
                        ],
                        [
                           "countryId" => 2, 
                           "name" => "UK" 
                        ],
                        [
                           "countryId" => 3, 
                           "name" => "USA" 
                        ],
                        [
                           "countryId" => 4, 
                           "name" => "EUR" 
                        ],
                     ], 
            "clothingSizeList" => [
                              [
                                 "sizeId" => 1, 
                                 "sizeName" => "XS" 
                              ],
                              [
                                 "sizeId" => 2, 
                                 "sizeName" => "XS" 
                              ],
                              [
                                 "sizeId" => 3, 
                                 "sizeName" => "S" 
                              ],
                              [
                                 "sizeId" => 4, 
                                 "sizeName" => "S" 
                              ],
                              [
                                 "sizeId" => 5, 
                                 "sizeName" => "M" 
                              ],
                              [
                                 "sizeId" => 6, 
                                 "sizeName" => "L" 
                              ],
                              [
                                 "sizeId" => 7, 
                                 "sizeName" => "L" 
                              ],
                              [
                                 "sizeId" => 8, 
                                 "sizeName" => "XL" 
                              ],
                              [
                                 "sizeId" => 9, 
                                 "sizeName" => "XL+" 
                              ],
                              [
                                 "sizeId" => 10, 
                                 "sizeName" => "XL+" 
                              ],
                              [
                                 "sizeId" => 11, 
                                 "sizeName" => "XL+" 
                              ],
                           ], 
            "shoeSizeList" => [
                                    [
                                       "sizeId" => 1 
                                    ],
                                    [
                                       "sizeId" => 2 
                                    ],
                                    [
                                       "sizeId" => 3 
                                    ],
                                    [
                                       "sizeId" => 4 
                                    ],
                                    [
                                       "sizeId" => 5 
                                    ],
                                    [
                                       "sizeId" => 6 
                                    ],
                                    [
                                       "sizeId" => 7 
                                    ],
                                    [
                                       "sizeId" => 8 
                                    ],
                                    [
                                       "sizeId" => 9 
                                    ],
                                    [
                                       "sizeId" => 10 
                                    ],
                                    [
                                       "sizeId" => 11
                                    ],
                                    [
                                       "sizeId" => 12
                                    ],
                                    [
                                       "sizeId" => 13
                                    ],
                                    [
                                       "sizeId" => 14
                                    ],
                                    [
                                       "sizeId" => 15
                                    ],
                                    [
                                       "sizeId" => 16
                                    ],
                                 ], 
            "clothingSizeChart" => [
                                [
                                    "countryId" => 1, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "4"],
                                        [ "sizeId" => 2,"size" => "6"],
                                        [ "sizeId" => 3,"size" => "8"],
                                        [ "sizeId" => 4,"size" => "10"],
                                        [ "sizeId" => 5,"size" => "12"],
                                        [ "sizeId" => 6,"size" => "14"],
                                        [ "sizeId" => 7,"size" => "16"],
                                        [ "sizeId" => 8,"size" => "18"],
                                        [ "sizeId" => 9,"size" => "20"],
                                        [ "sizeId" => 10,"size" => "22"],
                                        [ "sizeId" => 11,"size" => "22"],
                                    ] 
                                ],
                                [
                                    "countryId" => 2, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "4"],
                                        [ "sizeId" => 2,"size" => "6"],
                                        [ "sizeId" => 3,"size" => "8"],
                                        [ "sizeId" => 4,"size" => "10"],
                                        [ "sizeId" => 5,"size" => "12"],
                                        [ "sizeId" => 6,"size" => "14"],
                                        [ "sizeId" => 7,"size" => "16"],
                                        [ "sizeId" => 8,"size" => "18"],
                                        [ "sizeId" => 9,"size" => "20"],
                                        [ "sizeId" => 10,"size" => "22"],
                                        [ "sizeId" => 11,"size" => "22"],
                                    ] 
                                ], 
                                [
                                    "countryId" => 3, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "32"],
                                        [ "sizeId" => 2,"size" => "34"],
                                        [ "sizeId" => 3,"size" => "36"],
                                        [ "sizeId" => 4,"size" => "38"],
                                        [ "sizeId" => 5,"size" => "40"],
                                        [ "sizeId" => 6,"size" => "42"],
                                        [ "sizeId" => 7,"size" => "44"],
                                        [ "sizeId" => 8,"size" => "46"],
                                        [ "sizeId" => 9,"size" => "48"],
                                        [ "sizeId" => 10,"size" => "50"],
                                        [ "sizeId" => 11,"size" => "52"],
                                    ] 
                                ], 
                                [
                                    "countryId" => 4, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "34"],
                                        [ "sizeId" => 2,"size" => "36"],
                                        [ "sizeId" => 3,"size" => "38"],
                                        [ "sizeId" => 4,"size" => "40"],
                                        [ "sizeId" => 5,"size" => "42"],
                                        [ "sizeId" => 6,"size" => "44"],
                                        [ "sizeId" => 7,"size" => "46"],
                                        [ "sizeId" => 8,"size" => "48"],
                                        [ "sizeId" => 9,"size" => "50"],
                                        [ "sizeId" => 10,"size" => "52"],
                                        [ "sizeId" => 11,"size" => "54"],
                                    ] 
                                ],
                                [
                                    "countryId" => 5, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "36"],
                                        [ "sizeId" => 2,"size" => "38"],
                                        [ "sizeId" => 3,"size" => "40"],
                                        [ "sizeId" => 4,"size" => "42"],
                                        [ "sizeId" => 5,"size" => "44"],
                                        [ "sizeId" => 6,"size" => "46"],
                                        [ "sizeId" => 7,"size" => "48"],
                                        [ "sizeId" => 8,"size" => "50"],
                                        [ "sizeId" => 9,"size" => "52"],
                                        [ "sizeId" => 10,"size" => "54"],
                                        [ "sizeId" => 11,"size" => "56"],
                                    ] 
                                ],
                                [
                                    "countryId" => 6, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "0"],
                                        [ "sizeId" => 2,"size" => "2"],
                                        [ "sizeId" => 3,"size" => "4"],
                                        [ "sizeId" => 4,"size" => "6"],
                                        [ "sizeId" => 5,"size" => "8"],
                                        [ "sizeId" => 6,"size" => "10"],
                                        [ "sizeId" => 7,"size" => "12"],
                                        [ "sizeId" => 8,"size" => "14"],
                                        [ "sizeId" => 9,"size" => "16"],
                                        [ "sizeId" => 10,"size" => "18"],
                                        [ "sizeId" => 11,"size" => "20"],
                                    ] 
                                ],
                                [
                                    "countryId" => 7, 
                                    "sizes" => [
                                        [ "sizeId" => 1,"size" => "3"],
                                        [ "sizeId" => 2,"size" => "5"],
                                        [ "sizeId" => 3,"size" => "7"],
                                        [ "sizeId" => 4,"size" => "9"],
                                        [ "sizeId" => 5,"size" => "11"],
                                        [ "sizeId" => 6,"size" => "13"],
                                        [ "sizeId" => 7,"size" => "15"],
                                        [ "sizeId" => 8,"size" => "17"],
                                        [ "sizeId" => 9,"size" => "19"],
                                        [ "sizeId" => 10,"size" => "21"],
                                        [ "sizeId" => 11,"size" => "23"],
                                    ] 
                                ],
                                       ], 
            "clothingMeasurmentChart" => [
                ["sizeId" => 1, "bust" => [ "inch" => "32" , "cm" => "78" ],"waist" => [ "inch" => "24.5" , "cm" => "60" ],"hips" => [ "inch" => "34" , "cm" => "84" ]],
                ["sizeId" => 2, "bust" => [ "inch" => "33" , "cm" => "80.5" ],"waist" => [ "inch" => "25.5" , "cm" => "62.5" ],"hips" => [ "inch" => "35" , "cm" => "86.4" ]],
                ["sizeId" => 3, "bust" => [ "inch" => "34" , "cm" => "83" ],"waist" => [ "inch" => "26.5" , "cm" => "65" ],"hips" => [ "inch" => "36" , "cm" => "89" ]],
                ["sizeId" => 4, "bust" => [ "inch" => "36" , "cm" => "88" ],"waist" => [ "inch" => "28.5" , "cm" => "70" ],"hips" => [ "inch" => "38" , "cm" => "94" ]],
                ["sizeId" => 5, "bust" => [ "inch" => "38" , "cm" => "93" ],"waist" => [ "inch" => "30.5" , "cm" => "75" ],"hips" => [ "inch" => "40" , "cm" => "99" ]],
                ["sizeId" => 6, "bust" => [ "inch" => "40" , "cm" => "98" ],"waist" => [ "inch" => "32.5" , "cm" => "80" ],"hips" => [ "inch" => "42" , "cm" => "104" ]],
                ["sizeId" => 7, "bust" => [ "inch" => "42" , "cm" => "103" ],"waist" => [ "inch" => "34.5" , "cm" => "85" ],"hips" => [ "inch" => "44" , "cm" => "109" ]],
                ["sizeId" => 8, "bust" => [ "inch" => "45" , "cm" => "110.5" ],"waist" => [ "inch" => "37.5" , "cm" => "92.5" ],"hips" => [ "inch" => "47" , "cm" => "116.5" ]],
                ["sizeId" => 9, "bust" => [ "inch" => "50" , "cm" => "127" ],"waist" => [ "inch" => "42.9" , "cm" => "109" ],"hips" => [ "inch" => "54.7" , "cm" => "139" ]],
                ["sizeId" => 10, "bust" => [ "inch" => "52.8" , "cm" => "134" ],"waist" => [ "inch" => "45.7" , "cm" => "116" ],"hips" => [ "inch" => "57.5" , "cm" => "146" ]],
                ["sizeId" => 11, "bust" => [ "inch" => "55.5" , "cm" => "141" ],"waist" => [ "inch" => "48.8" , "cm" => "123" ],"hips" => [ "inch" => "60.2" , "cm" => "-" ]],            
              ], 
            "shoesSizeChart" => [
                [
                    "countryId" => 1,"sizes" => [
                        [ "sizeId" => 1,"size" => "4"],
                        [ "sizeId" => 2,"size" => "4.5"],
                        [ "sizeId" => 3,"size" => "5"],
                        [ "sizeId" => 4,"size" => "5.5"],
                        [ "sizeId" => 5,"size" => "6"],
                        [ "sizeId" => 6,"size" => "6.5"],
                        [ "sizeId" => 7,"size" => "7"],
                        [ "sizeId" => 8,"size" => "7.5"],
                        [ "sizeId" => 9,"size" => "8"],
                        [ "sizeId" => 10,"size" => "8.5"],
                        [ "sizeId" => 11,"size" => "9"],
                        [ "sizeId" => 12,"size" => "9.5"],
                        [ "sizeId" => 13,"size" => "10"],
                        [ "sizeId" => 14,"size" => "10.5"],
                        [ "sizeId" => 15,"size" => "11"],
                        [ "sizeId" => 16,"size" => "11.5"],
                    ] 
                ],
                [
                    "countryId" => 2,"sizes" => [
                        [ "sizeId" => 1,"size" => "2"],
                        [ "sizeId" => 2,"size" => "2.5"],
                        [ "sizeId" => 3,"size" => "3"],
                        [ "sizeId" => 4,"size" => "3.5"],
                        [ "sizeId" => 5,"size" => "4"],
                        [ "sizeId" => 6,"size" => "4.5"],
                        [ "sizeId" => 7,"size" => "5"],
                        [ "sizeId" => 8,"size" => "5.5"],
                        [ "sizeId" => 9,"size" => "6"],
                        [ "sizeId" => 10,"size" => "6.5"],
                        [ "sizeId" => 11,"size" => "7"],
                        [ "sizeId" => 12,"size" => "7.5"],
                        [ "sizeId" => 13,"size" => "8"],
                        [ "sizeId" => 14,"size" => "8.5"],
                        [ "sizeId" => 15,"size" => "9"],
                        [ "sizeId" => 16,"size" => "9.5"],
                    ] 
                ],
                [
                    "countryId" => 3,"sizes" => [
                       [ "sizeId"=>	1,"size" => "4.5"],
                        [ "sizeId"=>2,"size" => "5"],
                        [ "sizeId"=>3,"size" => "5.5"],
                        [ "sizeId"=>4,"size" => "6"],
                        [ "sizeId"=>5,"size" => "6.5"],
                        [ "sizeId"=>6,"size" => "7"],
                        [ "sizeId"=>7,"size" => "7.5"],
                        [ "sizeId"=>8,"size" => "8"],
                        [ "sizeId"=>9,"size" => "8.5"],
                        [ "sizeId"=>10,"size" => "9"],
                        [ "sizeId"=>11,"size" => "9.5"],
                        [ "sizeId"=>12,"size" => "10"],
                        [ "sizeId"=>13,"size" => "10.5"],
                        [ "sizeId"=>14,"size" => "11"],
                        [ "sizeId"=>15,"size" => "11.5"],
                        [ "sizeId"=>16,"size" => "12"],
                    ] 
                ],
                [
                    "countryId" => 4,"sizes" => [
                        [ "sizeId"=>1,"size" => "34"],
                        [ "sizeId"=>2,"size" => "35"],
                        [ "sizeId"=>3,"size" => "35.5"],
                        [ "sizeId"=>4,"size" => "36"],
                        [ "sizeId"=>5,"size" => "37"],
                        [ "sizeId"=>6,"size" => "37.5"],
                        [ "sizeId"=>7,"size" => "37"],
                        [ "sizeId"=>8,"size" => "38.5"],
                        [ "sizeId"=>9,"size" => "39"],
                        [ "sizeId"=>10,"size" => "39.5"],
                        [ "sizeId"=>11,"size" => "40"],
                        [ "sizeId"=>12,"size" => "41"],
                        [ "sizeId"=>13,"size" => "42"],
                        [ "sizeId"=>14,"size" => "42.5"],
                        [ "sizeId"=>15,"size" => "43"],
                        [ "sizeId"=>16,"size" => "44"],
                    ] 
                ] 
            ] 
         ]; 
        return $this->handleResponse($jayParsedAry,"Size List get successfully.");
    }

    public function filterItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isSell' => 'in:0,1',
            'clothData' => 'json',
            'shoesData' => 'json',
            'color' => 'json',
            'brandId' => 'json',
            'categoryId' => 'json',
            'pageIndex' => 'numeric',
            'subCategoryId' => 'json',
        ]);

        if ($validator->fails()) {
            return $this->handleError($validator->errors()->first());
        }

        $search=1;
        $userId = auth()->user()->id;
        $index=$request->pageIndex ?? 0;
        $item = Item::whereNotIn('user_id',MyHiddenUserList())->JsPublish()->where('user_id', "!=", \Auth::user()->id);
        if ($request->has('searchText') && $request->searchText != "") {
            $search=0;
            $brandList=Brand::where('name', 'LIKE', $request->searchText."%")->get()->pluck('id')->toArray();
            $item->where(function($query) use ($brandList,$request) {
                  return $query
                    ->whereIn('brand_Id', $brandList)
                    ->orWhere('captions', 'LIKE', $request->searchText . "%");
                 });
            //$item->whereIn('brand_Id', $brandList);
            //$item->orWhere('captions', 'LIKE', $request->searchText . "%");
        }
        if ($request->has('latitude') && $request->has('longitude') && $request->latitude != "" && $request->longitude != "") {
            $search=0;
            $item->JsNearMeItem($request->latitude, $request->longitude);
        }
        if ($request->has('color') && count(json_decode($request->color))>0) {
            $search=0;
            $item->FilterColor(json_decode($request->color));
        }
        
        if ($request->has('brandId') && count(json_decode($request->brandId))>0) {
            $search=0;
            $item->whereIn('brand_Id', json_decode($request->brandId));
        }
        if ($request->has('categoryId') && count(json_decode($request->categoryId))>0) {
            $search=0;
            $item->whereIn('category_id', json_decode($request->categoryId));
        }
        if ($request->has('subCategoryId') && count(json_decode($request->subCategoryId))>0) {
            $search=0;
            $item->whereIn('subCategoryId', json_decode($request->subCategoryId));
        }
        if ($request->has('clothData') && count(json_decode($request->clothData))>0) {
            $search=0;
            $item->FilterClothData(json_decode($request->clothData));
        }
        if ($request->has('shoesData') && count(json_decode($request->shoesData))>0) {
            $search=0;
            $item->FilterShoesData(json_decode($request->shoesData));
        }
        if ($request->has('isSell')) {
            $search=0;
            if($request->isSell=="1")
            {
                $item->where('is_sell',(int)$request->isSell);
            }
        }
        if ($search==1) {
            return $this->handleResponse("", "Item fetched successfully.");
        } else {
            return $this->handleResponse(ItemListResource::collection($item->JsPagination($index)->get()), "Item fetched successfully.");
        }
    }
}
