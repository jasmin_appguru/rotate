<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\ArchiveItem;
use App\Models\Item;
use App\Scopes\ActiveScope;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArchiveItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ArchiveItems = ArchiveItem::where('user_id',\Auth::user()->id)
        ->pluck('item_id')->toArray();
        $item=Item::whereIn('id',$ArchiveItems)->withoutGlobalScope(ActiveScope::class)->get();
        return $this->handleResponse(\App\Http\Resources\ArchiveItemResource::collection($item), __('api.archive_fetch_success'),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'itemId' => 'required|exists:items,id',
            'archiveStatus' => 'required|numeric',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //for errors of request

        if($request->archiveStatus=='0')
        {
            $Favorite = ArchiveItem::create([
                'user_id' => \Auth::user()->id,
                'item_id' => $request->itemId,
            ]);
            $item=Item::where('id',$request->itemId)->withoutGlobalScope(ActiveScope::class)->first();
            updateFirebaseItemDocumentDelete($item);
            $item->status="Archive";
          
            $item->save();
            return $this->handleResponse([], __('api.archive_unarchive_success',['STATUS' =>'archived']));
        }
        else
        {
            ArchiveItem::where('user_id',\Auth::user()->id)->where('item_id',$request->itemId)->delete();
            $item=Item::where('id',$request->itemId)->withoutGlobalScope(ActiveScope::class)->first();
            updateFirebaseItemDocumentUnDelete($item);
            $item->status="Active";
            $item->save();
            return $this->handleResponse([], __('api.archive_unarchive_success',['STATUS' =>'unArchived']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArchiveItem  $archiveItem
     * @return \Illuminate\Http\Response
     */
    public function show(ArchiveItem $archiveItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArchiveItem  $archiveItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArchiveItem $archiveItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArchiveItem  $archiveItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArchiveItem $archiveItem)
    {
        //
    }
}
