<?php

namespace App\Http\Controllers\Api;

use App\Events\DeleteUserAccount;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\Follower\SearchResource;
use App\Http\Resources\User\FirendListResource;
use App\Http\Resources\User\FriendListResource;
use App\Http\Resources\User\GetProfileResource;
use App\Jobs\changeIteamStatusFirebase;
use App\Http\Resources\UserResource;
use App\Models\OtpManager;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Mail\sendEmail;
use App\Mail\SenOtpMail;
use App\Models\DeleteAccount;
use App\Models\Notification;
use App\Notifications\SendOtp;
use App\Notifications\WelcomeMail;

class UserController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'mobile' => 'required|unique:users,mobile_number',
            'otp' => 'required',
            'deviceType' => 'required',
            'deviceToken' => 'required',
        ],[
            'email.unique' => __('api.register_email_unique')
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $validatedData = $request->all();
        $validatedData['mobile_number']=$request->mobile;
        $validatedData['device_type']=$request->deviceType;
        $validatedData['device_token']=$request->deviceToken;
        $validatedData['otp']=$request->otp;
        $validatedData['password'] = Hash::make($request->password);
        if($request->has('referralCode'))
        {
            $validatedData['joinReferralCode']=$request->referralCode;
        }
        
        if($request->has('otp'))
        {
            if(!$this->_validateEmailOTP($request->email,$request->otp))
            {
                $user = User::create($validatedData);
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->tokan=$accessToken;
            }
            else
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }
        return $this->handleResponse([
            "userId" => $user->id,
            "accessToken" => $user->tokan
        ], __('api.signup'));
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            // 'deviceToken' => 'required',
            // 'deviceType' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $auth = Auth::user();

            $request->user()->tokens->each(function($token, $key) {
                $token->delete();
            });

            // $success['token'] =  $auth->createToken('authToken')->accessToken;
            if($request->has('deviceToken'))
            {
                $auth->device_token=$request->deviceToken;
                $auth->device_type=$request->deviceType;
                $auth->save();
            }

            $tokan=$auth->createToken('authToken')->accessToken;
            $auth->tokan=$tokan;
            return $this->handleResponse(new UserResource($auth), __('api.login_success'));
        }
        elseif(Auth::attempt(['user_name' => $request->email, 'password' => $request->password])){
            $auth = Auth::user();

            // $success['token'] =  $auth->createToken('authToken')->accessToken;
            if($request->has('deviceToken'))
            {
                $auth->device_token=$request->deviceToken;
                $auth->device_type=$request->deviceType;
                $auth->save();
            }

            $tokan=$auth->createToken('authToken')->accessToken;
            $auth->tokan=$tokan;


            return $this->handleResponse(new UserResource($auth), __('api.login_success'));
        }  
        else{
            if (Str::contains($request->email,'@')) {
                return $this->handleError(__('api.invalid'));
            }
            else
            {
                return $this->handleError(__('api.invalid_user_name'));
            }
        }
    }

    public function setUpProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'firebaseId' => 'required',
            'userName' => 'required|unique:users,user_name',
            'dob' => 'required|date|date_format:d-m-Y',
            'age' => 'required',
            'location' => 'required',
            'location' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'clothCountry' => 'required',
            'shoesCountry' => 'required'
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=\Auth::user();
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->firebaseId=$request->firebaseId;
        $user->dob=date('Y-m-d',strtotime($request->dob));
        $user->age=$request->age;
        $user->location=$request->location;
        $user->user_name=$request->userName;
        $user->latitude=$request->latitude;
        $user->longitude=$request->longitude;
        $user->clothCountry=$request->clothCountry;
        $user->shoesCountry=$request->shoesCountry;
        if($request->has('clothData'))
        {
            $user->clothData=json_decode($request->clothData);
        }
        else
        {
            $user->clothData="";
        }
        
        if($request->has('shoesData'))
        {
            $user->shoesData=json_decode($request->shoesData);
        }
        else
        {
            $user->shoesData="";
        }

        $user->is_profile_setup=1;

        if(!empty($request->file('profilePic'))){
            $name=thumbnail($request,'profilePic');
             $user->image=$name;
            // $path = $request->file('profilePic')->store(
            //     'avatars', 'public'
            // );
            // $user->image=$path;
        }
        $profileIsUpdated = $user->save();

        if($profileIsUpdated  && ! empty($user->joinReferralCode)) {
            $referredUser = User::where('referralCode', $user->joinReferralCode)->first();
            if(!empty($referredUser)) {
                SendNotification(__('api.referral_message',['USERNAME' => $user->first_name.' '.$user->last_name]),$referredUser->id,true,Notification::REFERRAL_NOTIFICATION,"Default",null,$user->first_name.' '.$user->last_name,$user->thumb,$user->id);
            }
        }
        if ($profileIsUpdated && $user->is_profile_setup == 1) {
            $user->notify(new WelcomeMail($user));
        }

        return $this->handleResponse(new UserResource(\Auth::user()),__('api.setUpProfile'));

    }

    public function editProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'firebaseId' => 'required',
            'userName' => 'unique:users,user_name,'.\Auth::user()->id,
            'dob' => 'required|date|date_format:d-m-Y',
            'age' => 'required',
            'location' => 'required',
            'location' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'clothCountry' => 'required',
            'shoesCountry' => 'required'
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=\Auth::user();
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->firebaseId=$request->firebaseId;
        $user->dob=date('Y-m-d',strtotime($request->dob));
        $user->age=$request->age;
        $user->location=$request->location;
        if($request->has('userName'))
        {
            if($user->last_change_username > Date('Y-m-d h:i:s', strtotime('-1 days')))
            {
                return $this->handleError(__('api.editProfile_last30username'));
            }
            else
            {
                $user->last_change_username=Date('Y-m-d h:i:s');
            }
            //dd(Date('Y-m-d h:i:s', strtotime('-30 days')));
            $user->user_name=$request->userName;
        }
        $user->latitude=$request->latitude;
        $user->longitude=$request->longitude;
        $user->clothCountry=$request->clothCountry;
        $user->shoesCountry=$request->shoesCountry;
        if($request->has('clothData'))
        {
            $user->clothData=json_decode($request->clothData);
        }
        else
        {
            $user->clothData="";
        }
        
        if($request->has('shoesData'))
        {
            $user->shoesData=json_decode($request->shoesData);
        }
        else
        {
            $user->shoesData="";
        }
        $user->is_profile_setup=1;
        
        if(!empty($request->file('profilePic'))){
            $name=thumbnail($request,'profilePic');
             $user->image=$name;
        }
        $user->save();
        return $this->handleResponse(new UserResource($user),__('api.editProfile'));
        
    }



    public function getProfile(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'userId' => 'required|numeric',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->userId=='0')
        {
            $user=\Auth::user();
        }
        else
        {
            $user=User::find($request->userId);
        }

        if($user == null)
        {
            return $this->handleError(__('api.invalid_user_id'));
        }
        else
        {
            return $this->handleResponse(new GetProfileResource($user),__('api.profile_update_success'));
        }
    }

    public function CheckUserName(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'userName' => 'required|unique:users,user_name',
        ],[
            'userName.unique' => __('api.userName_unique_unavailable')
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        if($request->userName=="you" || $request->userName=="You" )
        {
            return $this->handleError("This username is unavailable. Try another.");
        }
        if(\Auth::user()->is_profile_setup=='1')
        {
            if(\Auth::user()->last_change_username > Date('Y-m-d h:i:s', strtotime('-30 days')))
            {
                return $this->handleError(__('api.editProfile_last30username'));
            }
        }
        return $this->handleResponse([],__('api.userName_unique_available'));
    }


    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newPassword' => 'required|min:6|max:16',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=\Auth::user();
        $user->password=Hash::make($request->newPassword);
        $user->save();

        return $this->handleResponse([],__('api.change_password'));
    }

    public function changeAccountType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isPublic' => 'required|integer|max:1',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=\Auth::user();
        $user->visibility=$request->isPublic;
        $user->save();

        if($request->isPublic=="0")
        {
            dispatch(new changeIteamStatusFirebase(\Auth::user()->id,"hide"));
        }
        else
        {
            dispatch(new changeIteamStatusFirebase(\Auth::user()->id,"show"));
            
        }

        return $this->handleResponse([],__('api.changeAccountType'));
    }

    public function changeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newEmail' => 'required|unique:users,email',
            'otp' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if(!$this->_validateEmailOTP($request->newEmail,$request->otp))
        {
            $user=\Auth::user();
            $user->email=$request->newEmail;           
            $user->save();
            //$accessToken = $user->createToken('authToken')->accessToken;
            //$user->tokan=$accessToken;
        }
        else
        {
            return $this->handleError(__('api.invalid_otp'));
        }

        return $this->handleResponse([],__('api.change_email_success'));
    }

    public function getVerificationCode(Request $request)
    {
        $role_j=array(
            'mobileNumber' => 'required'
        );

        $validator = Validator::make($request->all(),$role_j);
        if($validator->fails()){
            return $this->handleError($validator->errors());
        }
        $sentotp=$this->_SendOtp($request->mobileNumber,rand(0,999999));
        if($sentotp === true)
        {
            return $this->handleResponse([],__('api.getVerificationCode'));
        }
        return $this->handleError(__('api.error_otp_api'),$sentotp,401);
    }

    public function _SendOtp($receiverNumber,$otp)
    {
        $otp="1234";
        $message =__('api.otp_massage',['OTP' => $otp,'purpose'=>'signup']);

        try {
            if(config('portae_admin.TWILIO_ENABLE'))
            {
                $account_sid = config('portae_admin.TWILIO_SID');
                $auth_token = config('portae_admin.TWILIO_TOKEN');
                $twilio_number = config('portae_admin.TWILIO_FROM');
                $client = new Client($account_sid, $auth_token);
                $client->messages->create($receiverNumber, [
                    'from' => $twilio_number,
                    'body' => $message]);
            }
            OtpManager::updateOrCreate(['mobile_number'=>$receiverNumber],['otp'=>$otp]);
            return true;
        } catch (Exception $e) {
            \Log::alert($e);
            return $e->getMessage();
        }
    }

    public function _validateOTP($receiverNumber,$otp)
    {
        if($otp=="1234")
        {
            return 0;
        }
        $otp_manager=OtpManager::where('mobile_number',$receiverNumber)->where('otp',$otp)->orderBy('id','DESC')->first();
        if($otp_manager)
        {
            $otp_manager->otp="";
            $otp_manager->save();
            return 0;
        }
        else
        {
            return 1;
        }
    }
    public function _validateEmailOTP($email,$otp)
    {
        if($otp=="1234")
        {
            return 0;
        }
        $otp_manager=OtpManager::where('email',$email)->where('otp',$otp)->orderBy('id','DESC')->first();
        if($otp_manager)
        {
            $otp_manager->otp="";
            $otp_manager->save();
            return 0;
        }
        else
        {
            return 1;
        }
    }

    public function Logout(Request $request)
    {
        if(\Auth::user())
        {
            $u = Auth::user();
            $u->device_token="";
            $u->save();
            $user = Auth::user()->token();
            $user->revoke();
        }

        return $this->handleResponse([],__('api.Logout_success'));
    }

    public function searchUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'searchQuery' => 'required',
            'pageIndex' => 'required|numeric',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        
        $user=User::Active()->Public()->where(function($query) use ($request){
            $query->where('user_name', 'LIKE', '%' . $request->searchQuery . '%')
            ->orWhere('first_name', 'LIKE', '%' . $request->searchQuery . '%')
            ->orWhere('last_name', 'LIKE', '%' . $request->searchQuery . '%');
        })
        ->JsPagination($request->pageIndex)
        ->whereNotIn('id',MyHiddenUserList())
        ->get();
        
        if($user->count()<1)
        {
            return $this->handleResponse("",__('api.user_search_success'));
        }
        else
        {
            return $this->handleResponse(SearchResource::collection($user),__('api.user_search_success'));
        }
    }

    public function ForgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=User::where('email',$request->email)->first();
        if(!$user)
        {
            return $this->handleError("The email has not been registered with this user.");
        }
        $input = $request->only('email');

        $response = Password::sendResetLink($input);

        $message = $response === Password::RESET_LINK_SENT ? $response : __($response);

        //passwords.sent
        //passwords.user
        if($response=="passwords.user")
        {
            return $this->handleError(__($message));
        }
        else
        {
            return $this->handleResponse([],__($message));

        }
    }

    public function loginUser(Request $request)
    {
        return \Auth::user();
    }

    public function unauthorize(Request $request)
    {
        return $this->handleResponse([],__('api.invalid_url'));
    }

    public function WelcomEmail()
    {
        
        dd("sent welcome mail");
    }

    public function requestOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users,email',
        ],
        [
            'email.unique' => "This email has already been taken."
    ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        
           $otp = rand(1000,9999);
            $OtpManager=OtpManager::updateOrCreate(['email'=>$request->email],['otp'=>$otp]);
         
           if($OtpManager){
   
           $mail_details = [
               'subject' => 'Testing Application OTP',
               'body' => $otp
           ];
          

            \Notification::route('mail', $request->email)->notify(new SendOtp($mail_details));

            return $this->handleResponse(['otp'=>$otp],__('api.verify_email_success'));
           }
           else{
               return response(["status" => 401, 'message' => 'Invalid']);
           }
    }

    public function InviteFriendList(Request $request)
    {
        $user=User::where('joinReferralCode',\Auth::user()->referralCode)->where('is_profile_setup',1)->get();        
        return $this->handleResponse(FriendListResource::collection($user),__('api.invite_friend_fetch_success'));
    }

    public function changeWardrobeType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isWardrobePublic' => 'required',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $user=User::where('id',\Auth::user()->id)->first();
        $user->wardrobe_visibility=$request->isWardrobePublic;
        $user->save();
        if($request->isWardrobePublic=="0")
        {
            dispatch(new changeIteamStatusFirebase(\Auth::user()->id,"hide"));
        }
        else
        {
            $userCk=User::where('id',\Auth::user()->id)->where('visibility',0)->first();
            if(empty($userCk))
            {
                dispatch(new changeIteamStatusFirebase(\Auth::user()->id,"show"));
            }
            
        }
        return $this->handleResponse("",__('api.wardrobe_privacy_success'));
    }

    public function validateReferralCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'referralCode' => 'required',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $user=User::where('referralCode',$request->referralCode)->first();
        if(isset($user))
        {
            return $this->handleResponse("",__('Congrats! You have entered the correct code.'));
        }
        else
        {
            return $this->handleError(__('Sorry! You have entered the incorrect code.'),'0');
        }
    }

    public function DeleteAccount(Request $request)
    {
            $user1=Auth::user();
            if(\Auth::user())
            {
                $u = Auth::user();
                $u->device_token="";
                $u->save();
                $user = Auth::user()->token();
                $user->revoke();
            }
            $record_user=new DeleteAccount();
            $record_user->user_id=$user1->id;
            $record_user->user_detail=json_encode($user1);
            $record_user->request_ip=\Request::ip();
            $record_user->save();

            event (new DeleteUserAccount($user1));

            return $this->handleResponse("",__('api.delete_account'));
    }
}
