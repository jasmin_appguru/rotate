<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\Item\ItemListResource;
use App\Http\Resources\RackResource;
use App\Http\Resources\RackResourceWardrope;
use App\Http\Resources\Wardrobes\LetestResource;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemPhoto;
use App\Models\Rack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WardrobesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id'
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $data=array();
        // TODO
        foreach(Category::where('parent_id',null)->get() as $name)
        {
            $latest=Item::where('category_id',$name->id)
            ->JsPublishWithMyHidden($request->userId)
            ->where('user_id',$request->userId)
            ->latest()
            ->get()
            ->unique('subCategoryId')
            ->take(5);


            // $data[strtolower($name->name)]=$latest;
            $data[strtolower($name->name)]=LetestResource::collection($latest);
        }
        // dd($data);
        return $this->handleResponse($data,__('api.wardrobe_fetch_success'));
    }

    public function GetItemFromCat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'categoryId' => 'required|exists:categories,id',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $data=array();
$sale_item=Item::where('category_id',$request->categoryId)
->JsPublishWithMyHidden($request->userId)
->where('user_id',$request->userId)
->where('is_sell',1)
->first();
        $data_default=array(
            [
                    "subCategoryId" => 0,
                    "subCategoryName" => "For Sale",
                    "catUrl" =>  $sale_item->roles_list ?? "default.jpg"
                    
                ]);
        
            $latest=Item::where('category_id',$request->categoryId)
            ->where('user_id',$request->userId)
            ->where('category_id',$request->categoryId)
            ->whereNotIn('id',GetItemListArchived($request->userId))
            ->JsPublishWithMyHidden($request->userId)
            ->latest()
            ->get()
            ->unique('subCategoryId');
            
            foreach($latest as $iteam)
            {
                array_push($data_default,
                [
                    "subCategoryId"=> (int)$iteam->subCategoryId,
                    "subCategoryName"=> $iteam->SubCategory->name,
                    "catUrl" => $iteam->roles_list,
                ]
                );
            }

        return $this->handleResponse($data_default,__('api.subcategory_fetch_success'));
    }

    public function getWardrobeItem(Request $request)
    {
        // getWordrobeItem
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'pageIndex' => 'required',
            'subCategoryId' => 'required',
            'catId' => 'required',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $data=array();
        $index=$request->pageIndex ?? 0;
        if($request->pageIndex==0 && $request->subCategoryId!=0)
        {
            //$data['racks'] = RackResource::collection(Rack::where('user_id',$request->userId)->where('sub_category_id',$request->subCategoryId)->where('category_id',$request->catId)->orderBy('seq')->get());
            $data['racks'] = 
            RackResourceWardrope::collection(
                Item::whereHas('photos')
                ->with('rack', function($r) {
                    $r->orderBy('seq','asc');
                })
                ->where('subCategoryId',$request->subCategoryId)->where('category_id',$request->catId)->where('rackId',"!=",0)
                // ->latest()
                ->where('user_id',$request->userId)
                ->JsPublishWithMyHidden($request->userId)
                ->orderBy('order','asc')
                ->get()
                ->unique('rackId')
                //->sortByDesc('order')
            );
        }
        else
        {
            $data['racks'] = [];
        }

        if($request->subCategoryId!=0)
        {
            $data['items'] = ItemListResource::collection(Item::JsPublishWithMyHidden($request->userId)->whereNotIn('id',GetItemListArchived($request->userId))->where('user_id',$request->userId)->where('subCategoryId',$request->subCategoryId)->where('rackId',0)->where('category_id',$request->catId)->orderBy('order','asc')->orderBy('id','DESC')->JsPagination($index)->get());
        }
        else
        {
            $data['items'] = ItemListResource::collection(
                Item::JsPublishWithMyHidden($request->userId)
                ->where('user_id',$request->userId)
                ->where('is_sell',1)
                ->where('category_id',$request->catId)->JsPagination($index)->get());
        }
        return $this->handleResponse($data,__('api.wardrobe_item_fetch_success'));
    }

    public function getWardrobeRackList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'subCategoryId' => 'required|exists:categories,id',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $list=GetItemListArchived($request->userId);
        return $this->handleResponse(
            RackResourceWardrope::collection(
                Item::whereHas('photos')
                ->with('rack', function($r) {
                    $r->orderBy('seq','asc');
                })
                ->where('subCategoryId',$request->subCategoryId)->where('rackId',"!=",0)
                ->where('user_id',$request->userId)
                ->orderBy('order','asc')
                ->JsPublishWithMyHidden($request->userId)
                ->get()
                ->unique('rackId')
                )
                ,__('api.wardrobe_item_fetch_success'));
        // return $this->handleResponse(RackResource::collection(Rack::where('user_id',$request->userId)->where('sub_category_id',$request->subCategoryId)->with('Items')->whereHas('Items',function ($query) use ($list) 
        // { 
        //     $query->where('visibility','0'); 
        //     if(!empty($list))
        //     {
        //         $query->whereNotIn('id',GetItemListArchived($list));
        //     }
        // })->orderBy('seq')->get()),__('api.wardrobe_item_fetch_success'));
    }

    public function getWardrobeRackItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'subCategoryId' => 'required|exists:categories,id',
            'rackId' => 'required|exists:racks,id',
            'pageIndex' => 'required',

        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $index=$request->pageIndex ?? 0;
        return $this->handleResponse(ItemListResource::collection(
            Item::JsPublishWithMyHidden($request->userId)->where('subCategoryId',$request->subCategoryId)->where('rackId',$request->rackId)->orderBy('order','asc')->JsPagination($index)->get()
        ),__('api.wardrobe_item_fetch_success'));
    }

    public function reOrderItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rackOrder' => 'required|json',
            'itemOrder' => 'required|json',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //$rackOrder
        foreach(json_decode($request->rackOrder) as $res)
        {
            $racjk=Rack::where('id',$res->rackId)->where('user_id',\Auth::user()->id)->first();
            $racjk->seq=$res->orderId;
            $racjk->save();
        }
        foreach(json_decode($request->itemOrder) as $ite)
        {
            $item=Item::where('id',$ite->itemId)->where('user_id',\Auth::user()->id)->first();
            $item->order=$ite->orderId;
            $item->save();
        }
        return $this->handleResponse("",__('api.rack_reorder_success'));
    }

    public function moveItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rackId' => 'required',
            'subCategoryId' => 'required|exists:categories,id',
            'moveItem' => 'required|json',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        foreach(json_decode($request->moveItem) as $item_detail)
        {
            $item=Item::where('id',$item_detail->itemId)->where('user_id',\Auth::user()->id)->first();
            if(!empty($item))
            {
                $item->rackId=$request->rackId;
                $item->order="0";
                $item->subCategoryId=$request->subCategoryId;
                $item->save();
                $item_photos=ItemPhoto::where('item_id',$item->id)->get();
                foreach($item_photos as $photo)
                {
                    $photo->rack_id=$request->rackId;
                    $photo->category_id=$request->subCategoryId;
                    $photo->save();
                }
            }
            else
            {
                return $this->handleError("please select valid image Id.");
            }
        }
        return $this->handleResponse("",__('api.rack_reorder_success'));
    }

    
 }



