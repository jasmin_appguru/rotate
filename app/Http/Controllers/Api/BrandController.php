<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class BrandController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Brand=Brand::query();
        if($request->has('brandText') && $request->brandText != NULL)
        {
            $Brand->where('name','like',$request->brandText."%");
        }
        $Brand->where('status','Approved');
        $Brand->orderBy('name','asc');
        return $this->handleResponse(BrandResource::collection($Brand->get()),__('api.Brand_success'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'brandName' => 'required',
        ],
    [
        "brandName.unique" => __('api.add_brand_name_unique')
    ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $brand=Brand::where('name',$request->brandName)->first();
        if(empty($brand))
        {
            $brand=new Brand();
            $brand->name=$request->brandName;
            $brand->status='request';
            $brand->save();
        }
        else{
            if($brand->status=="Approved")
            {
                return $this->handleResponseWithOutData("",__('api.add_brand_name_unique'));
            }
        }
        
        return $this->handleResponse(new BrandResource($brand),__('api.brand_add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }
}
