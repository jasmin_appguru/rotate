<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\BlockListResource;
use App\Models\BlockUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlockUserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $index=$request->pageIndex ?? 0;
        $blockUser=BlockUser::where('user_id',\Auth::user()->id)->JsPagination($index)->get();
        return $this->handleResponse(BlockListResource::collection($blockUser),__('api.block_list_success'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function blockUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'blockStatus' => 'required',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->blockStatus=="0")
        {
            BlockUser::where('user_id',\Auth::user()->id)->where('to_user_id',$request->userId)->delete();
            return $this->handleResponse("",__('api.block_success',['USERNAME'=>GetProfileDetail($request->userId)->user_name,'ACTION'=>"Unblock"]));
        }
        else
        {
            $report_user=BlockUser::firstOrCreate([
                'user_id' => \Auth::user()->id,
                'to_user_id' => $request->userId,
            ]);

            return $this->handleResponse("",__('api.block_success',['USERNAME'=>GetProfileDetail($request->userId)->user_name,'ACTION'=>"block"]));
        }

        //return $this->handleResponse("",__('api.block_success',['USERNAME'=>GetProfileDetail($request->userId)->user_name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }
}
