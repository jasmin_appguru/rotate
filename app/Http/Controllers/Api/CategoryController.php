<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CatagoryResourse;
use App\Http\Resources\GlobalCatagoryResource;
use App\Http\Resources\SubCatagoryResource;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;


class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Category=Category::where('parent_id',NULL)->get();
        return $this->handleResponse(CatagoryResourse::collection($Category),__('api.category_success'));
    }

    public function SubCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'categoryId' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $Category=Category::where('parent_id',$request->categoryId)->with(['MyRack'])->get();
       return $this->handleResponse(SubCatagoryResource::collection($Category),__('api.sub_category_success'));      
    }

    public function GetGlobalCategoryList(Request $request)
    {

        $Category=Category::where('parent_id',null)->with(['SubCategory'])->get();
       return $this->handleResponse(GlobalCatagoryResource::collection($Category),"All Category fetched successfully.");      
    }

   
}
