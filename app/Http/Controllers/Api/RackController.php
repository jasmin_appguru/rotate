<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Rack\RackAddResource;
use App\Models\Rack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RackController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rackName' => 'required',
            'categoryId' => 'required|exists:categories,id',
            'subCategoryId' => 'required|exists:categories,id',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        
        $rac=Rack::create([
            'user_id' => \Auth::user()->id,
            'name' => $request->rackName,
            'category_id' => $request->categoryId,
            'sub_category_id' => $request->subCategoryId
        ]);
        
        return $this->handleResponse(new RackAddResource($rac),__('api.rack_added_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->handleResponse("Collation or list","massage of response");
    }
}
