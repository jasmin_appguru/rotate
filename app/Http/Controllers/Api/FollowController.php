<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\FollowerResource;
use App\Http\Resources\FollowingResource;
use App\Models\ActivityNotification;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class FollowController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function followUnfollow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id|',
            'following' => 'required|numeric',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->following=='1')
        {
            Follower::firstOrCreate([
                'from_user_id' => \Auth::user()->id,
                'to_user_id' => $request->userId
            ]);
            ActivityNotification::firstOrCreate([
                'notify_id' => \Auth::user()->id,
                'user_id' => $request->userId
            ]);

            $follow_user=User::find($request->userId);
            SendNotification(__('api.followed_msg_notification',['USERNAME' =>\Auth::user()->user_name]),$follow_user->id,true,Notification::NEW_FOLLOWER,"Default",null,\Auth::user()->user_name,\Auth::user()->thumb,\Auth::user()->id);
        }
        else{
            Follower::where([
                'from_user_id' => \Auth::user()->id,
                'to_user_id' => $request->userId
            ])->delete();
            ActivityNotification::where([
                'notify_id' => \Auth::user()->id,
                'user_id' => $request->userId
            ])->delete();
        }
        return $this->handleResponse([],__('api.follow_success',['OPERATION'=>($request->following==1) ? 'Follow' : 'Unfollowed']));
    }

    public function getFollowers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'pageIndex' => 'required|numeric',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $request_follower=Follower::where('to_user_id', $request->userId)->where('from_user_id', \Auth::user()->id)->get();
        if($request_follower)
        {
            $restlist=Follower::where('to_user_id', $request->userId)->where('from_user_id',"!=", \Auth::user()->id)
            ->whereNotIn('from_user_id',MyHiddenUserList())
            ->JsPagination($request->pageIndex)->orderBy('created_at','asc')->get();
            $collection = collect($request_follower);
            $follower=$collection->merge($restlist);
        }
        else
        {
            $follower=Follower::where('to_user_id', $request->userId)->whereNotIn('from_user_id',MyHiddenUserList())->JsPagination($request->pageIndex)->orderBy('created_at','asc')->get();
        }
        return $this->handleResponse(FollowerResource::collection($follower),__('api.followers_fetch_success'));
    }

    public function getFollowing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
            'pageIndex' => 'required|numeric',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $request_follower=Follower::where('from_user_id', $request->userId)->where('to_user_id', \Auth::user()->id)->get();
        if($request_follower)
        {
            $restlist=Follower::where('from_user_id', $request->userId)->where('to_user_id',"!=", \Auth::user()->id)->whereNotIn('to_user_id',MyHiddenUserList())->JsPagination($request->pageIndex)->orderBy('created_at','asc')->get();
            $collection = collect($request_follower);
            $follower=$collection->merge($restlist);
        }
        else
        {
            $follower=Follower::where('from_user_id', $request->userId)->whereNotIn('to_user_id',MyHiddenUserList())->JsPagination($request->pageIndex)->get();
        }
        return $this->handleResponse(FollowingResource::collection($follower),__('api.following_fetch_success'));
    }
}
