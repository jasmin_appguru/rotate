<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\ApplicationNotification;
use App\Models\Notification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotificationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $index=$request->pageIndex ?? 0;
        $Notifications = ApplicationNotification::where('user_id',\Auth::user()->id)->orderBy('id','desc')->JsPagination($index)->get();
        
        ApplicationNotification::where('user_id',\Auth::user()->id)->update(['is_read'=>ApplicationNotification::READ]);

        return $this->handleResponse(\App\Http\Resources\NotificationResource::collection($Notifications), __('api.notification_fetch_success'),200);
    }

    public function Count(Request $request)
    {
        $Notifications = ApplicationNotification::where('user_id',\Auth::user()->id)->where('is_read',ApplicationNotification::UNREAD);
        return $this->handleResponse([
            "notificationCount" => $Notifications->get()->count()
        ], __('api.notification_count_fetch_success'),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'colum_01' => 'required',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //for errors of request

        $validatedData = $request->all();
        $Notification = Notification::create($validatedData);

        //store data
        return $this->handleResponse(new \App\Http\Resources\NotificationResource($Notification), __('api.new_notification'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
