<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\favorite\FavoriteListResource;
use App\Models\Favorite;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavoriteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pageIndex' => 'required|numeric',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $Favorites = Favorite::where('user_id',\Auth::user()->id)
        ->whereHas('Item',function($q){
            $q->whereNotIn('user_id',MyHiddenUserList());
        })
        ->JsPagination($request->pageIndex)->with('item','item.user')->get();
        
        //return $this->handleResponse($Favorites, __('api.item_fetch_success'));
        return $this->handleResponse(FavoriteListResource::collection($Favorites), __('api.item_fetch_success'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'itemId' => 'required|exists:items,id',
            'likeStatus' => 'required|numeric'
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //for errors of request

        $validatedData = $request->all();
        if($request->likeStatus=='1')
        {
            $Favorite = Favorite::create([
                'user_id' => \Auth::user()->id,
                'item_id' => $request->itemId,
            ]);
            return $this->handleResponse([],__('api.fav_unfav_success',['STATUS' => 'Favorite']));
        }
        else
        {
            Favorite::where('user_id',\Auth::user()->id)->where('item_id',$request->itemId)->delete();
            return $this->handleResponse([], __('api.fav_unfav_success',['STATUS' => 'Unfavorite']));
        }

        //store data
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favorite $favorite)
    {
        //
    }
}
