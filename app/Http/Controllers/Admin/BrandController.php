<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class BrandController extends Controller
{
    public function getalldata($status="1",Request $request)
    {
        $users = Brand::orderBy('id', 'DESC')->where('status',$status)->get();
        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->name;
                    return $fullName;
                })
                ->addColumn('status', function($users){
                    if($users->status == "Approved")
                    {
                        $is_active = '<span class="badge badge-success">Approved</span>' ;
                    }
                    elseif($users->status == "Rejected")
                    {
                        $is_active = '<span class="badge badge-danger">Rejected</span>' ;
                    }
                    else
                    {
                        $is_active = '<span class="badge badge-warning">Request</span>' ;
                    }
                    //$is_active = ($users->status == "Approved") ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-danger">Pending</span>';
                    return $is_active;
                })
                ->addColumn('Action', function($users){
                    if($users->status == "Approved")
                    {
                        $is_active='<a href="'.route("brand.changeStatus",["id" => $users->id,"status"=>'Rejected']).'"  class="btn btn-danger"><i class="icon md-close" aria-hidden="true"></i></a>';
                    }
                    elseif($users->status == "Rejected")
                    {
                        $is_active='<a href="'.route("brand.changeStatus",["id" => $users->id,"status"=>'Approved']).'"  class="btn btn-success"><i class="icon md-check" aria-hidden="true"></i></a>';
                    }
                    else
                    {
                        $is_active='<a href="'.route("brand.changeStatus",["id" => $users->id,"status"=>'Rejected']).'"  class="btn btn-danger"><i class="icon md-close" aria-hidden="true"></i></a> ';
                        $is_active.='<a href="'.route("brand.changeStatus",["id" => $users->id,"status"=>'Approved']).'"  class="btn btn-success"><i class="icon md-check" aria-hidden="true"></i></a>';
                    }
                    return $is_active;
                })
                ->setRowClass('viewInformation1')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/Feedback/".$user->id);
                    },
                ])
                ->rawColumns(['name','status','Action'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {
        return view('admin.Brand.index',compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changestatus($id,$status)
    {
        $brand=Brand::find($id);
        $brand->status=$status;
        $brand->save();
        return redirect()->back();
    }
}
