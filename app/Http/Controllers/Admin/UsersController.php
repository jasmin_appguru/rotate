<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\Events\DeleteUserAccount;

class UsersController extends Controller
{
    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $users = User::get();
        }
        else{
            $users = User::orderBy('id', 'DESC')->get();
        }

        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->first_name.' '.$users->last_name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->image.'"></a>';
                    return $fullName;

                })
                ->editColumn('email', function($users){
                    $email = isset($users->email)?trim($users->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($users){
                    $mobile = isset($users->mobile_number) ? trim($users->mobile_number) : "N/A";
                    return $mobile;
                })
                ->editColumn('agency_name', function($users){
                    $agency_name ="N/A";
                    return $agency_name;
                })
                ->addColumn('status', function($users){
                    $is_active = ($users->is_active == 0) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                    return $is_active;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("/users/".$user->id);
                    },
                ])
                ->rawColumns(['name','status','action'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Users.index');
    }

    public function BlockRequest()
    {
        return view('admin.Users.block');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "User";

        $user = User::where('id',$id)->first();
        if($user){
            return view('admin.Users.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            event (new DeleteUserAccount($user));
            updateFirebaseUserDeletedAtDocument($user->email);
            // updateFirebaseUserDeletedAtDocument
            $user->delete();
            return response()
                ->json(['success' => true]);
        } catch(Exeception $e) {
            return response()
                ->json(['success' => false]);
        }
    }

    public function exportUser()
    {
      return Excel::download(new UsersExport(User::all()), 'Users-'.date('d-m-Y').'.csv');
    }
}
