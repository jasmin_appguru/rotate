<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $users = User::where('user_type',2)->get();
        }
        else{
            $users = User::where('user_type',2)->orderBy('id', 'DESC')->get();
        }

        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->first_name.' '.$users->last_name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->image.'"></a>';
                    return $fullName;

                })
                ->editColumn('email', function($users){
                    $email = isset($users->email)?trim($users->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($users){
                    $mobile = isset($users->mobile_number) ? trim($users->mobile_number) : "N/A";
                    return $mobile;
                })
                ->editColumn('agency_name', function($users){
                    $agency_name = isset($users->Owner->business_name) ? trim(\Str::limit($users->Owner->business_name,20)) : "N/A";
                    return $agency_name;
                })
                ->addColumn('status', function($users){
                    $is_active = ($users->is_active == 0) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                    return $is_active;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/owner-user/".$user->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Users.block');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
