<?php
/**
    * Author Name: LS
    * Datetime: 2021-08-16
    * Dashboard Controller class for dashboard 'Admin' section
**/

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Owner;
use App\Helper\Helper;
use App\Models\Booking;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Feedback;
use App\Models\Item;
use App\Models\JobApplication;
use App\Models\ReportUser;
use App\Models\User;
use Redirect;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Image;
use Storage;
use Config;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
	protected $breadcrumb = "Dashboard";
    protected $pageTitle = "Dashboard";

    public function index(Request $request){
    	$breadcrumb = $this->breadcrumb;
        $pageTitle = $this->pageTitle;

        $users = User::all()->count();
        $directUser = User::whereNull('joinReferralCode')->count();
        $referralUser = User::whereNotNull('joinReferralCode')->count();
        $suspendedUsers = User::where('status','<>',User::ACTIVE)->get()->count();
        $reportedUsers = ReportUser::all()->count();
        $jobApplication = JobApplication::all()->count();
        $brand = Brand::active()->get()->count();
        $totalItmes = Item::all()->count();
        $item = (int)round($totalItmes/((int)$users == 0 ? 1: (int)$users));
        $feedback = Feedback::count();
        $categories = Category::whereNull('parent_id')->get();
        $category = Category::whereNotNull('parent_id')->count();
        $priceSum = Item::sum('price');
        $pricePerCategory = round($priceSum / ($category == 0 ? 1: $category));
        $currentCategory="";
        $userRange = $userAge = $userstartDate = $userendDate = $userToDate = "";
        $catRange = $catstartDate = $catendDate = $catToDate = "";
        $otherRange = $otherstartDate = $otherendDate = $otherToDate = "";

        $boxes = [
            'users' => [
                ['text' => 'All Users','value' => $users],
                ['text' => 'Users: Direct Registered','value' => $directUser],
                ['text' => 'Users: Joined Via Referral','value' => $referralUser],
                ['text' => 'Reported Users','value' => $reportedUsers],
                ['text' => 'Suspended Users','value' => $suspendedUsers],
            ],
            'category' => [
                ['text' => 'Average Items/Profile','value' => $item],
                ['text' => 'Total Items','value' => $totalItmes],
                ['text' => 'Price Per Category','value' => $pricePerCategory],

            ],
            'other' => [
                ['text' => 'Brand','value' => $brand],
                ['text' => 'Feedback','value' => $feedback],
                ['text' => 'Job Application','value' => $jobApplication],

            ],
        ];
        return view("admin.dashboard",
                    compact(
                        'boxes',
                        'currentCategory',
                        'category',
                        'userAge',
                        'categories',
                        'pageTitle',
                        'userRange',
                        'userAge',
                        'userstartDate',
                        'userendDate',
                        'userToDate',
                        'catRange',
                        'catstartDate',
                        'catendDate',
                        'catToDate',
                        'otherRange',
                        'otherstartDate',
                        'otherendDate',
                        'otherToDate',
                    )
                );
    }

    public function filter(Request $request)
    {
        $feedback = Feedback::query();
        $users = User::query();
        $directUser = User::whereNull('joinReferralCode');
        $referralUser = User::whereNotNull('joinReferralCode');
        $suspendedUsers =User::where('status','<>',User::ACTIVE);
        $reportedUsers = ReportUser::query();
        $jobApplication = JobApplication::query();
        $brand = Brand::active();
        $item= Item::query();
        $feedback = Feedback::query();
        $categories = Category::whereNull('parent_id')->get();
        $priceSum = Item::query();

        // User Filter
        if($request->userRange=="daily") {
            $users->whereDay('created_at','=',date('d'));
            $directUser->whereDay('created_at','=',date('d'));
            $referralUser->whereDay('created_at','=',date('d'));
            $suspendedUsers->whereDay('updated_at','=',date('d'));
            $reportedUsers->whereDay('created_at','=',date('d'));
        } elseif($request->userRange=="weekly") {
            $users->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $directUser->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $referralUser->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $suspendedUsers->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $reportedUsers->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        } elseif($request->userRange=="monthly") {
            $users->whereMonth('created_at','=',date('m'));
            $directUser->whereMonth('created_at','=',date('m'));
            $referralUser->whereMonth('created_at','=',date('m'));
            $suspendedUsers->whereMonth('updated_at','=',date('m'));
            $reportedUsers->whereMonth('created_at','=',date('m'));
        } elseif($request->userRange=="quarterly") {
            $date = new \Carbon\Carbon('-3 months');
            $users->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $directUser->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $referralUser->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $suspendedUsers->whereBetween('updated_at', [$date->startOfQuarter(), now()]);
            $reportedUsers->whereBetween('created_at', [$date->startOfQuarter(), now()]);
        } elseif($request->userRange=="yearly") {
            $users->whereYear('created_at','=',date('Y'));
            $directUser->whereYear('updated_at','=',date('Y'));
            $referralUser->whereYear('updated_at','=',date('Y'));
            $suspendedUsers->whereYear('updated_at','=',date('Y'));
            $reportedUsers->whereYear('created_at','=',date('Y'));
        } elseif($request->userRange=="custom") {
            $users->whereBetween('created_at',[$request->userstartDate, $request->userendDate]);
            $directUser->whereBetween('created_at',[$request->userstartDate, $request->userendDate]);
            $referralUser->whereBetween('created_at',[$request->userstartDate, $request->userendDate]);
            $suspendedUsers->whereBetween('updated_at',[$request->userstartDate, $request->userendDate]);
            $reportedUsers->whereBetween('created_at',[$request->userstartDate, $request->userendDate]);
        } elseif($request->userRange=="todate" && $request->userTodate!="") {
            $users->where('created_at', '<=', $request->userTodate);
            $directUser->where('created_at', '<=', $request->userTodate);
            $referralUser->where('created_at', '<=', $request->userTodate);
            $suspendedUsers->where('updated_at', '<=', $request->userTodate);
            $reportedUsers->where('created_at', '<=', $request->userTodate);
        }
        if(!empty($request->userAge) && $request->userAge != "all"){
            if($request->userAge == '13_18') {
                 $users->whereBetween('age',[12,19]);
                 $directUser->whereBetween('age',[12,19]);
                 $referralUser->whereBetween('age',[12,19]);
             } elseif($request->userAge == '19_24') {
                 $users->whereBetween('age',[18,25]);
                 $directUser->whereBetween('age',[18,25]);
                 $referralUser->whereBetween('age',[18,25]);
             } elseif($request->userAge == '25_34') {
                 $users->whereBetween('age',[24,35]);
                 $directUser->whereBetween('age',[24,35]);
                 $referralUser->whereBetween('age',[24,35]);
             } elseif($request->userAge == '35_45') {
                 $users->whereBetween('age',[34,46]);
                 $directUser->whereBetween('age',[34,46]);
                 $referralUser->whereBetween('age',[34,46]);
             } elseif($request->userAge == '45_plus') {
                 $users->where('age', '>', 45);
                 $directUser->where('age', '>', 45);
                 $referralUser->where('age', '>', 45);
             }
         }

        // Category Filter
        if($request->catRange=="daily") {
            $item->whereDay('created_at','=',date('d'));
            $priceSum->whereDay('created_at','=',date('d'));
        } elseif($request->catRange=="weekly") {
            $item->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $priceSum->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        } elseif($request->catRange=="monthly") {
            $item->whereMonth('created_at','=',date('m'));
            $priceSum->whereMonth('created_at','=',date('m'));
        } elseif($request->catRange=="quarterly") {
            $date = new \Carbon\Carbon('-3 months');
            $item->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $priceSum->whereBetween('created_at', [$date->startOfQuarter(), now()]);
        } elseif($request->catRange=="yearly") {
            $item->whereYear('created_at','=',date('Y'));
            $priceSum->whereYear('created_at','=',date('Y'));
        } elseif($request->catRange=="custom") {
            $item->whereBetween('created_at',[$request->catstartDate, $request->catendDate]);
            $priceSum->whereBetween('created_at',[$request->catstartDate, $request->catendDate]);
        } elseif($request->catRange=="todate" && $request->catToDate!="") {
            $item->where('created_at', '<=', $request->catToDate);
            $priceSum->where('created_at', '<=', $request->catToDate);
        }
        if(!empty($request->currentCategory) && $request->currentCategory != "all"){
            $item->where('category_id',$request->currentCategory);
            $category = Category::where('parent_id',$request->currentCategory)->count();
        } else {
            $category = Category::whereNotNull('parent_id')->count();
        }

        // Other Filter
        if($request->otherRange=="daily") {
            $jobApplication->whereDay('created_at','=',date('d'));
            $brand->whereDay('created_at','=',date('d'));
            $feedback->whereDay('created_at','=',date('d'));
        } elseif($request->otherRange=="weekly") {
            $jobApplication->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $brand->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $feedback->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        } elseif($request->otherRange=="monthly") {
            $jobApplication->whereMonth('created_at','=',date('m'));
            $brand->whereMonth('created_at','=',date('m'));
            $feedback->whereMonth('created_at','=',date('m'));
        } elseif($request->otherRange=="quarterly") {
            $date = new \Carbon\Carbon('-3 months');
            $jobApplication->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $brand->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $feedback->whereBetween('created_at', [$date->startOfQuarter(), now()]);
        } elseif($request->otherRange=="yearly") {
            $brand->whereYear('created_at','=',date('Y'));
            $jobApplication->whereYear('created_at','=',date('Y'));
            $feedback->whereYear('created_at','=',date('Y'));
        } elseif($request->otherRange=="custom") {
            $jobApplication->whereBetween('created_at',[$request->otherstartDate, $request->otherendDate]);
            $brand->whereBetween('created_at',[$request->otherstartDate, $request->otherendDate]);
            $feedback->whereBetween('created_at',[$request->otherstartDate, $request->otherendDate]);
        } elseif($request->otherRange=="todate" && $request->otherToDate!="") {
            $jobApplication->where('created_at', '<=', $request->otherToDate);
            $brand->where('created_at', '<=', $request->otherToDate);
            $feedback->where('created_at', '<=', $request->otherToDate);
        }


        $feedbackCount = $feedback->count();
        $userCount = $users->count();
        $directUserCount = $directUser->count();
        $referralUserCount = $referralUser->count();
        $suspendedUsersCount = $suspendedUsers->count();
        $reportedUsersCount = $reportedUsers->count();
        $jobApplicationCount = $jobApplication->count();
        $brandCount = $brand->count();
        $totalItmes= $item->count();
        $priceSum= (int)$priceSum->sum('price');
        $feedbackCount = $feedback->count();
        $itemCount = round($totalItmes / ($userCount == 0 ? 1: $userCount));
        $pricePerCategory = round($priceSum / ($category == 0 ? 1: $category));

        $boxes = [
            'users' => [
                ['text' => 'All Users','value' => $userCount],
                ['text' => 'Users: Direct Registered','value' => $directUserCount],
                ['text' => 'Users: Joined Via Referral','value' => $referralUserCount],
                ['text' => 'Reported Users','value' => $reportedUsersCount],
                ['text' => 'Suspended Users','value' => $suspendedUsersCount],
            ],
            'category' => [
                ['text' => 'Average Items/Profile','value' => $itemCount],
                ['text' => 'Total Itmes','value' => $totalItmes],
                ['text' => 'Price Per Category','value' => $pricePerCategory],

            ],
            'other' => [
                ['text' => 'Brand','value' => $brandCount],
                ['text' => 'Feedback','value' => $feedbackCount],
                ['text' => 'Job Application','value' => $jobApplicationCount],

            ],
        ];

        return view("admin.dashboard")->with([
            'userRange' => $request->userRange,
            'userstartDate' => $request->userstartDate ?? "",
            'userendDate' => $request->userendDate ?? "",
            'userTodate' => $request->userTodate ?? "",
            'userAge' => $request->userAge ?? "",
            'catRange' => $request->catRange,
            'catstartDate' => $request->catstartDate ?? "",
            'catendDate' => $request->catendDate ?? "",
            'catTodate' => $request->catTodate ?? "",
            'currentCategory' => $request->currentCategory ?? "",
            'otherstartDate' => $request->otherstartDate ?? "",
            'otherendDate' => $request->otherendDate ?? "",
            'otherRange' => $request->otherRange ?? "",
            'otherToDate' => $request->otherToDate ?? "",
            'boxes' => $boxes,
            'jobApplication' => $jobApplicationCount,
            'categories' => $categories,
        ]);

    }


}
