<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\User;

class RaffleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getalldata()
     {
         if(isset($_REQUEST['order'])) {
             $users = User::with('referredFriends')->get();
         } else {
            $collaction= collect();
        
            $month=date('m',strtotime("+1 month"));
            while ($month > 1){
               //dd(date('m',strtotime("+1 month")));
                $month=$this->PreviousMonth($month);
                $year=2023;
                $users=User::with('referredFriends')->whereHas('referredFriends', function($q) use($month,$year){
                    $q->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->where('is_profile_setup',1);
                })->get();
    
                // if($month=='2')
                // {
                //     dd($users);
                // }
                //$users
               foreach($users as $us)
               {
                if(count($us->referredFriends) > 4)
                {
                    $data=$us->toArray();
                    $data['month']=$this->MonthName($month);
                    $data['year']=$year;
                    $data['count']=count($us->referredFriends);
                    $collaction->push($data);
                }
                }
            } 

            $user_date=$collaction;
         }
         return Datatables::of($user_date)
             ->addIndexColumn()
             ->addColumn('action', function($users) {
                 return $users['month']." / ".$users['year'];
             })
             ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user['id'];
                    },
                    'data-url' => function($user) {
                        return url("/raffle-users/".$user['id']);
                    },
                ])
             ->rawColumns(['name','reported_by','action'])
             ->make(true);
     }

    public function index()
    {
        $collaction= collect();
        
        $month=date('m',strtotime("+1 month"));
        while ($month > 1){
           //dd(date('m',strtotime("+1 month")));
            $month=$this->PreviousMonth($month);
            $year=2023;
            $users=User::with('referredFriends')->whereHas('referredFriends', function($q) use($month,$year){
                $q->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->where('is_profile_setup',1);
            })->get();

            // if($month=='2')
            // {
            //     dd($users);
            // }
            //$users
           foreach($users as $us)
           {
            if(count($us->referredFriends) > 4)
            {
                $data=$us->toArray();
                $data['month']=$this->MonthName($month);
                $data['year']=$year;
                $data['count']=count($us->referredFriends);
                $collaction->push($data);
            }
            }
        } 
        
        $user_date=$collaction->all();
        return view('admin.ReferralUsers.Raffle',compact('user_date'));
    }

    public function PreviousMonth($month)
    {
        return $month-1;
    }
    
    public function MonthName($month)
    {
        return date("F", mktime(0, 0, 0, $month, 1));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "User";

        $user = User::with('referredFriends')->where('id',$id)->first();
        if($user){
            return view('admin.ReferralUsers.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
