<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FeedbackController extends Controller
{
    public function getalldata()
    {
        $feedback = Feedback::orderBy('id', 'DESC')->get();
        return Datatables::of($feedback)
                ->addIndexColumn()
                ->editColumn('name', function($feedback){

                    $fullName = $feedback->User->first_name.' '.$feedback->User->last_name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$feedback->User->image.'"></a>';

                    return $fullName;

                })
                ->editColumn('feedback_text', function($feedback){
                    $email = isset($feedback->feedback_string)? trim($feedback->feedback_string) : "N/A";
                    return $email;
                })
                ->editColumn('device_type', function($feedback){
                    $deviceType = json_decode($feedback->device_detail,true)['deviceType'];

                    $device_type = "N/A";
                    if(isset($deviceType)){
                        if ($deviceType == '1') {
                            $device_type = 'iOS';
                        } else{
                            $device_type = 'Android';
                        }
                    }
                    return $device_type;
                })
                ->editColumn('version_code', function($feedback){
                    $versionCode = json_decode($feedback->device_detail,true)['versionCode'];

                    $version_code = isset($versionCode) ? trim($versionCode) : "N/A";
                    return $version_code;
                })
                ->editColumn('mobile', function($feedback){
                    $mobile = isset($feedback->User->mobile_number) ? trim($feedback->User->mobile_number) : "N/A";
                    return $mobile;
                })
                ->editColumn('submitted_on', function($feedback){
                    $submitted_on = ! empty($feedback->created_at) ? Carbon::createFromFormat('Y-m-d H:i:s', $feedback->created_at)->format('d M Y g:i A') : 'N/A';
                    return $submitted_on;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($feedback) {
                        return $feedback->id;
                    },
                    'data-url' => function($feedback) {
                        return url("/Feedback/".$feedback->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Feedback List";
        $users = Feedback::orderBy('id', 'DESC')->get();
        return view('admin.Feedback.index',compact('pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "Feedback Details";
        $feedback = Feedback::where('id',$id)->first();
        if($feedback){
            return view('admin.Feedback.view', compact('feedback','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        //
    }
}
