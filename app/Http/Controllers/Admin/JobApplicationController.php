<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\ReportUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JobApplicationController extends Controller
{
    public function getAllData(){
        if(isset($_REQUEST['order'])){
            $jobApplication = JobApplication::get();
        }
        else{
            $jobApplication = JobApplication::orderBy('id','DESC')->get();
        }

        return Datatables::of($jobApplication)
                ->addIndexColumn()
                ->editColumn('name', function($jobApplication){
                   return $jobApplication->first_name .' '.$jobApplication->last_name;
                })
                ->editColumn('birthdate', function($jobApplication){
                   return ! empty($jobApplication->birthdate) ?
                    Carbon::createFromFormat('Y-m-d', $jobApplication->birthdate)->format('d M Y') : 'N/A';
                })
                ->editColumn('role', function($jobApplication){
                   return $jobApplication->role;
                })
                ->editColumn('city', function($jobApplication){
                   return $jobApplication->city;
                })
                ->editColumn('country', function($jobApplication){
                   return $jobApplication->country;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($jobApplication) {
                        return $jobApplication->id;
                    },
                    'data-url' => function($jobApplication) {
                        return url("job-application/".$jobApplication->id);
                    },
                    'data-for' => function($jobApplication) {
                        return 'all';
                    }
                ])
                ->rawColumns(['name'])
                ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Job Applications";
        return view('admin.jobApplication.index', compact('pageTitle'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "Job Application Details";
        $jobApplication = JobApplication::find($id);
        if($jobApplication){
            return view('admin.jobApplication.view', compact('jobApplication','page'));
        }else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApplication  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobApplication = JobApplication::find($id);
        $jobApplication->first_name = $request->first_name;
        $jobApplication->last_name = $request->last_name;
        $jobApplication->role = $request->role;
        $jobApplication->country = $request->country;
        $jobApplication->city = $request->city;
        $jobApplication->social_links = $request->social_links;

        if($jobApplication->save()){
            return redirect()->to('job-application.index')->with("success","Job Application updated successfully !");
        }else{
            return redirect()->back()->with("error","Something wents wrong !");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobApplication = JobApplication::find($id);
        if($jobApplication->delete()){
            return redirect(route('job-application.index'))->with("success","Job Application deleted successfully !");
        }else{
            return redirect()->back()->with("error","Something wents wrong !");
        }
    }
}
