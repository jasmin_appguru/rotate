<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\AffiliationManagement;

class AffiliationManagementController extends Controller
{
    public function getalldata()
    {
        if (isset($_REQUEST['order'])) {
            $affiliate = AffiliationManagement::get();
        } else {
            $affiliate = AffiliationManagement::orderBy('id', 'DESC')->get();
        }

        return Datatables::of($affiliate)
                ->addIndexColumn()
                ->editColumn('name', function($affiliate){
                    $fullName = $affiliate->name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$affiliate->image.'"></a>';
                    return $fullName;

                })
                ->editColumn('email', function($affiliate){
                    $email = isset($affiliate->email)?trim($affiliate->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($affiliate){
                    $mobile = isset($affiliate->phone) ? trim($affiliate->phone) : "N/A";
                    return $mobile;
                })
                ->editColumn('business', function($affiliate){
                    return $affiliate->business;
                })
                ->addColumn('url', function($affiliate){
                    $url = '<a href="'.$affiliate->url.'" target="_blank">'.$affiliate->url.'</a>';
                    return $url;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($affiliate) {
                        return $affiliate->id;
                    },
                    'data-url' => function($affiliate) {
                        return url("/Affiliate/".$affiliate->id);
                    },
                ])
                ->rawColumns(['name','status','url'])
                ->make(true);
    }


    public function index()
    {
        return view('admin.affiliationManagement.index');
    }

    public function create()
    {
        return "create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        ini_set("memory_limit", "100M");
        ini_set('post_max_size', '50M');
        ini_set('upload_max_filesize', '50M');
        $affiliate = new AffiliationManagement();

        $affiliate->url=$request->afflink;

        $affiliate->name=$request->client_name;
        $affiliate->email=$request->email;
        $affiliate->phone=$request->phone;
        $affiliate->business=$request->business_company_name;


        if(!empty($request->logo)) {
            if($request->logo){
                $imageMainPath = '/uploads/affiliate_image/';
                if (!is_dir(public_path($imageMainPath))) {
                    mkdir(public_path($imageMainPath), 777, true);
                }

                $profile_image = "user-pro-".time().".png";
                $path = public_path($imageMainPath.$profile_image);

                $image = $request->logo;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path, base64_decode($image));
                $affiliate->logo = $profile_image;
            } else{
                return redirect()->back()->with("danger","Please select jpg, png, gif only");
            }
        }
        $affiliate->save();
        return redirect()->back()->with("success","Partner details have been created successfully.");
    }

    public function destroy($id)
    {
        $affiliate = AffiliationManagement::find($id);

        if($affiliate) {
            $affiliate->delete();
            return response()->json(['success' => true]);
        } else {
            response()->with("error","Opps!! Something went wrong. Please try again.");
            return response()->json(['success' => false]);
        }
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "Affiliate";

        $affiliate = AffiliationManagement::where('id',$id)->first();
        if($affiliate){
            return view('admin.affiliationManagement.view', compact('affiliate','page'));
        } else {
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        ini_set("memory_limit", "100M");
        ini_set('post_max_size', '50M');
        ini_set('upload_max_filesize', '50M');
        $affiliate = AffiliationManagement::find($id);
        $affiliate->url=$request->afflink;
        $affiliate->name=$request->client_name;
        $affiliate->email=$request->email;
        $affiliate->phone=$request->phone;
        $affiliate->business=$request->business_company_name;


        if(!empty($request->logo)) {
            if($request->logo){
                $imageMainPath = '/uploads/affiliate_image/';
                if (!is_dir(public_path($imageMainPath))) {
                    mkdir(public_path($imageMainPath), 777, true);
                }

                $profile_image = "user-pro-".time().".png";
                $path = public_path($imageMainPath.$profile_image);

                $image = $request->logo;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path, base64_decode($image));
                $affiliate->logo = $profile_image;
            } else{
                return redirect()->back()->with("danger","Please select jpg, png, gif only");
            }
        }
        $affiliate->save();
        return redirect()->back()->with("success","Partner details have been updated successfully.");
    }
}
