<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Models\ReportUser;
use App\Models\User;
use App\Notifications\SendActionMails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;


class ReportedUsersController extends Controller
{
    public function exportUser()
    {
        $reportUser = ReportUser::with(['reported_user'])->get();
        return Excel::download(new UsersExport($reportUser->each->reported_user), 'Reported-Users-'.date('d-m-Y').'.csv');
    }
    public function getalldata()
    {
        if(isset($_REQUEST['order'])) {
            $users = ReportUser::with(['reported_user','reported_by_user'])->get();
        } else {
            $users = ReportUser::with(['reported_user','reported_by_user'])->orderBy('id', 'DESC')->get();
        }
        return Datatables::of($users)
            ->addIndexColumn()
            ->editColumn('name', function($users) {
                $fullName = $users->reported_user->first_name.' '.$users->reported_user->last_name;
                $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->reported_user->image.'"></a>';
                return $fullName;

            })
            ->editColumn('reported_by', function($users) {
                $fullName = $users->reported_by_user->first_name.' '.$users->reported_by_user->last_name;
                $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->reported_by_user->image.'"></a>';
                return $fullName;
            })
            ->editColumn('reason', function($users) {
                $reason = isset($users->reason) ? trim($users->reason) : "N/A";
                return $reason;
            })
            ->editColumn('other_reason', function($users) {
                $otherReason = !empty($users->otherReason) ? trim($users->otherReason) : "N/A";
                return $otherReason;
            })
            ->addColumn('status', function($users) {
                $isActive = ($users->reported_user->status == User::ACTIVE) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                return $isActive;
            })
            ->addColumn('action', function($users) {
                $actionButtons = '<a href="javascript:void(0)" onclick="removeUser(\''.route("reported-users.destroy",['user' => $users->reported_user]).'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-delete" title="Delete" aria-hidden="true"></i></span></a>';
                if ($users->reported_user->status == User::ACTIVE){
                    $actionButtons .= '<a href="javascript:void(0)" onclick="changeStatus(\''.route("reported-users.suspend",['user' => $users->reported_user,'action' => User::INACTIVE]).'\',\''.User::INACTIVE.'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-settings" title="Suspend" aria-hidden="true"></i></span></a>';
                } else {
                    $actionButtons .= '<a href="javascript:void(0)" onclick="changeStatus(\''.route("reported-users.suspend",['user' => $users->reported_user,'action' => User::ACTIVE]).'\',\''.User::ACTIVE.'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-settings" title="Sustain" aria-hidden="true"></i></span></a>';
                }
                $actionButtons .= '<a href="javascript:void(0)" onclick="showBlockUserModal(\''.route("reported-users.block",['user' => $users->reported_user]).'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-block" title="Block" aria-hidden="true"></i></span></a>';
                return $actionButtons;
            })
            ->rawColumns(['name','reported_by','status','action'])
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ReportedUsers.index');
    }

    /**
     * Make User Active/Inactive the specified user.
     *
     * @param  int  $id
     * @param string $action
     * @return \Illuminate\Http\Response
     */
    public function suspend($id,$action)
    {
        $user = User::find($id);
        if($action == User::INACTIVE) {
            $user->status = User::INACTIVE;
        } elseif($action == User::ACTIVE) {
            $user->status = User::ACTIVE;
        }
        $user->save();
        if($action == User::INACTIVE) {
            $user->notify(new SendActionMails($user));
        }

        if(!empty($user->firebaseId))
        {
            if($action == User::INACTIVE) {
                firebaseSuspend($user->firebaseId,1);
            } elseif($action == User::ACTIVE) {
                firebaseSuspend($user->firebaseId,0);
            }
            \Log::info($user->firebaseId." Suspend By Admin ");
        }
        // firebaseId

        return response()
            ->json(['success' => true]);
    }

    /**
     * Block the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function block($id)
    {
        try {
            $blockPeriod = request()->get('block_period');
            $reportUser = ReportUser::where('user_id',$id)->first();

            if ($blockPeriod == User::BLOCK_FIFTHTEEN_DAYS) {
                $blockedTillDate = now()->addDays(15);
            } elseif ($blockPeriod == User::BLOCK_ONE_MONTH) {
                $blockedTillDate = Carbon::now()->addMonth();
            } elseif ($blockPeriod == User::BLOCK_THREE_MONTH) {
                $blockedTillDate = Carbon::now()->addMonths(3);
            } elseif ($blockPeriod == User::BLOCK_SIX_MONTH) {
                $blockedTillDate = Carbon::now()->addMonths(6);
            } elseif ($blockPeriod == User::BLOCK_ONE_YEAR) {
                $blockedTillDate = Carbon::now()->addYear();
            }

            // Change Status to Blocked in Users table
            $user = User::find($reportUser->user_id);
            $user->status = User::BLOCKED;
            $user->blocked_until = $blockedTillDate;
            $user->save();

            if(!empty($user->firebaseId))
            {
                firebaseBlock($user->firebaseId,1);
                \Log::info($user->firebaseId."  Block By Admin ");
            }


            // Remove user from report_user table
            $reportUser->delete();
            $user->notify(new SendActionMails($user));

            return response()
                ->json(['success' => true]);
        } catch(Exeception $e) {
                return response()
                    ->json(['success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = ReportUser::where('user_id',$id)->firstOrFail();
        deleteFireBaseUser($user->email);
        $user->delete();
        return response()
            ->json(['success' => true]);
    }
}
