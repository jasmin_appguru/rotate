<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ReferralController extends Controller
{
    public function exportUser()
    {
        $reportUser = User::with(['reported_user'])->get();
        return Excel::download(new UsersExport($reportUser->each->reported_user), 'Reported-Users-'.date('d-m-Y').'.csv');
    }
    public function getalldata()
    {
        if(isset($_REQUEST['order'])) {
            $users = User::with('referredFriends')->get();
        } else {
            $users = User::with('referredFriends')->orderBy('id', 'DESC')->get();
        }
        return Datatables::of($users)
            ->addIndexColumn()
            ->editColumn('name', function($users) {
                $fullName = $users->first_name.' '.$users->last_name;
                $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->image.'"></a>';
                return $fullName;
            })
            ->editColumn('counter', function($users) {
                $user=User::where('joinReferralCode',$users->referralCode)->count();
                return $user;
            })
            ->editColumn('month', function($users) {
                $user=User::where('joinReferralCode',$users->referralCode)->whereMonth('created_at', '=', date('m'))->count();
                return $user;
            })
            ->editColumn('year', function($users) {
                $user=User::where('joinReferralCode',$users->referralCode)->whereYear('created_at', '=', date('Y'))->count();
                return $user;
            })
            ->editColumn('total', function($users) {
                $user=User::where('joinReferralCode',$users->referralCode)->count();
                return $user;
            })

            ->addColumn('action', function($users) {
               //$actionButtons = '<a href="javascript:void(0)" onclick="removeUser(\''.route("reported-users.destroy",['user' => $users->reported_user]).'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-delete" title="Delete" aria-hidden="true"></i></span></a>';
                // if ($users->reported_user->status == User::ACTIVE){
                //     $actionButtons .= '<a href="javascript:void(0)" onclick="changeStatus(\''.route("reported-users.suspend",['user' => $users->reported_user,'action' => User::INACTIVE]).'\',\''.User::INACTIVE.'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-settings" title="Suspend" aria-hidden="true"></i></span></a>';
                // } else {
                //     $actionButtons .= '<a href="javascript:void(0)" onclick="changeStatus(\''.route("reported-users.suspend",['user' => $users->reported_user,'action' => User::ACTIVE]).'\',\''.User::ACTIVE.'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-settings" title="Sustain" aria-hidden="true"></i></span></a>';
                // }
                //$actionButtons .= '<a href="javascript:void(0)" onclick="showBlockUserModal(\''.route("reported-users.block",['user' => $users->reported_user]).'\',\''.$users->reported_user->first_name.' '.$users->reported_user->last_name.'\')" class="btn btn-xs"><span><i class="site-menu-icon md-block" title="Block" aria-hidden="true"></i></span></a>';
                return $users['month'];
            })

            ->rawColumns(['name','reported_by','action'])
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ReferralUsers.index');
    }
}
