<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Twilio\Rest\Client;


class TwilioSMSController extends Controller
{
    public function index()
    {
        $receiverNumber = "+917096111131";
        $message = __('api.otp_massage',['OTP' => 142536,'purpose'=>'signup']);
        //dd($message);
        try {
            $account_sid = config('portae_admin.TWILIO_SID');
            $auth_token = config('portae_admin.TWILIO_TOKEN');
            $twilio_number = config('portae_admin.TWILIO_FROM');
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message]);

            dd('SMS Sent Successfully.');

        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    }
}
