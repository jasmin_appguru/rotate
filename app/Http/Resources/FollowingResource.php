<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FollowingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'userId' => $this->following->id,
            "userName" => $this->following->user_name,
            "firstName" => $this->following->first_name,
            "lastName" => $this->following->last_name,
            "profilePic" => $this->following->image,
            "profilePicThumb" => $this->following->thumb ?? '',
            "following" => IsMyFollower($this->following->id),
        ];
        return parent::toArray($request);
    }
}
