<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArchiveItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "itemId"=>$this->id,
            "url" => $this->thumbUrl ?? '',
            "thumb" => $this->ThumbUrl,
            "userName" => $this->user->user_name,
            "rackName" => ucfirst($this->SubCategory->name),
            "price" => (string)round($this->price)
        ];
        return parent::toArray($request);
    }
}
