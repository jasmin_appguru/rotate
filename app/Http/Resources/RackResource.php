<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'rackId' => $this->id,
            'rackName' => $this->name,
            'rackUrl' => $this->CatUrl,
            'orderId' => $this->seq
        ];
    }
}
