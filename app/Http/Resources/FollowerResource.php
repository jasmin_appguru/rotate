<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FollowerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'userId' => $this->follower->id,
            "userName" => $this->follower->user_name,
            "firstName" => $this->follower->first_name,
            "lastName" => $this->follower->last_name,
            "profilePic" => $this->follower->image,
            "profilePicThumb" => $this->follower->thumb ?? '',
            "following" => IsMyFollower($this->follower->id),
        ];
        return parent::toArray($request);
    }
}
