<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class FriendListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'friendName' => $this->first_name." ".$this->last_name,
            'joiningDate' => $this->created_at->format('d-m-Y')
        ];
        return parent::toArray($request);
    }
}
