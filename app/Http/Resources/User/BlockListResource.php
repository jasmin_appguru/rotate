<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class BlockListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "userId"=> $this->ToUser->id,
            "firstName"=> $this->ToUser->first_name,
            "lastName" => $this->ToUser->last_name,
            "profilePic" => $this->ToUser->image,
            "firebaseId" => $this->ToUser->firebaseId,
            "userName" => $this->ToUser->user_name,
        ];
    }
}
