<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class GetProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
                "userId" => $this->id,
                "firebaseId" => $this->firebaseId ?? '',
                "firstName" => $this->first_name ?? '',
                "lastName" => $this->last_name ?? '',
                "profilePic" => $this->image ?? '',
                "profilePicThumb" => $this->thumb ?? '',
                "userName" => $this->user_name?? "not set yet",
                "location" => $this->location ?? "not set yet",
                "followStatus" => MyFollower($this->id) ? 1 : 0 ,
                "blockStatus" => $this->BlockCount ?? 0,
                "isPublic" => $this->visibility,
                "isWardrobePublic" => $this->wardrobe_visibility ?? 0,
                "isReported" => $this->ReportCount ?? 0,
                "latitude" => $this->latitude ?? '0',
                "longitude" => $this->longitude ?? '0',
                "notificationStatus" => StatusEnableNotification(\Auth::user()->id,$this->id),
                "followersCount" => $this->FollowersCount ?? 0,
                "followingCount" => $this->FollowingCount ?? 0,
                "totalItemCount" => $this->ItemCount ?? 0,
        ];
        return parent::toArray($request);
    }
}
