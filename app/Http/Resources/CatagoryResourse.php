<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CatagoryResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'categoryId' => $this->id,
            'categoryName' => ucfirst($this->name),
        ];

        // return [
        //     'subCategoryId' => $this->id,
        //     'subCategoryName' => $this->name,
        //     'catUrl' => $this->CatUrl,
        //     'racks' => RackResource::collection($this->MyRack),
        // ];
        return parent::toArray($request);
    }
}
