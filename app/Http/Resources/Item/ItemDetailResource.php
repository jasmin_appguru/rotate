<?php

namespace App\Http\Resources\Item;

use App\Http\Resources\BrandResource;
use App\Http\Resources\RackResource;
use App\Http\Resources\Setting\ColorResource;
use App\Http\Resources\Parts\SubCatagoryResource;
use App\Http\Resources\Parts\PhotosResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // BrandResource;
        $subCategory = [
            'userId' => $this->user_id,
            'SubCategory' => $this->SubCategory,
        ];
        $responce = [
            "userId"=>$this->user_id,
            "itemId"=>$this->id,
            "photos"=> $this->photos ? PhotosResource::collection($this->photos) : "1",
            "likeCount"=> $this->LikesCount->count() ? CountsInWord($this->LikesCount->count()) : '0',
            "captions" => $this->captions,
            "brand" =>new BrandResource($this->brand),
            "shopLink" => $this->shopLink ?? '',
            "categoryId" => (int)$this->category_id ?? 0,
            "clothCountry" => (int)$this->clothCountry ?? 0,
            "shoesCountry" => (int)$this->shoesCountry ?? 0,
            "clothData" => $this->clothData ?? 0,
            "shoesData" => $this->shoesData ?? 0,
            "color" => ColorResource::collection($this->color),
            "isSell" => $this->is_sell,
            "isWardrobeItemPublic" => ($this->visibility=='0') ? 1 : 0,
            "priceType" => $this->priceType,
            "subCategory" => new SubCatagoryResource($subCategory),
        ];
        if ($this->is_sell==0) {
            $responce["price"] = "0";
        } else {
            $responce["price"] = (string)round($this->price);
        }
        if (isset($this->Rack)) {
            $responce["rack"] =new RackResource($this->Rack);
        }

        return $responce;
    }
}
