<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $prize="";
        if($this->is_sell==0)
        {
            $prize = "0";
        }
        else
        {
            $prize= (string)round($this->price);
        }
        return [
            'itemId' => $this->id,
            'userId' => $this->User->id,
            'url' => $this->url,
            'thumb' => $this->thumbUrl,
            'caption' => $this->captions,
            'userName' => $this->User->user_name ?? 'USER_NAME',
            'subCategory' => ucfirst($this->SubCategory->name),
            'price' => $prize,
            'likeStatus' => $this->LikeCount ?? 0,
            'orderId' => (int)$this->order ?? 0
        ];
 
    }
}
