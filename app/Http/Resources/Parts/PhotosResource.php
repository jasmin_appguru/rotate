<?php

namespace App\Http\Resources\Parts;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "imageId" => $this->id,
            "image" =>  $this->name,
            "orderId" => $this->seq
        ];
    }
}
