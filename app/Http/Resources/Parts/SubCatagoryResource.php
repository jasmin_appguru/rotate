<?php

namespace App\Http\Resources\Parts;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCatagoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'subCategoryId' => $this['SubCategory']->id,
            'subCategoryName' => $this['SubCategory']->name,
            'catUrl' => $this['SubCategory']->catagoryUrl($this['userId']),
        ];
    }
}
