<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RackResourceWardrope extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'rackId' => $this->Rack->id,
            'rackName' => $this->Rack->name,
            'rackUrl' => $this->RolesList, 
            'orderId' => $this->Rack->seq
        ];
        return parent::toArray($request);
    }
}
