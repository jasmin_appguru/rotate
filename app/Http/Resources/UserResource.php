<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use PhpParser\Node\Expr\Cast\Double;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        'secret' => $this->when($request->user()->isAdmin(), 'secret-value'),


        $responce=[
            "userId" => $this->id,
            "firebaseId" => $this->firebaseId,
            "profilePic" => $this->image ?? '',
            "profilePicThumb" => $this->thumb ?? '',
            'firstName' => $this->first_name ?? '',
            'lastName' => $this->last_name ?? '',
            "userName" => $this->user_name ?? '',
            "mobile" => $this->mobile_number ?? '',
            "dob" => $this->dob ?  date('d-m-Y',strtotime($this->dob)) : '',
            "age" => (int)$this->age ?? 0,
            "location" => $this->location ?? '',
            "latitude" => $this->latitude ?? '0',
            "longitude" => $this->longitude ?? '0',
            "referralCode" => $this->referralCode ?? '',
            'email' => $this->email,
            "isPublic" => (int)$this->visibility,
            "radius" => $this->radius ? (int)$this->radius : (int)2,
            "isWardrobePublic" => $this->wardrobe_visibility ?? 0,
            "isProfileSetUp" => (int)$this->is_profile_setup,
            "clothCountry" => (int)$this->clothCountry ?? 0,
            "shoesCountry" => (int)$this->shoesCountry ?? 0,
            "clothData" => (array)$this->clothData ?? [],
            "shoesData" => (array)$this->shoesData ?? []
        ];

        if(\Str::contains($request->getRequestUri(), 'login'))
        {
            $responce["accessToken"] = $this->tokan ?? '';
        }

        return $responce;

    }
}
