<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status=0;
        if($this->status=="Approved")
        {
            $status=1;
        }
        return [
            'brandId' => $this->id,
            'brandName' => $this->name,
            'brandStatus' => $status,
        ];
        return parent::toArray($request);
    }
}
