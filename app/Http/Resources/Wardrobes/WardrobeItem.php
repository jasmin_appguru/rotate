<?php

namespace App\Http\Resources\Wardrobes;

use Illuminate\Http\Resources\Json\JsonResource;

class WardrobeItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "subCategoryId"=> (int)$this->subCategoryId,
            "subCategoryName"=> $this->SubCategory->name,
            "catUrl" => "https://picsum.photos/300/300?random=1"
        ];
    }
}
