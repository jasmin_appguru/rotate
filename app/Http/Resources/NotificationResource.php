<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'notificationId' => $this->id,
            'notificationText' => $this->description,
            'notificationType' => $this->type,
            'notificationTime' =>  \Carbon\Carbon::parse($this->created_at)->format('d/m/Y h:i:s'),
            'postId' =>  (int)$this->postId ?? 0,
            'username' => $this->user_name ?? "",
            'url' =>  $this->url ?? "",
            'userId' => (int)$this->from_user_id ?? 0,
            // 'isRead' => $this->isRead,
        ]; 
        return parent::toArray($request);
    }
}
