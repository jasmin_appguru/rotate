<?php

namespace App\Http\Resources\favorite;

use Illuminate\Http\Resources\Json\JsonResource;

class FavoriteListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "itemId"=> $this->item_id,
            "userId"=> $this->Item->User->id,
            "url" => $this->Item->url ?? "",
            "thumb" => $this->Item->ThumbUrl ?? "",
            "userName" => $this->Item->User->user_name,
            "subCategory" => $this->Item->subCategory->name ?? "",
            "price" => ($this->is_sell=="0") ? (string)round($this->item->price) : "0",
            "likeStatus" =>  1
        ];
        return parent::toArray($request);
    }
}
