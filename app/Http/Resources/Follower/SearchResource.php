<?php

namespace App\Http\Resources\Follower;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "profilePic" =>  $this->image ?? "",
            "profilePicThumb" => $this->thumb ?? '',
            "userId" =>  $this->id,
            "userName" =>  $this->user_name,
            "location" =>  $this->location ?? '',
            "latitude" =>  $this->latitude ? (string)$this->latitude : (string)"0",
            "longitude" => $this->longitude ? (string)$this->longitude : (string)"0"
        ];
        // return parent::toArray($request);
    }
}
