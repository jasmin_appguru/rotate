<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\WelcomeMail;
use App\Scopes\ExceptMeScope;
use Laravel\Passport\HasApiTokens;
use Notification; 
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ACTIVE = 'Active';
    const INACTIVE = 'Suspended';
    const BLOCKED = 'Blocked';
    const BLOCK_FIFTHTEEN_DAYS = '15_days';
    const BLOCK_ONE_MONTH = '1_month';
    const BLOCK_THREE_MONTH = '3_month';
    const BLOCK_SIX_MONTH = '6_month';
    const BLOCK_ONE_YEAR = '1_year';

    protected $fillable = [
        'first_name',
        'last_name',
        'image',
        'user_name',
        'email',
        'password',
        'mobile_number',
        'dob',
        'age',
        'is_profile_setup',
        'device_type',
        'device_token',
        'latitude',
        'longitude',
        'location',
        'clothCountry',
        'shoesCountry',
        'clothData',
        'shoesData',
        'referralCode',
        'joinReferralCode',
        'visibility',
        'status',
        'remember_token',
        'firebaseId',
        'blocked_until'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['deviceType'];

    protected $dates = [
        'blocked_until'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'age' => 'integer',
    ];

    public $string;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
                do {
                    $code = "RT".self::generateRandomString(6);
                } while (User::where("referralCode", "=", $code)->first());
                $model->referralCode = $code;
        });
    } 

    public static function generateRandomString($length = 20) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function scopeJsNearMe($query,$latitude,$longitude,$radius)
    {
        $Item=User::selectRaw("id, latitude, longitude,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$latitude, $longitude, $latitude])
        ->having("distance", "<", $radius)
        ->orderBy("distance",'asc')
        ->pluck('id')->toArray();
        return $query->whereIn('id',$Item);
    }

    public function scopeActive($query)
    {
        return $query->where('id', '!=',\Auth::user()->id);
    }

    public function scopePublic($query)
    {
        return $query->where('visibility','1');
    }

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
    
    public function getImageAttribute($value)
    {
        
        if(empty($value))
        {
            return "";
        }
        return asset("storage/avatars/".$value);
    }

    public function getThumbAttribute()
    {
        if(empty($this->attributes['image']))
        {
            return "";
        }
        return asset("storage/avatars/thumb/".$this->attributes['image']);
    }

    public function getClothDataAttribute($value)
    {
        if(empty($value))
        {
            return [];
        }
        return  array_map('intval', explode(',', $value)); 
    }

    public function getCountReferrerAttribute()
    {
        $user = User::where('joinReferralCode', $this->referralCode)->count();

        return $user;
    }

    public function scopeCountReferrerMonth($q,$month,$year) 
    {
        $q->whereHas('referredFriends', function ($q,$year,$month) {
            $q->where('joinReferralCode', $this->referralCode)->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
        });
        
        // $user = $q->where('joinReferralCode', $this->referralCode)->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);

        return $q;
        // if (($user = \Auth::user()) && ($office = $user->office_id)) {
        // $q->where('office_id', $office);
        // }
    }

    public function setClothDataAttribute($value)
    {
        $this->attributes['clothData'] = implode(",",$value);
    }
    
    public function getShoesDataAttribute($value)
    {
        if(empty($value))
        {
            return [];
        }
        return  array_map('intval', explode(',', $value)); 
    }

    public function setShoesDataAttribute($value)
    {
        $this->attributes['shoesData'] = implode(",",$value);
    }

      public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token,$this->email,$this));
    }

    public function WelcomeMail()
    {
            $details = [
                'greeting' => 'Hello '.$this->first_name.", ",
                'body' => 'This is my first notification from Nicesnippests.com',
                'thanks' => 'Thank you for using Nicesnippests.com tuto!',
                'actionURL' => url('/'),
            ];
            //$this->notify(new WelcomeMail($details));
            Notification::send($this, new WelcomeMail($details));

    }

    public function GetFollowersCountAttribute()
    {
        return $this->Follower->count();
    }

    public function GetFollowingCountAttribute()
    {
        return $this->Following->count();
    }
    public function GetReportCountAttribute()
    {
        if(!empty($this->Report))
        {
            return 1;
        }
        else
        {
            return 0;
        }
        // return $this->Report->count();
    }
    public function GetBlockCountAttribute()
    {
        // return User::whereHas('Block', function($q){
        //     $q->where('user_id', '=', \Auth::user()->id);
        //     $q->where('to_user_id', '=', $q->id);
        // })->first();
        if(!empty($this->Block))
        {
            return 1;
        }
        else
        {
            return 0;
        }
        // return $this->Block;
    }

    public function GetItemCountAttribute()
    {
        return $this->Item->count();
    }

    public function Follower()
    {
        return $this->hasMany(Follower::class,'to_user_id','id');
    }
    public function Following()
    {
        return $this->hasMany(Follower::class,'from_user_id','id');
    }
    public function Block()
    {
        return $this->hasOne(BlockUser::class,'to_user_id','id')->where('user_id',\Auth::user()->id)->where('to_user_id',$this->id);
    }
    public function Item()
    {
        return $this->hasMany(Item::class,'user_id','id');
    }
    public function Report()
    {
        return $this->hasOne(ReportUser::class,'user_id','id')->where('from_user_id',\Auth::user()->id)->where('user_id',$this->id);;
    }

    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function referredFriends()
    {
        return $this->hasMany(User::class,'joinReferralCode','referralCode');
        //->where('joinReferralCode',$this->referralCode);
    }
    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function lastItemPosted()
    {
        return $this->hasOne(Item::class,'user_id','id')->latest('id')->take(1);
    }
}
