<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Item extends Model
{
    use HasFactory;

    const PUBLISH = '0';
    const DRAFT = '1';
    //protected $fillable=[];

    protected static function boot()
    {
        parent::boot();
  
        static::addGlobalScope(new ActiveScope);
    }
    

     //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Scopes
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function scopeJsNearMe($query,$latitude,$longitude,$radius)
    {
        $Item=User::selectRaw("id, captions, latitude, longitude,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$latitude, $longitude, $latitude])
        ->having("distance", "<", $radius)
        ->orderBy("distance",'asc')
        ->pluck('id')->toArray();
        return $query->whereIn('user_id',$Item);
    }

    protected function scopeJsNearMeItem($query,$latitude,$longitude)
    {
        $radius=\Auth::user()->radius ?? 100;
        $Item=User::selectRaw("id, latitude, longitude,
                ( 6371 * acos( cos( radians(?) ) *
                cos( radians( latitude ) )
                * cos( radians( longitude ) - radians(?)
                ) + sin( radians(?) ) *
                sin( radians( latitude ) ) )
                ) AS distance", [$latitude, $longitude, $latitude])
        ->having("distance", "<", $radius)
        ->orderBy("distance",'asc')
        ->pluck('id')->toArray();

        return $query->whereIn('user_id',$Item);
    }

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }

    protected function scopeJsPublish($query)
    {
        return $query->where('visibility','0');
    }

    protected function scopeJsPublishWithMyHidden($query,$user)
    {
        if($user==\Auth::user()->id)
        {
            return $query;
        }
        else
        {
            // return $query->whereNotIn('id',GetItemListArchived($user));
            return $query->where('visibility','0');
        }

    }
    protected $appends = ['roles_list'];

    public function scopeSelectedUser($query, $manager)
    {
        return $query->where('user_id', $manager->id);
    }

    public function scopeFilterClothData($query, $clothData)
    {
        $i=0;
        foreach($clothData as $list)
        {
            if($i==0)
            {
                $query->Where("clothData","like","%".$list."%");
            }
            else
            {
                $query->orWhere("clothData","like","%".$list."%");
            }
            $i++;
        }
            return $query;
    }
    
    public function scopeFilterShoesData($query, $clothData)
    {
        $i=0;
        foreach($clothData as $list)
        {
            if($i==0)
            {
                $query->Where("shoesData","like","%".$list."%");
            }
            else
            {
                $query->orWhere("shoesData","like","%".$list."%");
            }
            $i++;
        }
        return $query;
    }

    public function scopeFilterColor($query, $colors)
    {
        $query->whereIn("color", $colors);
        return $query;
    }

    

    public function getRolesListAttribute()
    {
        return asset($this->Image());
    }
  
    public function Image()
    {
        return $this->photos()->orderBy('id', 'DESC')->first()->name ?? "Default.php";
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Relations
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function User()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Rack()
    {
        return $this->belongsTo(Rack::class, 'rackId', 'id');
    }

    public function Brand()
    {
        return $this->belongsTo(Brand::class,'brand_Id','id');
    }
    public function photos()
    {
        return $this->hasMany(ItemPhoto::class)->orderBy('seq','asc');
    }

    public function Category()
    {
        return $this->belongsTo(Category::class,'subCategoryId','id');
    }

    public function SubCategory()
    {
        return $this->belongsTo(Category::class,'subCategoryId','id');
    }

    public function Likes()
    {
        return $this->hasOne(Favorite::class)->where('user_id',\Auth::user()->id)->where('item_id',$this->id);
    }

    public function LikesCount()
    {
        return $this->hasMany(Favorite::class,'item_id','id');
    }

    public function hasPosts(): bool
    {
        return $this->Archive()->exists();
    }

    public function Archive()
    {
        return $this->hasOne(ArchiveItem::class);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Attribute
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getColorAttribute($value)
    {
        if(empty($value))
        {
            return [];
        }
        return Colors::whereIn('id',explode(',', $value))->get();
    }

    public function setColorAttribute($value)
    {
        if(!empty($value))
        {
            $this->attributes['color'] = implode(",",json_decode($value));
        }
    }
    public function getClothDataAttribute($value)
    {
        if(empty($value))
        {
            return [];
        }
        return  array_map('intval', explode(',', $value)); 
    }

    public function setClothDataAttribute($value)
    {
        if(!empty($value))
        {
            $this->attributes['clothData'] = implode(",",json_decode($value));
        }
    }
    
    public function getShoesDataAttribute($value)
    {
        if(empty($value))
        {
            return [];
        }
        return  array_map('intval', explode(',', $value)); 
    }

    public function setShoesDataAttribute($value)
    {
        if(!empty($value))
        {
        $this->attributes['shoesData'] = implode(",",json_decode($value));
        }
    }

    public function getUrlAttribute()
    {
        return $this->photos()->latest()->first() ? $this->photos()->latest('id')->first()->name : "https://picsum.photos/300/300?random=1";
    }

    public function getThumbUrlAttribute()
    {
        return $this->photos()->first() ? $this->photos()->first()->thamb : "https://picsum.photos/300/300?random=1";
    }

    public function GetLikeCountAttribute()
    {
        // return User::whereHas('Block', function($q){
        //     $q->where('user_id', '=', \Auth::user()->id);
        //     $q->where('to_user_id', '=', $q->id);
        // })->first();
        if(!empty($this->Likes))
        {
            return 1;
        }
        else
        {
            return 0;
        }
        // return $this->Block;
    }
}
