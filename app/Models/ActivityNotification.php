<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityNotification extends Model
{
    use HasFactory;

    protected $fillable=['notify_id','user_id'];
    
    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
}
