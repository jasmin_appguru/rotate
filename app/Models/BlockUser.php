<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockUser extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'to_user_id'
    ];

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
    
    public function FromUser()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function ToUser()
    {
        return $this->belongsTo(User::class,'to_user_id','id');
    }
}
