<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $appends = ['CatUrl'];

    public function getNameAttribute($q)
    {
        return ucfirst($q);
    }

    public function getCatUrlAttribute($q)
    {
        //return $this->Items()->where('rack_id',null)->where('user_id',\Auth::user()->id)->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
        return $this->Items()->where('user_id',\Auth::user()->id)->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
    }

    public function catagoryUrl($userId)
    {
        return $this->Items()->where('user_id',$userId)->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
    }

    public function getUserCatUrlAttribute($q)
    {
        //return $this->Items()->where('rack_id',null)->where('user_id',\Auth::user()->id)->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
        return $this->Items()->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
    }

    public function Items()
    {
        return $this->hasMany(ItemPhoto::class);
    }

    public function MyRack()
    {
        return $this->hasMany(Rack::class,'sub_category_id')->where('user_id',\Auth::user()->id);
    }

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }
    
    public function SubCategory()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }
}
