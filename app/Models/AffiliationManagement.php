<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffiliationManagement extends Model
{
    use HasFactory;

    protected $fillable=[
        'name',
        'email',
        'phone',
        'business',
        'campaign',
        'url',
        'logo',
    ];

    public function getLogoAttribute($value)
    {
        return asset("uploads/affiliate_image/".$value);
    }

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
}
