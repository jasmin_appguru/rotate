<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    use HasFactory;

    protected $guarded=[];

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }


    public function Follower()
    {
        return $this->belongsTo(User::class,'from_user_id');
    }
    public function Following()
    {
        return $this->belongsTo(User::class,'to_user_id');
    }
}
