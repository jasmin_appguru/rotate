<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportUser extends Model
{
    use HasFactory;

    protected $fillable=[
        'from_user_id',
        'user_id',
        'reason',
        'otherReason',
    ];

    public static function boot()
    {
        parent::boot();
    }
    /**
     * Get the user who is reported.
     */
    public function reported_user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    /**
     * Get the user who reported user.
     */
    public function reported_by_user()
    {
        return $this->belongsTo(User::class,'from_user_id','id');
    }
}
