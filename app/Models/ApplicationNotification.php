<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationNotification extends Model
{
    use HasFactory;

    const READ = '1';
    const UNREAD = '0';


    public function User()
    {
        return $this->belongsTo(User::class);
    }

    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
}
