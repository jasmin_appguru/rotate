<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemPhoto extends Model
{
    use HasFactory;

    protected $fillable=[
        'seq'
    ];

    public function getnameAttribute($value)
    {
        return  asset('storage/item/'.$value);
    }

    public function getthambAttribute($value)
    {
        return  asset('storage/item/thumb/'.$this->getRawOriginal('name'));
    }

   
    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }

    public function detail()
    {
        return $this->belongsTo(Item::class);
    }
}
