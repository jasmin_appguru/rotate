<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    const PUSH_NOTIFICATION = 2;
    const BIRTH_NOTIFICATION = 3;
    const FAVORITE_ITEM = 4;
    const REFERRAL_NOTIFICATION = 5;
    const POSTING_FIRST_ITEM_REMINDER_NOTIFICATION = 6;
    const FOLLOWED_USER_ADD_ITEM = 7;
    const POSTING_ITEM_REMINDER_NOTIFICATION = 8;
    const REFERRAL_REMINDER_NOTIFICATION = 9;
    const LIKE_SUMMARY = 10;
    const NEW_FOLLOWER = 11;

    //protected $fillable=[];
    
    protected function scopeJsPagination($query,$Offset)
    {
        return $query->skip($Offset * ApiPaginationDefault())->take(ApiPaginationDefault());
    }
}
