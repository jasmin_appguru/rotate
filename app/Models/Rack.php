<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'name',
        'category_id',
        'sub_category_id',
    ];

    protected $appends = ['CatUrl'];

    protected function scopeJsPublishWithMyHidden($query,$user)
    {
        if($user==\Auth::user()->id)
        {
            return $query;
        }
        else
        {
            return $query->where('visibility','0');
        }

    }

    public function WithOutArchive($user)
    {
        return $this->ItemsArchive($user);
    }

    public function getCatUrlAttribute($q)
    {
        $list="0";
        return $this->ItemsPhotos()->orderBy('id','DESC')->first()->name ?? asset("storage/item/")."/default.jpeg";
    }

    public function Items()
    {
        return $this->hasMany(Item::class,'rackId','id');
    }
    public function ItemsPhotos()
    {
        return $this->hasMany(ItemPhoto::class);
    }

    public function ItemsArchive($id)
    {
        return $this->hasMany(ItemPhoto::class)->where('user_id','11');
    }
}
