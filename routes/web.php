<?php

use App\Http\Controllers\Admin\AffiliationManagementController;
use App\Http\Controllers\Admin\ApiMessagesController;
use App\Http\Controllers\Admin\BlockController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\OwnerUserController;
use App\Http\Controllers\Admin\NotificationManagerController;
use App\Http\Controllers\Admin\PushNotificationController;
use App\Http\Controllers\Admin\ReportedUsersController;
use App\Http\Controllers\ReferralController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\JobApplicationController;
use App\Http\Controllers\Admin\RaffleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TwilioSMSController;
use Kreait\Firebase\Contract\Firestore;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Core\Timestamp;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Google\Cloud\Firestore\FieldValue;
use Google\Cloud\Storage\StorageClient;
use Kreait\Firebase\Auth;

use App\Models\Admin\PushNotification;
use App\Models\Item;
use Doctrine\DBAL\Schema\Index;
use App\Jobs\changeIteamStatusFirebase;
use App\Models\Owner;
use Kreait\Firebase\Firestore as FirebaseFirestore;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// \Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('commingsoon');
})->name('frontend.home');

Route::get('/success', function () {
    return view('sucess');
});

Route::get('/tandc', function () {
    return view('tc');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/emailView', function () {
    return view('email.welcomeMail');
});

Route::get('/deleteAuth/{email}', function ($email) {
    return updateFirebaseUserDeletedAtDocument($email);
    return view('privacy');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/send',[TwilioSMSController::class,'index']);

Route::get('/viewMailtem',function(){
    $item=Item::find(3);
    dispatch(new changeIteamStatusFirebase(1,"hide"));
    //new changeIteamStatusFirebase();
    //AddFirebaseItemDocument($item);
    // updateFirebaseItemDocument($item);
    // firebaseBlock("YjCg1iCedUaLmaZkFxWepzPg3Xl2","1");

    return "update";
    // $firestore = new Firestore([
    //     'keyFile' => json_decode(file_get_contents(__DIR__.'/../FirebaseKey.json'), true)
    //     ]);
    //     $updateData =  $firestore->collection('users')->document($uid);

    $data = [
        'name' => 'Los Angeles',
        'state' => 'CA',
        'country' => 'USA'
    ];
        $firestore = new FirestoreClient([
		'keyFile' => json_decode(file_get_contents(__DIR__.'/../FirebaseKey-dev.json'), true)
    	]);
    	try {

			$firestore->collection('users')->document("user")->set($data);

    	} catch (Exception $e) {
    		return array("status" => 401, "firebaseError" => $e->getMessage());
    	}

        dd($firestore);
//$factory = (new Factory)->withServiceAccount(__DIR__.'/../FirebaseKey.json');
    $factory->createFirestore();
    $studen= $factory->database()->collection('students')->newDocument();
    $studen->set([
        "fname"=>"jasmin",
        "lname" => "shukla",
        "age" => 24
    ]);
    dd("data Added");
    $auth = $factory->createAuth();
    $firestore = new Firestore();
    $database = $firestore->database();
    $database->getReference('config/website')
    ->set([
        'name' => 'My Application',
        'emails' => [
            'support' => 'support@domain.tld',
            'sales' => 'sales@domain.tld',
        ],
        'website' => 'https://app.domain.tld',
       ]);
    try {
        $user = $auth->getUserByEmail("jasmin@gmail.com");
        dd($user);
    } catch(Exception $e) {
        return array("status" => 401, "firebaseError" => $e->getMessage());
    }
dd($factory);

    
});

Route::get('/updatefirebase',function(){
    updateFirebaseUserDocument();
    echo "update";
    die();
});



Route::group(['namespace' => 'App\Http\Controllers'], function()
    {
		//Auth::routes();
		/* Start Custom Auth URL's */
			// Authentication Routes..
			Route::post('logout', 'Auth\LoginController@logout')->name('logout');
			// Registration Routes...
			Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
			Route::post('register', 'Auth\RegisterController@register');
			// Password Reset Routes...
			Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
			Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
			Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
			Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
			// Email Verification Routes...
			Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
			Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
			Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
			// Admin Urls
			Route::get('admin/login', 'Adminauth\LoginController@showLoginForm')->name('adminLoginFrom');
			Route::post('admin/login', 'Adminauth\LoginController@adminLogin')->name('adminlogin');
			Route::post('admin/logout', 'Adminauth\LoginController@logout')->name('adminlogout');
			Route::get('/admin/password/reset','Adminauth\ForgotPasswordController@showForgotPasswordForm')->name('forgotpasswordform');
			Route::post('/admin/password/email','Adminauth\ForgotPasswordController@sendResetLinkEmail')->name('adminforgotpassword');
			Route::get('/admin/password/reset/{token}','Adminauth\ResetPasswordController@showResetPasswordForm')->name('showResetPasswordForm');
			Route::post('/admin/password/reset','Adminauth\ResetPasswordController@reset')->name('adminResetPassword');
			/* END Custom Auth URL's */

            
    });
    
	Route::group([ 'namespace' => 'App\Http\Controllers\Admin','middleware' => ['auth:admin']], function()
	{
		/*DASHBOARD CONTROLLER*/
		Route::get('/','DashboardController@index');
		Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::post('dashboard', 'DashboardController@filter')->name('filter');
		Route::resource('user-profile', 'ProfileController');
        Route::post('/changePassword/{id}','ProfileController@changePassword')->name('changePassword');
        /*NOTIFICATION MESSAGE CONTROLLER*/
        Route::get('notification-messages/getAllData', 'NotificationMessageController@getAllData');
        Route::resource('notification-messages', 'NotificationMessageController');
        Route::get('notification-messages/getAllData', 'NotificationMessageController@getAllData');
        Route::resource('notification-messages', 'NotificationMessageController');
        Route::resource('Notification', 'PushNotificationController');

        /*REGISTERED USER CONTROLLER*/
        Route::resource('users', 'UsersController');
        //Route::resource('users', 'UsersController');
        Route::get('exportUsers', 'UsersController@exportUser');
        Route::get('block-users', [BlockController::class,'index']);
        Route::get('reported-users', [ReportedUsersController::class,'index'])->name('reported-users.index');
        Route::get('referral-users', [ReferralController::class,'index'])->name('referral-users.index');
        Route::resource('raffle-users', 'RaffleController');
        //Route::get('raffle-users', [RaffleController::class,'index'])->name('raffle-users.index');
        Route::post('reported-users/{user}/suspend/{action}', [ReportedUsersController::class,'suspend'])->name('reported-users.suspend');
        Route::post('reported-users/{user}/block', [ReportedUsersController::class,'block'])->name('reported-users.block');
        Route::delete('reported-users/{user}', [ReportedUsersController::class,'destroy'])->name('reported-users.destroy');
        Route::get('reported-users/exportUsers', 'ReportedUsersController@exportUser');

        Route::resource('Feedback', 'FeedbackController');
        Route::resource('Notification', 'PushNotificationController');
        Route::get('ApiMessages',[ApiMessagesController::class,'index'])->name('ApiMessages');
        Route::get('ApiMessages/{key}',[ApiMessagesController::class,'show'])->name('ApiMessages.show');
        Route::put('ApiMessages/{key}',[ApiMessagesController::class,'update'])->name('ApiMessages.update');
        Route::get('Affiliate', 'AffiliationManagementController@index')->name('Affiliate.index');
        Route::post('Affiliate/New', 'AffiliationManagementController@store')->name('createAffiliate');
        Route::delete('Affiliate/{affiliate}/delete', 'AffiliationManagementController@destroy')->name('deleteAffiliate');
        Route::post('Affiliate/{affiliate}/update', 'AffiliationManagementController@update')->name('updateAffiliate');
        Route::get('Affiliate/{affiliate}', 'AffiliationManagementController@show')->name('showAffiliate');

        
        Route::post('faq/sortable', 'FaqController@sortable');
        Route::resource('faq-management', 'FaqController');
        Route::resource('job-application', 'JobApplicationController',['names'=>'job-application']);


        Route::get('user-feedbacks/getalldata', 'FeedbackController@getalldata');
        Route::resource('user-feedbacks', 'FeedbackController');

        // Route::resource('brands','BrandController');
        Route::get('brands/list/{status}','BrandController@index')->name('brand.List');
        Route::get('brands/change-status/{id}/{status}','BrandController@changestatus')->name('brand.changeStatus');

	});

    Route::group(['prefix' => 'admin-api'], function()
    {
        
        Route::get('faq-management/getAllFaq', [FaqController::class,'getAllFaq']);
        Route::post('faq/sortable', [FaqController::class,'sortable']);
        // Route::post('faq/sortable', 'FaqController@sortable');
        Route::get('users/getAllData', [UsersController::class,'getalldata']);
        Route::get('reported-users/getAllData', [ReportedUsersController::class,'getalldata']);
        Route::get('referral-users/getAllData', [ReferralController::class,'getalldata']);
        Route::get('raffle-users/getAllData', [RaffleController::class,'getalldata']);
        Route::get('job-application/getAllData', [JobApplicationController::class,'getalldata']);
        Route::get('block-users/getAllData', [BlockController::class,'getalldata']);
        Route::get('owner-user/getAllData', [OwnerUserController::class,'getalldata']);
        Route::get('feedback-list/getAllData', [FeedbackController::class,'getalldata']);
        Route::get('api-message-list/getAllData', [ApiMessagesController::class,'getalldata']);
        Route::get('brands/getAllData/{status}', [BrandController::class,'getalldata']);
        Route::get('brands/list/{status}', [BrandController::class,'getalldata']);
        Route::get('Affiliate/getAllData', [AffiliationManagementController::class,'getalldata']);
        Route::get('Notification/getAllData', [PushNotificationController::class,'getalldata'])->name('Ajax.Notification');
    });

