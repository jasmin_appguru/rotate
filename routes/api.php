<?php

use App\Http\Controllers\Api\AffiliationManagementController;
use App\Http\Controllers\Api\ActivityNotificationController;
use App\Http\Controllers\Api\ArchiveItemController;
use App\Http\Controllers\Api\BlockUserController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\FavoriteController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\ReportUserController;
use App\Http\Controllers\Api\FollowController;
use App\Http\Controllers\Api\ItemController;
use App\Http\Controllers\Api\RackController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WardrobesController;
use App\Http\Controllers\Api\SubscriptionController;
use App\Http\Middleware\IsSubscription;
use App\Http\Controllers\FaqController;
use App\Models\ArchiveItem;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function()
{
    Route::post('ForgotPassword',[UserController::class,'ForgotPassword']);
    
    Route::post('signUp',[UserController::class,'register']);
    
    Route::post('login',[UserController::class,'login'])->name('api.login')->middleware('validate.active');

    Route::post('validateReferralCode',[UserController::class,'validateReferralCode']);
        
    Route::post('verifyEmailOTP',[UserController::class,'requestOtp']);

    Route::post('Logout',[UserController::class,'Logout']);

    Route::middleware(['auth:api','validate.active'])->group( function () {

        Route::post('checkReceipt',[SubscriptionController::class,'GetSubscription']);
        Route::post('setMemberShip',[SubscriptionController::class,'setMemberShip']);
        
        Route::post('setUpProfile',[UserController::class,'setUpProfile']);
        
        Route::middleware([IsSubscription::class])->group(function () {

            Route::post('editProfile',[UserController::class,'editProfile']);

            Route::post('registerReport',[ReportUserController::class,'RegisterReport']);

            Route::post('blockUser',[BlockUserController::class,'blockUser']);
            
            Route::post('blockList',[BlockUserController::class,'index']);
            
            Route::post('getAffiliations',[AffiliationManagementController::class,'index']);
            
            Route::post('CheckUserName',[UserController::class,'CheckUserName']);
            
            Route::post('changeEmail',[UserController::class,'changeEmail']);

            Route::post('changePassword',[UserController::class,'changePassword']);
            
            Route::post('changeAccountType',[UserController::class,'changeAccountType']);

            Route::post('getProfile',[UserController::class,'getProfile']);

            Route::post('searchUser',[UserController::class,'searchUser']);
            
            Route::post('InviteFriendList',[UserController::class,'InviteFriendList']);

            Route::post('getBrand',[BrandController::class,'index']);
            
            Route::post('addBrand',[BrandController::class,'store']);
            
            Route::post('getCategory',[CategoryController::class,'index']);
            
            Route::post('getSubCategory',[CategoryController::class,'SubCategory']);
            
            Route::post('addNewRack',[RackController::class,'add']);

            Route::post('followUnfollow',[FollowController::class,'followUnfollow']);
            
            Route::post('getFollowers',[FollowController::class,'getFollowers']);

            Route::post('getFollowing',[FollowController::class,'getFollowing']);

            Route::post('faq',[FaqController::class,'index']);

            Route::post('SendFeedback',[FeedbackController::class,'SendFeedback']);

            Route::post('addItem',[ItemController::class,'store']);
            
            Route::post('ItemList',[ItemController::class,'index']);
            
            Route::post('getDetail',[ItemController::class,'detail']);

            Route::post('deleteItem',[ItemController::class,'deleteItem']);

            Route::post('searchItem',[ItemController::class,'searchItem']);
            
            Route::post('favouriteList',[FavoriteController::class,'index']);
            
            Route::post('favouriteUnFavouriteItem',[FavoriteController::class,'store']);
            
            Route::post('enableUserNotification',[ActivityNotificationController::class,'store']);
            
            Route::post('getColor',[SettingController::class,'getColor']);
            
            Route::post('archiveList',[ArchiveItemController::class,'index']);
            
            Route::post('archiveUnArchive',[ArchiveItemController::class,'store']);

            Route::post('getNotifications',[NotificationController::class,'index']);
            
            Route::post('NotificationCount',[NotificationController::class,'Count']);
        
            Route::post('getWardrobe',[WardrobesController::class,'index']);

            Route::post('getWardrobeItem',[WardrobesController::class,'getWardrobeItem']);
            
            Route::post('getWardrobeSubCategories',[WardrobesController::class,'GetItemFromCat']);

            Route::post('getWardrobeRackList',[WardrobesController::class,'getWardrobeRackList']);

            Route::post('getWardrobeRackItem',[WardrobesController::class,'getWardrobeRackItem']);

            Route::post('reOrderItem',[WardrobesController::class,'reOrderItem']);

            Route::post('moveItem',[WardrobesController::class,'moveItem']);
            
            Route::post('changeWardrobeType',[UserController::class,'changeWardrobeType']);
            
            Route::post('changeWardrobeItemType',[ItemController::class,'changeItemType']);

            Route::post('getSizeList',[ItemController::class,'getSizeList']);

            Route::post('filterItem',[ItemController::class,'filterItem']);

            Route::post('getAllSubCategory',[CategoryController::class,'GetGlobalCategoryList']);

        });

        Route::post('DeleteAccount',[UserController::class,'DeleteAccount']);


        
    });
});

Route::middleware('auth:api')->group( function () {
    /**
     * List of Basic Authentication
     * Our Base table is user Laravel authentication
     */
    
    Route::any('welcome',[UserController::class,'WelcomEmail']);
    
    ////////////// removed
    // Route::post('completeProfile',[UserController::class,'completeProfile']);
    // Route::post('editProfile',[UserController::class,'editProfile']);
    /**
     * List of Relief Driver Api list
     * When user type = 1
     */
    // Validated Request is Driver or not


    /**
     * @author Jasmin Shukla
     * Application Request accept/decline
     * 0 = decline
     * 1 = Accept
     */
    Route::post('LoginUserDetail',[UserController::class,'loginUser']);
});
Route::get('/',[UserController::class,'unauthorize']);
