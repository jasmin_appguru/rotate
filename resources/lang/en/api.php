<?php

return array (
  'change_password' => 'Password has been updated successfully.',
  'setUpProfile' => 'Profile Setup successfully.',
  'editProfile' => 'Profile updated successfully.',
  'editProfile_last30username' => 'You can not change your UserName again within 30 days.',
  'userName_unique_available' => 'This username is available.',
  'userName_unique_unavailable' => 'This username is unavailable. Try another.',
  'edit_profile' => 'Profile edit successfully.',
  'error_otp_api' => 'Otp Communication APIs not configure.',
  'faq_index' => 'FAQ list fetched successfully.',
  'changeAccountType' => 'Your account privacy has been changed.',
  'email_verification' => 'Email Verification Code Send Successfully.',
  'get_booking_request_success' => 'booking list fetch successfully.',
  'invalid' => 'Email or Password is incorrect.',
  'invalid_user_name' => 'Username or Password is incorrect.',
  'invalid_otp' => 'Invalid Verification Code',
  'login_success' => 'You are successfully logged in.',
  'otp_massage' => ':OTP is your OTP / verification code. for Relief Driver :purpose.',
  'push_notification_title' => 'Rotate',
  'register_email_unique' => 'You are already registered with this email address. Please login to proceed.',
  'request_operation_success' => 'Your booking for :COMPANY has been :OPERATION .',
  'sendBookingNotification' => ':DRIVER has sent you a booking request for :DATES .',
  'send_booking_driver_warring' => ':Driver has not accepted your last :count requests.',
  'signup' => 'you have signup successfully.',
  'Logout_success' => 'You have been logged out successfully.',
  'verify_email_success' => 'OTP has been sent on this email. Please check the inbox.',
  'Brand_success' => 'Brand fetched successfully.',
  'brand_add_success' => 'Brand added successfully.',
  'category_success' => 'Category fetched successfully.',
  'sub_category_success' => 'SubCategory fetched successfully.',
  'change_email_success' => 'Email has been updated successfully.',
  'follow_success' => ':OPERATION user successfully.',
  'reported_success' => 'You have reported :USERNAME.',
  'block_success' => 'You have :ACTION :USERNAME.',
  'feedback_success' => 'Thank you for your feedback. We will investigate and take the necessary steps.',
  'Item_success' => 'Item added successfully.',
  'block_list_success' => 'Block list fetch successfully.',
  'archive_fetch_success' => 'Archive fetched successfully.',
  'affiliation_fetch_success' => 'Affiliations fetched successfully.',
  'followers_fetch_success' => 'Followers list fetched successfully.',
  'following_fetch_success' => 'Following list fetched successfully.',
  'item_fetch_success' => 'Item fetched successfully.',
  'notification_fetch_success' => 'Notification fetched successfully.',
  'color_fetch_success' => 'Color fetched successfully.',
  'invite_friend_fetch_success' => 'Invite friend list fetched successfully.',
  'notification_count_fetch_success' => 'Notification Count fetched successfully.',
  'item_details_fetch_success' => 'Item details fetched successfully.',
  'wardrobe_fetch_success' => 'Your wardrobe fetched successfully.',
  'wardrobe_item_fetch_success' => 'Your wardrobe item fetched successfully.',
  'subcategory_fetch_success' => 'SubCategory fetched successfully.',
  'item_deleted_success' => 'Item deleted successfully.',
  'archive_unarchive_success' => 'You have :STATUS successfully.',
  'fav_unfav_success' => ':STATUS successfully.',
  'notification_on_off' => 'Notification for :USERNAME is :STATUS successfully.',
  'new_data_add' => 'Added New data.',
  'new_notification' => 'new Notification created.',
  'profile_update_success' => 'Profile updated successfully.',
  'update_data' => 'Update Data',
  'wardrobe_item_privacy_success' => 'Wardrobe item privacy updated successfully.',
  'wardrobe_privacy_success' => 'Wardrobe privacy updated successfully.',
  'rack_added_success' => 'Rack added successfully.',
  'invalid_user_id' => 'Invalid User id.',
  'invalid_url' => 'Invalid Url.',
  'user_search_success' => 'User search successfully.',
  'rack_reorder_success' => 'All rack item reorder successfully.',
  'add_brand_name_unique' => 'This brand has already been there in our list. Please check and select the brand.',
  'birthday_wish' => 'Team Roate wishes you a Happy Birthday :USERNAME.',
  'referral_message' => ':USERNAME has joined via your referral code.',
  'referral_reminder_message' => 'You haven\'t referred any friend in last month. Refer your friend to get advantage of free membership.',
  'post_item_reminder_message' => 'You haven\'t added a new item to your wardrobe since 30 days/a month. Click here to post new item.',
  'post_first_item_reminder_message' => 'You haven\'t added an item to your wardrobe. Click here to post your first item.',
  'favorite_item_on_sale' => 'Your favourited :CATNAME is listed for sale.',
  'followed_user_new_item' => ':USERNAME uploaded a new item.',
  'followed_msg_notification' => ':USERNAME started following you.',
  'like_summary' => 'Last week :CONT people favourited your items.',
  'delete_account' =>"Your account has been deleted.",
);