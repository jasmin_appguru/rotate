<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Welcome Email</title>

    <style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,700;1,400&display=swap');

            @font-face {
                font-family: "CustomFont";
                src: url('{{ asset('font/portae-light.otf') }}') format("opentype");
            }



        html,body{width:100%;font-family:'Roboto','helvetica neue',arial,verdana,sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; margin: 0px; padding: 0px;}
        ul {
  margin: 0;
}
ul.dashed {
  list-style-type: none;
}
ul.dashed > li {
  text-indent: -5px;
}
ul.dashed > li:before {
  content: "-";
  text-indent: -5px;
}
    </style>
</head>
<body style="background-color: #FAFBFB;">
    <div class="es-wrapper-color">
        <table class="es-wrapper" style="background-color: #FAFBFB; background-position: center top;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td class="esd-email-paddings" valign="top">
                        <table cellpadding="0" cellspacing="0" class="es-content esd-header-popover" align="center" style="padding: 30px 0px;">
                            <tbody>
                                <tr>
                                    <td class="es-adaptive esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p10" align="center" valign="center">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-structure" align="left">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-container-frame" width="100%" valign="top" align="center">
                                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="esd-block-image es-p20b" align="center" style="font-size:0;">
                                                                                                        <a href="#" target="_blank">
                                                                                                            <img src="{{ asset('images/mail_logo.png') }}" alt="" style="display: block;/* position: absolute; */" width="200">
                                                                                                        </a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: #fff; padding: 15px; border: 1px solid #757575;" width="600" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="600" valign="top" align="center">
                                                                        <table style="padding: 20px 0px;" width="100%" cellspacing="0" cellpadding="0" >
                                                                            <tbody>
                                                                                {{-- <tr>
                                                                                    <td class="esd-block-text  " align="left">
                                                                                        <img src="{{ asset('images/welcome.png') }}" alt="">
                                                                                    </td>
                                                                                </tr> --}}
                                                                                <tr>
                                                                                    <td class="esd-block-text  " align="left">
                                                                                        <h3 style="color: #030303; font-size: 35px; margin: 0px 0px 15px; font-weight: 500; ">
                                                                                            <span style="font-family:'CustomFont','Roboto'; ">
                                                                                                Welcome, {{$name ?? "PORTAE"}}    
                                                                                            </span> !
                                                                                            </h3>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text " align="left">
                                                                                        
                                                                                        <p style="font-size: 35px;  font-weight: 300;">We're so thrilled to have you join Portae! The first app of it's kind that allows you to curate your wardrobe and intuitively sell your items.</p>

                                                                                        <p style="font-size: 35px;  font-weight: 300;">You can begin using the app by creating a profile and uploading your clothing, shoes and accessories. Remember the more descriptive your items are, the more searchable they will be.</p>

                                                                                        <p style="font-size: 35px;  font-weight: 300;">After you have created your profile, don't forget to share it with your friends to keep up to date with the latest styles and be the first to know when they list items for sale.</p>
                                                                                        
                                                                                        <p style="font-size: 35px;  font-weight: 300;">For simple tips and tricks you can refer to the 'FAQs' and 'Tips' sections of the App (accessed via Settings).</p>
                                                                                        
                                                                                        <p style="font-size: 35px;  font-weight: 300;">Don't keep your wardrobe behind closed doors! Happy Portae-ing!</p>
    
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                {{-- <tr>
                                                    <td class="esd-structure " style="background-color: #fcfcfc;"  align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                        <table style="border-color: #A0A0A0; border-style: solid; border-width: 1px; padding: 20px; border-radius: 10px; border-collapse: separate; background-color: #FAFBFB;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text" align="center">
                                                                                        <h2 style="color: #030303; font-size: 24px; margin: 15px 0px 20px; font-weight: bold;">Account Credentials</h2>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text" align="left">
                                                                                        <p style="color: #030303; font-size: 35px; line-height: 150%;font-weight: 300;"><span style="display: inline-block; font-weight: bold;">Email:</span> username@gmail.com</p>
                                                                                        <p style="color: #030303; font-size: 35px; line-height: 150%;font-weight: 300;"><span style="display: inline-block; font-weight: bold;">Password:</span> APP@12345</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-button" align="left">
                                                                                        <span class="es-button-border" style="display: inline-block; padding: 15px 0px;"><a href="#" class="es-button" target="_blank" style="border-radius: 6px; border: 1px solid #000; padding: 9px 20px; display: inline-block; color: #000; text-decoration: none; font-weight: 500; box-sizing: border-box;">Let's Start</a></span>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr> --}}
                                                <tr>
                                                    <td class="esd-block-social-links  " align="center" style="border-top: 1px solid #DBDBDB; padding-top:20px; padding-bottom: 5px;">
                                                        <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" style="" >
                                                            <tbody>
                                                                <tr>
                                                                    {{-- <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href="#"><img title="figma" src="{{ asset('images/figma.png') }}" alt="Fg" width="30"></a>
                                                                    </td> --}}
                                                                  
                                                                    <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href="https://www.instagram.com/portae_au/"><img title="Instagram" src="{{ asset('images/Instagram.png') }}" alt="Inst" width="30"></a>
                                                                    </td>
                                                                    <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href=""><img title="Facebook" src="{{ asset('images/Facebook.png') }}" alt="Fb" width="30"></a>
                                                                    </td>
                                                                    <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href=""><img title="twitter" src="{{ asset('images/Tiktok.png') }}" alt="tw" width="30"></a>
                                                                    </td>
                                                                    {{-- <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href="#"><img title="linkedin" src="{{ asset('images/linkedin.png') }}" alt="in" width="30"></a>
                                                                    </td>
                                                                    <td class="es-icon" valign="top" align="center" style="padding:5px;">
                                                                        <a target="_blank" style="display: inline-block;" href="#"><img title="dribbble" src="{{ asset('images/dribbble.png') }}" alt="drbl" width="30"></a>
                                                                    </td> --}}

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: #fcfcfc;" width="600" cellspacing="0" cellpadding="0"  align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p40t  " esd-custom-block-id="15790" align="left">

                                                        <table class="es-left" width="100%" cellspacing="0" cellpadding="0" align="center" style="padding: 30px 0px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <p style="color: #000; font-size: 14px; margin:0px; font-weight: normal;text-align: center;">© {{ date('Y') }} Portae</p>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>