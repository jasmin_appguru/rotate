<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;text-align:center;'><strong><span style='font-size:13px;font-family:"Arial",sans-serif;'>Online Membership Terms and Conditions<br>&nbsp;</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;'><span style='font-size:13px;font-family:"Arial",sans-serif;'>These terms and conditions (<strong>Terms</strong>) are the contract between you as the online membership member (<strong>you</strong> or <strong>your</strong>) and Rotate Australia Pty Ltd of 4606/15 Anderson Street, Kangaroo Point, Queensland (<strong>us</strong>, <strong>our&nbsp;</strong>or&nbsp;<strong>we</strong>).&nbsp;<br>&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;'><span style='font-size:13px;font-family:"Arial",sans-serif;'>By visiting or using the Membership platform at www.portae.com.au&nbsp;</span><span style='font-size:13px;font-family:"Arial",sans-serif;'>(</span><strong><span style='font-size:13px;font-family:"Arial",sans-serif;'>Site</span></strong><span style='font-size:13px;font-family:"Arial",sans-serif;'>), you agree to be bound by the Terms. Please read this agreement carefully and save it. If you do not agree to be bound by the Terms, you should leave the Site immediately.</span></p>
<ol class="decimal_type" style="list-style-type: decimal;margin-left:26px;">
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Definitions</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: decimal;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">In this agreement, the following words have the following meanings:</span>
                <ol class="decimal_type" style="list-style-type: decimal;">
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Content&nbsp;</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">means the written, video or sound content that is available for you on the Site and it may include content posted by your other members of the Site.</span></li>
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Governing Law</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">&nbsp;means the law of Queensland;</span></li>
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Membership</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">&nbsp;means your membership of the Site and these Terms. It includes the membership service we provide as set out on our Site and in this contract.</span></li>
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Post&nbsp;</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">means Content and any other material on our Site and includes the phrases <strong>Posted</strong> and <strong>Posting</strong> in these Terms.</span></li>
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Services&nbsp;</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">means all of the services or benefits available with your Membership on the Site, whether free or paid.</span></li>
                    <li><strong><span style="line-height:115%;font-family:Arial;font-size:13px;">Site</span></strong><span style="line-height:115%;font-family:Arial;font-size:13px;">&nbsp;means the Membership platform at&nbsp;</span><a href="http://www.portae.com.au"><span style="line-height:115%;font-family:Arial;font-size:13px;">www.portae.com.au</span></a><span style="line-height:115%;font-family:Arial;font-size:13px;">&nbsp;</span><span style="line-height:115%;font-size:13px;">and any other app or service designed for access by mobile phones or fixed devices and includes all web pages controlled by us. For clarity, the Site is currently hosted by Synergy Wholesale Accreditations Pty Ltd (<strong>Membership Site</strong>) as at the date of these Terms. In agreeing to these Terms you also agree to be bound by any Terms required by the Membership Site or any other third party hosting website we use from time to time.</span></li>
                </ol>
            </li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Our contract</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">These terms and conditions regulate our business relationship with you. By electing to join our Membership Services or use our Site free of charge, you agree to be bound by them. <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">The contract between us comes into existence when we receive payment from you for your Membership.</span></span></span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">In entering into this contract you acknowledge and agree that you have not relied on any representation or information from any source except the definition and explanation of the Services given on our Site.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Subject to these Terms, we agree to provide to you some or all of the Services described on our Site at the prices we charge from time to time.&nbsp;</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If we give you free access to a Service or feature on our Site which is normally a paid Membership only feature, and that Service or feature is usually subject to additional contractual terms, you agree that you will abide by those additional terms in order to gain access to that feature.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Your account and personal information</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">When you visit our Site, you accept responsibility for any action done by any person in your name or under your account or password. You should take all necessary steps to ensure that your password is kept confidential and secure.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree to inform us immediately if you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorised manner.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree that you have provided accurate, up to date, and complete information about yourself to us. We are not responsible for any error made as a result of such information being inaccurate.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree to notify us of any changes in your information, such as updated credit card details or other critical personal information immediately once a change occurs. If you do not do so, we may terminate your account at our discretion.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><span style='font-family:"Arial",sans-serif;color:blue;'><strong><span style="line-height:115%;color:black;font-size:10.0pt;color:black;">Membership</span></strong></span></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Details of the cost and benefits of <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:;font-size:10.0pt;line-height:;;">Membership</span></span> are as set out on our Site. You may subscribe to Membership Services at any time on the basis we offer it to you at the time you elect to subscribe.&nbsp;</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You do not have to take any action for these Terms to apply other than electing to be bound by the Membership. By accepting these Terms, you instruct us to give you immediate access to the <strong>Membership Services&nbsp;</strong>and you know that by doing so, you may not be entitled to a refund of any Membership fees paid to us.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Termination of Membership will be regulated by this contract set out in paragraph 14 below.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You may not transfer your Membership to any other person.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We reserve the right to modify the <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:10.0pt;color:black;">Membership</span></span> rules or system and to change the Terms of this contract at any time, without notice. If, after such modifications, you continue to use your <span style="color:black;">Membership, we will deem this as your acceptance of the modified Terms</span>.&nbsp;</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Prices</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">The price payable for the Membership is set out on our Site. We reserve the right to update that Membership price from time to time at our discretion.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">The price charged for Membership will be in Australian Dollars, if you are located in another country, you may be charged international conversion rates and be subject to an exchange rate. You will be required to pay any additional fees in this regard.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Prices are inclusive of any goods and services tax or other sales tax (where it is applicable).</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You will pay all sums due to us under these Terms and your Membership by the means specified without any set-off, deduction or counterclaim.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Renewal payments</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="font-family:Arial;font-size:13px;">Your Membership and licence to use the Membership Services will renew automatically each month. At the monthly renewal we will automatically take payment from your credit card or other specified method of payment of the sum specified on our Site as the then monthly Membership fee.&nbsp;</span></li>
            <li><span style="font-family:Arial;font-size:13px;">Membership is ongoing until such time as you actively cancel your Membership in accordance with these Terms.&nbsp;</span></li>
            <li><span style="font-family:Arial;font-size:13px;">You can cancel your Membership at any time by emailing a cancellation notice to our nominated email address at the foot of these Terms or by managing your payment in our Site under the settings &ldquo;Manage Payment&rdquo;.&nbsp;</span></li>
            <li><span style="font-family:Arial;font-size:13px;">Cancellation will take effect the following month after notice is received. &nbsp;&nbsp;</span></li>
            <li><span style="font-family:Arial;font-size:13px;">Other than the limitations set out above Membership is non-refundable and non-transferable.&nbsp;</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Security of your credit card</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Please note that credit card payments are not processed on a page controlled by us.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Processing takes place on the third party payment processor connected to our Membership Site, and we are bound by their terms and conditions and any other relevant third payment processor that the Membership Site uses to take your payment.&nbsp;</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If you have concerns about the safety or otherwise of your card, the Membership Site and other third payment processor terms should be read before you agree to the monthly membership direct debit from your card to ensure the details are being kept safely. While we will use our reasonable commercial endeavours to ensure the safety of any details we hold, we cannot directly control the details held by third party sites and will not be liable in this regard.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Restrictions on what you may Post to Our Website</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We may, at our discretion, read, assess, review or moderate any Content Posted on our Site. If we do, we do not need to notify you or give you a reason.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree that you will not use or allow anyone else to use our Site to Post Content which is or may:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">be malicious or defamatory;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">consist in commercial audio, video or music files;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">be obscene, offensive, threatening or violent;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">be sexually explicit or pornographic;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">be likely to deceive any person or be used to impersonate any person, or to misrepresent your identity, age or affiliation with any person;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">give the impression that it emanates from us or that you are connected with us or that we have endorsed you or your business;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">solicit passwords or personal information from anyone;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">be used to sell any goods or services or for any other commercial use;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">include anything other than words (i.e. you will not include any symbols or photographs) except for a photograph of yourself in your profile in such place as we designate;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">link to any of the material specified above, in this paragraph.</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Post excessive or repeated off-topic messages to any forum or group;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">sending age-inappropriate communications or Content to anyone under the age of <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">18</span></span>.</span></li>
                </ol>
            </li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Restricted Content</span></strong></h1>
    </li>
</ol>
<p style='margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">In connection with the restrictions set out below, we may refuse or edit or remove a Post that does not comply with these terms.</span></p>
<p style='margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">In addition to the restrictions set out above, a Posting must not contain:</span></p>
<ol class="decimal_type" style="list-style-type: undefined;">
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">hyperlinks, other than those specifically authorised by us;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">keywords or words repeated, which are irrelevant to the Content Posted.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">the name, logo or trademark of any organisation other than that of you or your client.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">inaccurate, false, or misleading information.</span></li>
</ol>
<ol class="decimal_type" style="list-style-type: undefined;margin-left:26px;">
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>How we handle your Content</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Our privacy policy is at <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">www.portae.com.au</span></span></span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If you Post Content to any public area of our Site it becomes available in the public domain. We have no control over who sees it or what anyone does with it.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Even if access to your text is behind a user registration it remains effectively in the public domain because someone has only to register and log in, to access it. You should therefore avoid Posting unnecessary confidential information.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We require the freedom to be able to publicise our Services and your use of them. Accordingly, you irrevocably grant us the right and licence to edit, copy, publish, distribute, translate and otherwise use any Content that you place on our Site, in public domains and in any medium. You represent and warrant that you are authorised to grant all such rights.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We will use that licence only for commercial purposes of the business of our Site and will stop using it after a commercially reasonable period of time.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree to any act or omission which may otherwise infringe your right to be identified as the author and your right to object to derogatory treatment of your work as provided in the <em>Copyright Act 1968</em>.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Posting content of any sort does not change your ownership of the copyright in it. We have no claim over it and we will not protect your rights for you.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You understand that you are personally responsible for your breach of someone else&rsquo;s intellectual property rights, defamation, or any law, which may occur as a result of any Content having been Posted by you.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You accept all risk and responsibility for determining whether any Content is in the public domain and not confidential.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Please notify us immediately of any security breach or unauthorised use of your account.</span></li>
            <li><span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">We do not solicit ideas or text for improvement of our Membership Service, but if you do send to us material of any sort, you are deemed to have granted to us a licence to use it in the terms set out at sub paragraph 10.4 above</span></span><span style="line-height:115%;font-family:Arial;font-size:13px;">.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Removal of offensive Content</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">For the avoidance of doubt, this paragraph is addressed to any person who comes on our Site for any purpose.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We are under no obligation to monitor or record the activity of any customer for any purpose, nor do we assume any responsibility to monitor or police Internet-related activities. However, we may do so without notice to you and without giving you a reason.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If you are offended by any Content, the following procedure applies:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">your claim or complaint must be submitted to us in the form available on our Site, or contain the same information as that requested in our form. It must be sent to us by post or email;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">we shall remove the offending Content as soon as we are reasonably able;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">after we receive notice of a claim or complaint, we shall investigate so far as we alone decide;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">we may re-instate the Content about which you have complained or not.</span></li>
                </ol>
            </li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">In respect of any complaint made by you or any person on your behalf, whether using our form of complaint or not, you now irrevocably grant to us a licence to publish the complaint and all ensuing correspondence and communication, without limit.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If any complaint made by you is either frivolous or vexatious, you agree that you will repay us the cost of our investigation including legal fees, if any.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Security of Our Website</span></strong></h1>
    </li>
</ol>
<p style='margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">You agree that you will not, and will not allow any other person to:</span></p>
<ol class="decimal_type" style="list-style-type: undefined;">
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">modify, copy, or cause damage or unintended effect to any portion of our Site, or any software used within it.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">link to our Site in any way that would cause the appearance or presentation of our Site to be different from what would be seen by a user who accessed our Site by typing the URL into a standard browser;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">download any part of our Site, without our express written consent;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">collect or use any product listings, descriptions, or prices;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">collect or use any information obtained from or about our Site or the Content except as intended by this agreement;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">aggregate, copy or duplicate in any manner any of the Content or information available from our Site, other than as permitted by this agreement or as is reasonably necessary for your use of the Services;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">share with a third party any login credentials to our Site.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Notwithstanding the preceding clauses, we grant a licence to you to:</span>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">create a hyperlink to our Site for the purpose of promoting an interest common to both of us. You can do this without specific permission. This licence is conditional upon your not portraying us or any product or service in a false, misleading, derogatory, or otherwise offensive manner. You may not use any logo or other proprietary graphic or trademark of ours as part of the link without our express written consent.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">you may copy the text of any page for your personal use in connection with the purpose of our Site a Service we provide.</span></li>
        </ol>
    </li>
</ol>
<ol class="decimal_type" style="list-style-type: undefined;margin-left:26px;">
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Disclaimers</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">The law differs from one country to another. This paragraph applies so far as the applicable law allows.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">All implied conditions, warranties and terms are excluded from this agreement. If in any jurisdiction an implied condition, warrant or term cannot be excluded, then this sub paragraph shall be deemed to be reduced in effect, only to the extent necessary to release that specific condition, warranty or term.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">The Site and our Membership and Services are provided &ldquo;as is&rdquo;. We make no representation or warranty that the Site, Membership or Services will be:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">useful to you;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">of satisfactory quality;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">fit for a particular purpose;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">available or accessible, without interruption, or without error;</span></li>
                </ol>
            </li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Your use of the Membership Services or the Site, or the exercise of any right granted under this agreement will infringe any other intellectual property or other rights of any other person.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Our Site may contain links to other third party Internet websites (<strong>Third Party Sites</strong>). We do not have power or control over any Third Party Sites and you acknowledge and agree that we shall not be liable in any way for the Content of any such linked website, nor for any loss or damage arising from your use of any such website.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We are not liable in any circumstances for special, indirect or consequential damages or any damages whatsoever resulting from loss of use, loss of data or loss of revenues or profits, whether in an action of contract, negligence or otherwise, arising out of or in connection with your use of our Site.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We claim no expert knowledge in any subject. We disclaim any obligation or liability to you arising directly or indirectly from information you take from our Site or receive directly from a third party as a result of an introduction via our Site. You must make your own enquiries regarding your individual circumstances before applying any information or otherwise obtained from our Site and we disclaim all responsibility in this regard.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">From time to time we may arrange Membership events on third party platforms like Facebook, Zoom or other platforms. In attending these events you will be responsible for your internet security and passwords and we shall not be liable for any loss or damage arising from use of such platforms to attend Membership Events.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Duration and termination</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">This agreement shall operate for the period for which you have subscribed to <span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">Membership</span></span> Service and will be on-going until such time as you terminate your Membership.&nbsp;</span></li>
            <li><span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">You may terminate this agreement at any time, for any reason, with immediate effect by&nbsp;</span></span><span style="line-height:115%;font-family:Arial;font-size:13px;">emailing a cancellation notice to our nominated email address at the foot of these Terms or by managing your payment in our Site under the settings &ldquo;Manage Payment&rdquo;<span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">. Upon such cancellation, the Membership will terminate from the next monthly membership payment due. We reserve the right to check the validity of any request to terminate membership before acting on it.&nbsp;</span></span></span></li>
            <li><span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">We may terminate this agreement at any time, for any reason, with immediate effect by sending you notice to that effect by email</span></span><span style="line-height:115%;font-family:Arial;font-size:13px;">, noting that the membership will terminate immediately and the next monthly membership payment will be cancelled by us.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Termination by either party shall have the following effects:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">your right to use the Services immediately ceases;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">we are under no obligation to forward any unread or unsent messages to you or any third party.</span></li>
                </ol>
            </li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">There shall be no re-imbursement or credit if the Service is terminated due to your breach of the terms of this agreement.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We retain the right, at our sole discretion, to terminate any and all parts of the Services provided to you, without refunding to you any fees paid if we decide in our absolute discretion that you have failed to comply with any of the terms of this agreement.&nbsp;</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Storage of data</span></strong><strong><span style="line-height:115%;font-size:13px;">&nbsp;</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We assume no responsibility for the deletion or failure to store or deliver email or other messages.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">We may, from time to time, set a limit on the number of messages you may send, store, or receive through the Service. We may delete messages in excess of that limit. We shall give you notice of any change to your limit, except in an emergency.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You accept that we cannot be liable to you for any such deletion or failure to deliver to you.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:black;'>Intellectual property rights</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>Unless otherwise indicated, we own or licence all rights, title and interest (including intellectual property) in the Content on the Site and delivered via our Services. Your use of the Site and our Services to participate in the Membership we offer and your use of or access to any Content does not grant or transfer to you any rights, title or interest in relation to our Site or the Content.</span></li>
        </ol>
    </li>
</ol>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:54.0pt;font-size:16px;font-family:"Cambria",serif;'>&nbsp;</p>
<div style='margin:0cm;font-size:16px;font-family:"Cambria",serif;'>
    <ol style="margin-bottom:0cm;list-style-type: undefined;">
        <li style='margin:0cm;font-size:16px;font-family:"Cambria",serif;'><span style='font-family:"Arial",sans-serif;font-size:13px;'>You must not, without our prior written consent or the consent of the owner of the Content (where we do not own the Content) as applicable:</span></li>
    </ol>
</div>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;font-size:16px;font-family:"Cambria",serif;'><span style='font-size:13px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
<ol class="decimal_type" style="list-style-type: undefined;">
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>copy or use, in whole or in part, any Content;</span></li>
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate any Content to any third party; or</span></li>
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>breach any intellectual property rights connected with our Site, including (without limitation) by:</span></li>
</ol>
<ol style="list-style-type: undefined;margin-left:98px;">
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>altering or modifying any of the Content;</span></li>
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>causing any of the Content to be framed or embedded in another website or platform; or</span></li>
    <li><span style='font-family:"Arial",sans-serif;font-size:13px;'>creating derivative works from the Content.</span></li>
</ol>
<ol class="decimal_type" style="list-style-type: undefined;margin-left:26px;">
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Interruption to Services</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If it is necessary for us to interrupt the Services, we will give you reasonable notice where this is possible and when we think the down time is such as to justify telling you.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You acknowledge that the Services may also be interrupted for many reasons beyond our control.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">You agree that we are not liable to you for any loss, foreseeable or not, arising from any interruption to the Services.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Limitation of liability</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">Our total liability to you, for any one event or series of related events, and whether in contract, tort, negligence, breach of statutory duty or otherwise, shall be limited to the amount of your monthly Membership fee.</span></span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Neither party shall be liable to the other in any possible way, for any loss or expense which is:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">indirect or consequential loss; or</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">economic loss or other loss of turnover, profits, business or goodwill.</span></li>
                </ol>
            </li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">This paragraph (and any other paragraph which excludes or restricts our liability) applies to our directors, officers, employees, subcontractors, agents and affiliated companies as well as to us.</span></li>
        </ol>
    </li>
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Indemnity</span></strong></h1>
    </li>
</ol>
<p style='margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">You agree to indemnify us against any loss, damage or liability, suffered by us at any time and arising out of:</span></p>
<ol class="decimal_type" style="list-style-type: undefined;">
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">any act, neglect or default of yours in connection with this agreement or your use of the Services;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">your breach of this agreement;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">your failure to comply with any law;</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">a contractual claim arising from your use of the Services.</span></li>
</ol>
<ol class="decimal_type" style="list-style-type: undefined;margin-left:26px;">
    <li>
        <h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:0cm;font-size:21px;font-family:"Calibri",sans-serif;color:#365F91;font-weight:normal;line-height:115%;'><strong><span style='line-height:115%;font-family:"Arial",sans-serif;font-family:"Arial",sans-serif;font-size:10.0pt;color:windowtext;'>Miscellaneous matters</span></strong></h1>
        <ol class="decimal_type" style="list-style-type: undefined;">
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If any term or provision of this agreement is at any time held by any jurisdiction to be void, invalid or unenforceable, then it shall be treated as changed or reduced, only to the extent minimally necessary to bring it within the laws of that jurisdiction and to prevent it from being void and it shall be binding in that changed or reduced form. Subject to that, each provision shall be interpreted as severable and shall not in any way affect any other of these terms.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">The rights and obligations of the parties set out in this agreement shall pass to any permitted successor in title.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">If you are in breach of any term of this agreement, we may:</span>
                <ol class="decimal_type" style="list-style-type: undefined;">
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">publish all text and Content relating to the claimed breach, including your name and email address and all correspondence between us and our respective advisers; and you now irrevocably give your consent to such publication;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">terminate your account and refuse access to our Site;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">remove or edit Content, or cancel any order at our discretion;</span></li>
                    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">issue a claim in any court.</span></li>
                </ol>
            </li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Any obligation in this agreement intended to continue to have effect after termination or completion shall so continue.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">No failure or delay by any party to exercise any right, power or remedy will operate as a waiver of it nor indicate any intention to reduce that or any other right in the future.</span></li>
            <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Any communication to be served on either of the parties by the other shall be delivered by hand or sent by express post or recorded delivery or by e-mail.</span></li>
        </ol>
    </li>
</ol>
<table style="border: none;margin-left:72.0pt;border-collapse:collapse;">
    <tbody>
        <tr>
            <td style="width: 390.15pt;padding: 0cm 5.4pt;vertical-align: top;">
                <p style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">It shall be deemed to have been delivered:</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 390.15pt;padding: 0cm 5.4pt;vertical-align: top;">
                <p style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">if delivered by hand: on the day of delivery;</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 390.15pt;padding: 0cm 5.4pt;vertical-align: top;">
                <p style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">if sent by post to the correct address: within 72 hours of posting;</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 390.15pt;padding: 0cm 5.4pt;vertical-align: top;">
                <p style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;margin-left:36.0pt;line-height:115%;font-size:16px;font-family:"Arial",sans-serif;'><span style="font-size:13px;line-height:115%;">If sent by e-mail to the address from which the receiving party has last sent e-mail: within 24 hours if the sender has received no notice of non-receipt.&nbsp;</span></p>
            </td>
        </tr>
    </tbody>
</table>
<ol class="decimal_type" style="list-style-type: undefined;">
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">In the event of a dispute between the parties to this agreement, then they undertake to attempt to settle the dispute by engaging in good faith with the other in a process of mediation before commencing arbitration or litigation.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">So far as the law permits, and unless otherwise stated, this agreement does not give any right to any third party.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Neither party shall be liable for any failure or delay in performance of this agreement that is caused by circumstances beyond its reasonable control.</span></li>
    <li><span style="line-height:115%;font-family:Arial;font-size:13px;">Governing Law set out in the Definitions shall govern the validity, construction and performance of this agreement and you agree that any dispute arising from it shall be litigated in accordance with the Governing Law<span style='font-family:"Arial",sans-serif;color:blue;'><span style="line-height:115%;font-size:13px;">.</span></span></span></li>
</ol>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;color:red;'>&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;text-align:center;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>WEBSITE TERMS OF USE</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Rotate Technology Pty Ltd ABN 97 655 460 307<span style="color:red;">&nbsp;</span>(<strong>we</strong>, <strong>our</strong> or <strong>us</strong>) operates this website (<strong>Site</strong>). The domain address of the Site is:&nbsp;</span><a href="http://www.portae.com.au"><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>www.portae.com.au</span></a><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>&nbsp;<span style="color:red;">&nbsp;</span>and it may also be available through other addresses or channels.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>You agree to be bound by these Terms&nbsp;</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>By using our Site, you agree to be bound by these Site terms of use (<strong>Terms</strong>) and the Privacy Policy available on our Site. Please read these Terms and if you don&rsquo;t agree to them, then you should stop using our Site at once.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>When we can change these Terms</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We may change these Terms and anytime by publishing the varied terms on our Site. Make sure you come back and check the Terms on a regular basis to ensure you are up to date with the current Terms.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Changes to the Site</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Materials and information on this Site (<strong>Content</strong>) are also subject to change without notice. While we try to keep our Site current, we do not make any promises or undertake to keep our Site up-to-date and are not liable if any Content is inaccurate or out-of-date.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>The way in which you use the Site</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>You have no ownership in the Site. We own the Site and grant you a non-exclusive, royalty-free, revocable, worldwide, non-transferable licence to use the Site.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>You may not use the Site in any other way without our agreement in writing. All other uses of this Site must be in accordance with these Terms.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We do not permit you to:</span></p>
<ul style="list-style-type: disc;margin-left:29.35px;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>copy Content or any other details on our Site;&nbsp;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>use or copy our Site or Content in any way that competes with our business; or</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>breach our copyright or other intellectual property in the Site.</span></li>
</ul>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Behaviour on the Site</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>When you use our Site, we expect you to abide by a clear standard of behaviour. You must not do, or attempt to do anything:</span></p>
<ul style="list-style-type: disc;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>that is unlawful;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>prohibited by law</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>we would reasonably consider inappropriate; or</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>that might bring our Site or us into disrepute.&nbsp;</span></li>
</ul>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>This includes (without limitation):</span></p>
<ol style="list-style-type: undefined;margin-left:39.65px;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>anything that would breach the privacy of an individual;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>using our Site to defame, harass, threaten, menace or offend any person;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>interfering with any user using our Site;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>tampering with or modifying our Site;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>intentionally transmitting viruses to our Site;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>intentionally transmitting disabling or damaging features to our Site;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>interfering with our Site, including the use of Trojan horses, viruses, piracy or programming routines that may damage our Site;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>using our Site to send unsolicited email messages; or</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>assisting a third party to do any of the above.</span></li>
</ol>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Information only</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>The content on our Site provides a summary and general overview of our business and the things we do. The information we provide does not create a client relationship with you. While the information may be helpful to you, it is not intended to be comprehensive or specific, and we do not have any obligation to you in this regard.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Disclaimer</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We use reasonable commercial efforts to ensure the accuracy and completeness of the Content on our Site. However, to the maximum extent permitted by law, we make no representation, warranty or guarantee with respect to the Content or the likely outcomes you will get if you action the information on our Site and apply it to your situation or life. You should always get professional advice about your circumstances from an appropriate professional.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Intellectual Property rights</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Unless we state otherwise on the Site, we own or licence all rights, title and interest (including intellectual property rights) in our Site and Content.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Your use of our Site and your use of and access to the Content does not grant to you or transfer any rights, title or interest in relation to our Site or our Content. You must not:</span></p>
<ol style="list-style-type: undefined;margin-left:39.65px;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>copy or use any Content from our Site (in whole or part);</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate any Content to any third party; or</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>breach any intellectual property rights connected with our Site or our Content, including (without limitation) altering or modifying any of our Content, causing any of our Content to be framed or embedded in another website or platform, or creating derivative works from our Content.</span></li>
</ol>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Third party sites</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Our Site may contain links to websites operated by third parties (Third Party Sites). Unless stated on our Site, we are not responsible for the content on Third Party Sites. Further, we do not control, endorse or approve any Third Party Sites.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Content you upload to our Site</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We encourage you to interact with our Site. We may permit you to post, upload, publish, submit or send (<strong>upload</strong>) information and content to our Site (<strong>User Content</strong>).&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>If you upload User Content to our Site, you grant us a worldwide, irrevocable, perpetual, non-exclusive, transferable, royalty-free licence for the User Content. This means we are able to use, view, copy, adapt, modify, distribute, licence, transfer, communicate, display, publicly perform, transmit, stream, broadcast, access, or otherwise use the User Content on, through or by means of our Site.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>You agree that you are responsible for all User Content that you upload and warrant that:</span></p>
<ol style="list-style-type: undefined;margin-left:39.65px;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>you are either the sole and exclusive owner of all User Content or you have all rights, licences, consents and releases that are necessary to grant to us the rights in the User Content (as contemplated by these Terms); and</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>the User Content, your upload of the User Content or our use of it on, through or by means of our Site will infringe, misappropriate or violate a third party&rsquo;s intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.</span></li>
</ol>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We do not endorse or approve, and are not responsible for, any User Content. We may, at any time remove any User Content you upload at our discretion.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Warranties and disclaimers</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>To the maximum extent permitted by law, we make no representations or warranties about our Site or Content, including (without limitation) that:</span></p>
<ol style="list-style-type: undefined;margin-left:39.65px;">
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>it is complete, accurate, reliable, up-to-date and suitable for any particular purpose;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>you will have uninterrupted access;</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>it will be error-free or free from viruses; or</span></li>
    <li><span style='line-height:115%;font-family:"Arial",sans-serif;font-size:13px;'>our Site will be secure.</span></li>
</ol>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>You read, use and act on our Site and our Content at your own risk.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Limited Liability</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>To the maximum extent permitted by law, we are not responsible for any loss, damage or expense, however it arises, whether direct or indirect and/or whether present, unascertained, future or contingent (<strong>Liability</strong>) suffered by you or any third party, arising from or in connection with your use of our Site and/or our Content and/or any inaccessibility of, interruption to or outage of our Site and/or any loss or corruption of data and/or the fact that our Content is incorrect, incomplete or out-of-date.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Indemnity</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>To the maximum extent permitted by law, you must indemnify us, and hold us harmless, against any Liability suffered or incurred by us arising from or in connection with your use of our Site or any breach of these Terms or any applicable laws by you. This indemnity is a continuing obligation, independent from the other obligation under these Terms, and continues after these Terms end. It is not necessary for us to suffer or incur any Liability before enforcing a right of indemnity under these Terms.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Removing our Site (or your access to it)</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>We may, at any time and without notice to you, discontinue our Site, in whole or in part. We may also exclude any person (including you) from using our Site, at any time at our discretion. We are not responsible for any loss, damage or Liability you may suffer arising from or in connection with any such discontinuance or exclusion.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Termination</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>These Terms are effective until terminated by us, which we may do at any time and without notice to you. In the event of termination, all restrictions imposed on you by these Terms and limitations of liability set out in these Terms will survive.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>What happens if part of these Terms is not right?</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>If a provision of these Terms is held to be void, invalid, illegal or unenforceable, that provision must be read down as narrowly as necessary to allow it to be valid or enforceable. If it is not possible to read down a provision (in whole or in part), that provision (or that part of that provision) is severed from these Terms without affecting the validity or enforceability of the remainder of that provision or the other provisions in these Terms.</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>The law that applies to these Terms</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>The laws of Queensland, Australia, govern these Terms. If you access our Site throughout Australia or overseas, we make no representation that our Site complies with the laws (including intellectual property laws) of any State outside Queensland<span style="color:red;">&nbsp;</span>and/or country outside Australia. If you access our Site from outside Australia, you do so at your own risk and are responsible for complying with the laws of the jurisdiction where you access our Site.&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>For any questions and notices, please contact us at:</span></strong></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Rotate Technology Pty Ltd ABN 97 655 460 307&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Email: info@portae.com.au</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>Last update</span></strong><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>: 14 April 2023</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
<p style='margin:0cm;font-size:16px;font-family:"Cambria",serif;margin-bottom:6.0pt;line-height:115%;'><span style='font-size:13px;line-height:115%;font-family:"Arial",sans-serif;color:red;'>&nbsp;</span></p>
<div id="highlighter--hover-tools" style="display: none;">
    <div id="highlighter--hover-tools--container">
        <div class="highlighter--icon highlighter--icon-copy" title="Copy"><br></div>
        <div class="highlighter--icon highlighter--icon-change-color" title="Change Color"><br></div>
        <div class="highlighter--icon highlighter--icon-delete" title="Delete"><br></div>
    </div>
</div>