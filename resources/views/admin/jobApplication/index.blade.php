@extends('admin.layouts.main')

@section('content')
<div class="page">
  <div class="page-header">
    <h1 class="page-title">{{ __('admin.job-application-page-title') }}</h1>
    <ol class="breadcrumb">
      {{-- <li class="breadcrumb-item"><a href="../index.html">Home</a></li>
      <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li>
      <li class="breadcrumb-item active">DataTables</li> --}}
    </ol>
  </div>
<!-- Content Wrapper. Contains page content -->
  <!-- Content Header (Page header) -->
  <div class="page-content">
    <!-- Panel Basic -->
    <div class="panel">
      <header class="panel-heading">
        <div class="panel-actions"></div>
        <h3 class="panel-title">  </h3>
      </header>
      <div class="">
      <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="tab-pane fade show active" id="custom-tabs-two-all" role="tabpanel" aria-labelledby="custom-tabs-two-all-tab">
            <!-- <div class="card"> -->
               <div class="card-body">
                      <br>
                      <table id="all-table" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Birthdate</th>
                            <th>Role</th>
                            <th>City</th>
                            <th>Country</th>
                          </tr>
                        </thead>
                        <tbody id="tablecontents" >

                        </tbody>
                      </table>
                    </div>
                  <!-- </div> -->
                </div>
              </div>
            <!-- </div> -->
            <!-- /.card-body -->
          </div>
        </div>

      <!-- /.col -->
    </div>
    <!-- /.row -->
      </div>
    </div>
  </div>
  <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

@stop
@section('footer_script')
<script type="text/javascript">
	$(document).ready(function(){

      $("#tablecontents").sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

      var dt = $('#all-table').DataTable({
        "aaSorting": [],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "destroy": true,
        "pageLength": 25,
            //"iDisplayLength": 5,
            //"lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            processing: true,
            serverSide: true,
            ajax: '{{url("admin-api/job-application/getAllData")}}',
            columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'birthdate', name: 'birthdate'},
            {data: 'role', name: 'role'},
            {data: 'city', name: 'city'},
            {data: 'country', name: 'country'},
            ]
          });
  });
</script>
@stop