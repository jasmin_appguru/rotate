
<div class="modal-dialog modal-lg" id="myinformationModal">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">{{$page}}</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body" id="informationModelBody">
      <section class="panel">
        <div class="card card-primary card-outline card-tabs">
              <div class="card-header p-0 pt-1 border-bottom-0">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#view">View Details</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" data-url="#" id="editDetails" href="#menu2">Edit Details</a></li>
            <li class="nav-item"><a href="javascript:;" class="nav-link" data-redirect="" id="deleteDetails"> Delete</a></li>
        </ul>
        </div>
          <div class="card-body">
            <div class="tab-content">
                <div id="view" class="tab-pane fade in active show"><br>

                  <div class="form-row">
                    <div class="form-group col-md-12 col-12">
                        <table class="table table-hover">
                          <tr>
                            <td><label>First Name</label></td>
                            <td style='word-break: break-word;' >{{$jobApplication->first_name}}</td>
                          </tr>
                          <tr>
                            <td><label>Last Name</label></td>
                            <td style='word-break: break-word;' >{{$jobApplication->last_name}}</td>
                          </tr>
                          <tr>
                            <td><label>Role</label></td>
                            <td style='word-break: break-word;' >{{$jobApplication->role}}</td>
                          </tr>
                          <tr>
                            <td><label>Birthdate</label></td>
                            <td style='word-break: break-word;' >@empty($jobApplication->birtdate) N/A @else {{Carbon\Carbon::createFromFormat('Y-m-d', $jobApplication->birthdate)->format('d M Y') }} @endEmpty</td>
                          </tr>
                          <tr>
                            <td><label>City</label></td>
                            <td style='word-break: break-word;' >{{$jobApplication->city}}</td>
                          </tr>
                          <tr>
                            <td><label>Country</label></td>
                            <td style='word-break: break-word;' >{{$jobApplication->country}}</td>
                          </tr>
                          <tr>
                            <td><label>Social Link</label></td>
                            <td style='word-break: break-word;'> @if($jobApplication->social_links) <a href="{{$jobApplication->social_links}}" target="_blank">{{$jobApplication->social_links}}</a> @else N/A @endif</td>
                          </tr>
                        </table>
                    </div>


                  </div>

                </div>

                <div id="menu2" class="tab-pane fade">
                  <form id="faqEditForm" action="{{ url('job-application/').'/'.$jobApplication->id }}" method="post" >
                    <div class="box-body">
                      @csrf
                      @method('PUT')
                        <div class="form-group" >
                          <label for="first_name" >First Name</label> <span class="text-danger" >*</span>
                          <input class="form-control" name="first_name" id="first_name" value="{{ $jobApplication->first_name }}">
                        </div>
                        <div class="form-group" >
                          <label for="last_name" >Last Name</label> <span class="text-danger" >*</span>
                          <input class="form-control" name="last_name" id="last_name" value="{{ $jobApplication->last_name }}">
                        </div>
                        <div class="form-group" >
                          <label for="role" >Role</label> <span class="text-danger" >*</span>
                          <input class="form-control" name="role" id="role" value="{{ $jobApplication->role }}">
                        </div>
                        <div class="form-group" >
                          <label for="city" >City</label> <span class="text-danger" >*</span>
                          <input class="form-control" name="city" id="city" value="{{ $jobApplication->city }}">
                        </div>
                        <div class="form-group" >
                          <label for="country" >Country</label> <span class="text-danger" >*</span>
                          <input class="form-control" name="country" id="country" value="{{ $jobApplication->country }}">
                        </div>
                        <div class="form-group" >
                          <label for="social_links" >Social Links</label> <span class="text-danger" >*</span>
                          <input type="url" class="form-control" name="social_links" id="social_links" value="{{ $jobApplication->social_links }}">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw" ></i>Save</button>
                      </div>
                    </div>
                  </form>
                  <!-- /.box-body -->
                </div>

              </div>
            </div>
          </div>
      </section>
    </div>
  </div>
</div>

<!-- /.delete Model-open -->
<div class="modal fade in" id="modalConfirmDelete" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm modal-sm-new">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" onclick="$('#modalConfirmDelete').modal('hide');">×</button>
      </div>
      <div class="modal-body text-center">
        <p class="delete-conform-p">Are you sure you want to delete?</p>
      </div>

      <div class="modal-footer">
        <form action="{{ url('job-application/').'/'.$jobApplication->id}}" method="post" >
          @method('delete')
          @csrf
          <button type="submit" class="btn btn-danger btn-flat">Confirm Delete</button>
        </form>
        <button type="button" class="btn btn-default btn-flat"  onclick="$('#modalConfirmDelete').modal('hide');">Close</button>
      </div>

    </div>
  </div>
</div>
<!-- /.delete Model-close -->
{{ Html::script('admin_theme/assets/dist/js/custom_validation.js') }}
<script type="text/javascript">
  $('#question1').on('keyup',function(){
    let val = $('#question1').val();
    $('#question1').val(val.trimStart());
  });
  $('#answer1').on('keyup',function(){
    let val = $('#answer1').val();
    $('#answer1').val(val.trimStart());
  })
</script>