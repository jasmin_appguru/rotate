<div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-item @if(in_array('dashboard', Request::segments())) active @endif">
              <a href="{{route('admin.dashboard')}}">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.dashboard-menu') }}</span>
              </a>
            </li>
            

            <li class="site-menu-item @if(in_array('users', Request::segments())) active @endif">
              <a href="{{route('users.index')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.users-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('referral-users', Request::segments())) active @endif">
              <a href="{{route('referral-users.index')}}">
                <i class="site-menu-icon icon md-receipt"></i>
                <span class="site-menu-title">{{ __('admin.referral-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('raffle-users', Request::segments())) active @endif">
              <a href="{{route('raffle-users.index')}}">
                <i class="site-menu-icon icon md-receipt"></i>
                <span class="site-menu-title">{{ __('admin.raffle-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item has-sub @if(in_array('brands', Request::segments())) active @endif">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-present-to-all" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.brand-menu') }}</span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item @if(in_array('request', Request::segments())) active @endif">
                  <a href="{{route('brand.List',["status"=>"request"])}}">
                    <span class="site-menu-title">Request</span>
                  </a>
                </li>
                <li class="site-menu-item @if(in_array('Approved', Request::segments())) active @endif">
                  <a href="{{route('brand.List',["status"=>"Approved"])}}">
                    <span class="site-menu-title">Approved</span>
                  </a>
                </li>
                <li class="site-menu-item @if(in_array('Rejected', Request::segments())) active @endif">
                  <a href="{{route('brand.List',["status"=>"Rejected"])}}">
                    <span class="site-menu-title">Rejected</span>
                  </a>
                </li>
              </ul> 
            </li>

            <li class="site-menu-item @if(in_array('reported-users', Request::segments())) active @endif">
              <a href="{{route('reported-users.index')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.users-reported-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('job-application', Request::segments())) active @endif">
              <a href="{{route('job-application.index')}}">
                <i class="site-menu-icon md-receipt" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.job-application-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('Notification', Request::segments())) active @endif">
              <a href="{{url('Notification')}}">
                <i class="site-menu-icon icon md-notifications"></i>
                <span class="site-menu-title">{{ __('admin.notification-menu') }}</span>
              </a>
            </li>

            <li class="site-menu-item @if(in_array('Affiliate', Request::segments())) active @endif">
              <a href="{{route('Affiliate.index')}}">
                <i class="site-menu-icon md-library" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.Affiliate-menu') }}</span>
              </a>
            </li>
            
            <li class="site-menu-item @if(in_array('Feedback', Request::segments())) active @endif">
              <a href="{{route('Feedback.index')}}">
                <i class="site-menu-icon md-comment-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.feedback-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('faq-management', Request::segments())) active @endif">
              <a href="{{route('faq-management.index')}}">
                <i class="site-menu-icon md-receipt" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.faq-menu') }}</span>
              </a>
            </li>

       

            {{-- <li class="site-menu-item @if(in_array('Notification', Request::segments())) active @endif">
              <a href="{{url('admin/Notification')}}">
                <i class="site-menu-icon icon md-notifications"></i>
                <span class="site-menu-title">{{ __('admin.notification-menu') }}</span>
              </a>
            </li>
             --}}


              
              <li class="site-menu-item @if(in_array('ApiMessages', Request::segments())) active @endif">
                <a href="{{url('ApiMessages')}}">
                  <i class="site-menu-icon icon md-receipt"></i>
                  <span class="site-menu-title">{{ __('admin.api-messages-menu') }}</span>
                </a>
              </li>

          </ul>
        </div>
      </div>
    </div>
  </div>
