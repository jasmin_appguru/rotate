@extends('admin.layouts.main')
@section('content')

  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">{{ __('admin.users-reported-page-title') }}</h1>
      <ol class="breadcrumb">
        {{-- <li class="breadcrumb-item"><a href="../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li>
        <li class="breadcrumb-item active">DataTables</li> --}}
      </ol>
    </div>

    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">  </h3>
        </header>
        <div class="panel-body app-contacts">
          {{-- <div class="panel-heading">
              <div class="text-right">
                  <a href="{{ url('/admin/exportAgentData') }}" class="btn btn-flat btn-success">Download CSV</a>
              </div>
          </div> --}}
          <div class="row">
            <div class="col-md-12 text-right">
              <div class="mb-15">
                <a href="{{ url('/reported-users/exportUsers') }}" class="btn btn-primary">Download CSV</a>
              </div>
            </div>
          </div>
          <div class="table-responsive">
          <table class="table table-hover dataTable table-striped w-full" id="data-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Reported By</th>
                <th>Reason</th>
                <th>Other Reason</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>

    <!-- Add Block User Form -->
    <div class="modal fade" id="blockUserModal" aria-hidden="true" aria-labelledby="blockUserModal"
      role="dialog" tabindex="-1">
      <div class="modal-dialog modal-simple">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
            <h4 class="modal-title" id='blockUser-modal-title'>Block User</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" id="userName" value=''>
            <form id="blockUserForm" >
              @csrf
              <div class="form-group">
              <label>Block Period : </label>
                <select name="block_period" id="block_period" class="form-control" required>
                    <option value="" disabled>Select Period</option>
                    <option value="{{User::BLOCK_FIFTHTEEN_DAYS}}" selected>15 Days</option>
                    <option value="{{User::BLOCK_ONE_MONTH}}"> 1 Month</option>
                    <option value="{{User::BLOCK_THREE_MONTH}}"> 3 Months </option>
                    <option value="{{User::BLOCK_SIX_MONTH}}"> 6 Months </option>
                    <option value="{{User::BLOCK_ONE_YEAR}}"> 1 Year </option>
                </select>
              </div>
            <div class="modal-footer">
              <button type="submit" id="submitBlockUserForm" class="btn btn-add" style="background-color: #6C863D;color: white;">Block</button>
              <button type="button" class="btn btn-cancel" data-dismiss="modal" style="background-color: white; color: black;">Cancel</button>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Add Block User Form -->
  </div>
  <!-- End Page -->
@stop

@section('footer_script')
<script>
    $(document).ready(function(){
        var dt = $('#data-table').DataTable({
            "aaSorting": [],
            "stateSave": false,
            "responsive": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "iDisplayLength": 100,
            "lengthMenu": [[10,100, 150, 200, -1], [10,100, 150, 200, "All"]],
            processing: true,
            serverSide: true,
            ajax: '{{url("admin-api/reported-users/getAllData")}}',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'reported_by', name: 'reported_by'},
                {data: 'reason', name: 'reason'},
                {data: 'other_reason', name: 'other_reason'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'},
            ]
        });
        $('#submitBlockUserForm').on('click', function(e) {
          e.preventDefault();
          var form= $("#blockUserForm");
          console.log(form.serialize())
          blockUrl = $('#blockUserForm').attr('action');
          userName = $('#userName').val();
          $('#blockUserModal').modal('hide');
          Swal.fire({
              icon: 'warning',
              title:'Rotate',
              html: "Are you sure you want to block <strong>"+userName+"</strong> ?",
              showCancelButton: true,
              confirmButtonText: 'Yes',
              confirmButtonColor: '#ff0000',
              cancelButtonText: 'Cancel',
              cancelButtonColor: '#808080',
              }).then((result) => {
              if (result.isConfirmed) {
                $.LoadingOverlay("show", {
                  image: "/themes/loader.gif",
                });
                $.ajax({
                    type : "POST",
                    url : blockUrl,
                    dataType: 'json',
                    data: $('#blockUserForm').serialize(),
                    success : function(response) {
                        $.LoadingOverlay("hide");
                        location.reload();
                    }
                });
              }
          })
        });
    });

    function removeUser(deleteUrl,userName){
      $("#dataInfoModal").modal('hide');
      Swal.fire({
          icon: 'warning',
          title:'Rotate',
          html: "Are you sure you want to delete <strong>"+userName+"</strong> ?",
          showCancelButton: true,
          confirmButtonText: 'Yes',
          confirmButtonColor: '#ff0000',
          cancelButtonText: 'Cancel',
          cancelButtonColor: '#808080',
          }).then((result) => {
          if (result.isConfirmed) {
              $.LoadingOverlay("show", {
                image: "/themes/loader.gif",
              });
              $.ajax({
                  type : "DELETE",
                  url : deleteUrl,
                  dataType: 'json',
                  headers: {
                      'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                  },
                  success : function(response) {
                      $.LoadingOverlay("hide");
                      location.reload();
                  }
              });
          }
      })
    }
    function changeStatus(updateUrl,action,userName){
        Swal.fire({
            icon: 'warning',
            title:'Rotate',
            html: "Are you sure you want to make <strong>"+userName+"</strong> "+action+" ?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            confirmButtonColor: '#ff0000',
            cancelButtonText: 'Cancel',
            cancelButtonColor: '#808080',
            }).then((result) => {
            if (result.isConfirmed) {
                $.LoadingOverlay("show", {
                  image: "/themes/loader.gif",
                });
                $.ajax({
                    type : "POST",
                    url : updateUrl,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    },
                    success : function(response) {
                        $.LoadingOverlay("hide");
                        location.reload();
                    }
                });
            }
        })
    }
    function showBlockUserModal(url,userName){
      $('#blockUserForm').attr('action',url);
      $('#userName').val(userName);
      $('#blockUser-modal-title').html('Block User: ' + userName);
      $('#blockUserModal').modal('show');
    }
</script>
@stop
