@extends('admin.layouts.main')
@section('content')

  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">{{ __('admin.users-page-title') }}</h1>
      <ol class="breadcrumb">
        {{-- <li class="breadcrumb-item"><a href="../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li>
        <li class="breadcrumb-item active">DataTables</li> --}}
      </ol>
    </div>

    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">  </h3>
        </header>
        <div class="panel-body app-contacts">
          {{-- <div class="panel-heading">
              <div class="text-right">
                  <a href="{{ url('/admin/exportAgentData') }}" class="btn btn-flat btn-success">Download CSV</a>
              </div>
          </div> --}}
          <div class="row">
            <div class="col-md-12 text-right">
              <div class="mb-15">
                <a href="{{ url('/exportUsers') }}" class="btn btn-primary">Download CSV</a>
              </div>
            </div>
          </div>
          <div class="table-responsive">
          <table class="table table-hover dataTable table-striped w-full" id="data-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>
  <!-- End Page -->
@stop

@section('footer_script')
<script>
    $(document).ready(function(){
      // alert("hedrdsf");
        var dt = $('#data-table').DataTable({
            "aaSorting": [],
            "stateSave": false,
            "responsive": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "iDisplayLength": 100,
            "lengthMenu": [[10,100, 150, 200, -1], [10,100, 150, 200, "All"]],
            processing: true,
            serverSide: true,
            ajax: '{{url("admin-api/users/getAllData")}}',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'status', name: 'status'},
            ]
        });
    });

    function removeUser(deleteUrl,userName,openModal= false){
      $("#dataInfoModal").modal('hide');
      Swal.fire({
          icon: 'warning',
          title:'Rotate',
          html: "Are you sure you want to delete <strong>"+userName+"</strong> ?",
          showCancelButton: true,
          confirmButtonText: 'Yes',
          confirmButtonColor: '#ff0000',
          cancelButtonText: 'Cancel',
          cancelButtonColor: '#808080',
          }).then((result) => {
          if (result.isConfirmed) {
              $.LoadingOverlay("show", {
                image: "/themes/loader.gif",
              });
              $.ajax({
                  type : "DELETE",
                  url : deleteUrl,
                  dataType: 'json',
                  headers: {
                      'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                  },
                  success : function(response) {
                      $.LoadingOverlay("hide");
                      location.reload();
                  }
              });
          } else {
            if(openModal){
              $("#dataInfoModal").modal('show');
            }
          }
      })
    }
</script>

@stop
