@extends('admin.layouts.main')
@section('content')


<div class="page">
   <div class="page-header">
      <h1 class="page-title">{{ __('admin.dashboard-page-title') }}</h1>
    </div>
    <div class="page-content container-fluid">
      <div class="container-fluid p-3"  >
          <form action="{{ route('filter') }}" method="POST">
            <div class="row" >
              @csrf
              <div class="col-sm-3">
                <div class="form-group">
                  <select name="userRange" id="globalRange" class="form-control globalRange userGlobalRange" required>
                      <option value="all">All</option>
                      <option value="daily" {{ $userRange == "daily" ? "selected" : "" }} >Daily</option>
                      <option value="weekly"  {{ $userRange == "weekly" ? "selected" : "" }}>Weekly</option>
                      <option value="monthly"  {{ $userRange == "monthly" ? "selected" : "" }}>Monthly</option>
                      <option value="quarterly"  {{ $userRange == "quarterly" ? "selected" : "" }}>Quarterly</option>
                      <option value="yearly"  {{ $userRange == "yearly" ? "selected" : "" }}>Yearly</option>
                      <option value="custom"  {{ $userRange == "custom" ? "selected" : "" }}>Custom Date A - B </option>
                      <option value="todate"  {{ $userRange == "todate" ? "selected" : "" }}>To Date</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <select name="userAge" id="userAge" class="form-control userAge">
                      <option value="{{ $userAge == "" ? "selected" : "" }}">User Age</option>
                      <option value="all">All</option>
                      <option value="13_18" {{ $userAge == "13_18" ? "selected" : "" }} >13 - 18</option>
                      <option value="19_24"  {{ $userAge == "19_24" ? "selected" : "" }}>19 - 24</option>
                      <option value="25_34"  {{ $userAge == "25_34" ? "selected" : "" }}>25 - 34</option>
                      <option value="35_45"  {{ $userAge == "35_45" ? "selected" : "" }}>35 - 45</option>
                      <option value="45_plus"  {{ $userAge == "45_plus" ? "selected" : "" }}>45+</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group" style="">
                    <button class="btn btn-primary rotate-color-combo" id="applyFilter" ><i class="fa fa-filter fa-fw" ></i>Filter</button>
                </div>
              </div>
              <div class="col-sm-3 userGlobalRangecustome" style="display: {{ $userRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">Start Date</label>
                  <input type="date" name="userstartDate" class="form-control" value="{{ $userstartDate ? $userstartDate : "" }}">
                </div>
              </div>
              <div class="col-sm-3 userGlobalRangecustome" style="display: {{ $userRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="userendDate" class="form-control" value="{{ $userendDate ? $userendDate : "" }}">
                </div>
              </div>
              <div class="col-sm-3 usertodate" style="display: {{ $userRange == "todate" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="userTodate" class="form-control" value="{{ $userTodate ?? "" }}">
                </div>
              </div>
            </div>
      </div>
      <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="row">
            @foreach($boxes['users'] as $key => $value)
              <div class="col-lg-3">
                  <div class="card card-block p-25 rotate-color-combo">
                    <div class="counter counter-lg">
                      <span class="counter-number user-counter">{{ $value['value']}}</span>
                      <div class="counter-label text-uppercase">{{ $value['text']}}</div>
                    </div>
                  </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      <div class="container-fluid p-3"  >
            <div class="row" >
              <div class="col-sm-3">
                <div class="form-group">
                  <select name="catRange" id="globalRange" class="form-control globalRange catGlobalRange" required>
                      <option value="all">All</option>
                      <option value="daily" {{ $catRange == "daily" ? "selected" : "" }} >Daily</option>
                      <option value="weekly"  {{ $catRange == "weekly" ? "selected" : "" }}>Weekly</option>
                      <option value="monthly"  {{ $catRange == "monthly" ? "selected" : "" }}>Monthly</option>
                      <option value="quarterly"  {{ $catRange == "quarterly" ? "selected" : "" }}>Quarterly</option>
                      <option value="yearly"  {{ $catRange == "yearly" ? "selected" : "" }}>Yearly</option>
                      <option value="custom"  {{ $catRange == "custom" ? "selected" : "" }}>Custom Date A - B </option>
                      <option value="todate"  {{ $catRange == "todate" ? "selected" : "" }}>To Date</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <select name="currentCategory" id="currentCategory" class="form-control currentCategory">
                      <option value="{{ $currentCategory == "" ? "selected" : "" }}">Category</option>
                      <option value="all">All</option>
                      @foreach ($categories as $cat)
                        <option value="{{$cat->id}}" {{ $currentCategory == $cat->id ? "selected" : "" }} >{{$cat->name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group" style="">
                    <button class="btn btn-primary rotate-color-combo" id="applyFilter" ><i class="fa fa-filter fa-fw" ></i>Filter</button>
                </div>
              </div>


              <div class="col-sm-3 catGlobalRangecustome" style="display: {{ $catRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">Start Date</label>
                  <input type="date" name="catstartDate" class="form-control" value="{{ $catstartDate ? $catstartDate : "" }}">
                </div>
              </div>

              <div class="col-sm-3 catGlobalRangecustome" style="display: {{ $catRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="catendDate" class="form-control" value="{{ $catendDate ? $catendDate : "" }}">
                </div>
              </div>

              <div class="col-sm-3 cattodate" style="display: {{ $catRange == "todate" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="catToDate" class="form-control" value="{{ $catToDate ?? "" }}">
                </div>
              </div>
            </div>
      </div>
      <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="row">
              @foreach($boxes['category'] as $key => $value)
              <div class="col-lg-3">
                  <div class="card card-block p-25 rotate-color-combo">
                    <div class="counter counter-lg">
                      <span class="counter-number user-counter">{{ $value['value']}}</span>
                      <div class="counter-label text-uppercase">{{ $value['text']}}</div>
                    </div>
                  </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      <div class="container-fluid p-3"  >
            <div class="row" >
              <div class="col-sm-3">
                <div class="form-group">
                  <select name="otherRange" id="globalRange" class="form-control globalRange otherGlobalRange" required>
                      <option value="all">All</option>
                      <option value="daily" {{ $otherRange == "daily" ? "selected" : "" }} >Daily</option>
                      <option value="weekly"  {{ $otherRange == "weekly" ? "selected" : "" }}>Weekly</option>
                      <option value="monthly"  {{ $otherRange == "monthly" ? "selected" : "" }}>Monthly</option>
                      <option value="quarterly"  {{ $otherRange == "quarterly" ? "selected" : "" }}>Quarterly</option>
                      <option value="yearly"  {{ $otherRange == "yearly" ? "selected" : "" }}>Yearly</option>
                      <option value="custom"  {{ $otherRange == "custom" ? "selected" : "" }}>Custom Date A - B </option>
                      <option value="todate"  {{ $otherRange == "todate" ? "selected" : "" }}>To Date</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-9">
                <div class="form-group" style="">
                    <button class="btn btn-primary rotate-color-combo" id="applyFilter" ><i class="fa fa-filter fa-fw" ></i>Filter</button>
                </div>
              </div>
              <div class="col-sm-3 otherGlobalRangecustome" style="display: {{ $otherRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">Start Date</label>
                  <input type="date" name="otherstartDate" class="form-control" value="{{ $otherstartDate ? $otherstartDate : "" }}">
                </div>
              </div>
              <div class="col-sm-3 otherGlobalRangecustome" style="display: {{ $otherRange == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="otherendDate" class="form-control" value="{{ $otherendDate ? $otherendDate : "" }}">
                </div>
              </div>
              <div class="col-sm-3 othertodate" style="display: {{ $otherRange == "todate" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="otherToDate" class="form-control" value="{{ $otherToDate ?? "" }}">
                </div>
              </div>
            </div>
          </form>
      </div>
      <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="row">
               @foreach($boxes['other'] as $key => $value)
              <div class="col-lg-3">
                  <div class="card card-block p-25 rotate-color-combo">
                    <div class="counter counter-lg">
                      <span class="counter-number user-counter">{{ $value['value']}}</span>
                      <div class="counter-label text-uppercase">{{ $value['text']}}</div>
                    </div>
                  </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>
@stop

@section('footer_script')
<script>
    $(document).ready(function(){
      $(".globalRange").change(function(){
          if ($(this).hasClass('userGlobalRange')){
            className = 'userGlobalRangecustome'
            todateClassName = 'usertodate'
          }
          if ($(this).hasClass('otherGlobalRange')){
            className = 'otherGlobalRangecustome'
            todateClassName = 'othertodate'
          }
          if ($(this).hasClass('catGlobalRange')){
            className = 'catGlobalRangecustome'
            todateClassName = 'cattodate'
          }
          if($(this).val()=="custom") {
            $("."+className).show();
          } else {
            $("."+className).hide();
          }
          if($(this).val()=="todate") {
            $("."+todateClassName).show();
          } else {
            $("."+todateClassName).hide();
          }
      });
    });
</script>
@stop
