<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RackFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'user_id' => User::all()->random()->id,
            'sub_category_id' => Category::where('parent_id',"!=",NULL)->get()->random()->id,
            'category_id' => Category::where('parent_id',"=",NULL)->get()->random()->id,
        ];
    }
}
