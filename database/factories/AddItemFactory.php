<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AddItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        	//captions
            //brand_Id 
            //shopLink
            //clothCountry
            //shoesCountry
            //clothData
            //shoesData
            //style
            //is_sell	
            //price
            //priceType
            //subCategoryId
            //rackId
            //category_id 
            //user_id
            //visibility
            
        return [
            'captions' => $faker->text,
            'user_id' => factory(\App\Models\User::class),
            'brand_Id' => factory(\App\Models\Brand::class),
        ];
    }
}
