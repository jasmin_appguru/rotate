<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Notification::class;
    
    public function definition()
    {
        return [
            'name' => $this->faker->text,
            'user_id' => User::all()->random()->id
        ];
    }
}
