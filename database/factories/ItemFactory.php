<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\Rack;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Item::class;


    public function definition()
    {
        
            //subCategoryId
            //rackId
            //category_id 
            //user_id
            //visibility
        $data= [
            'captions' => $this->faker->text,
            'user_id' => User::all()->random()->id,
            'brand_Id' => Brand::all()->random()->id,
            'shopLink' => $this->faker->url,
            'clothCountry' => 1,
            'shoesCountry' => 1,
            'clothData' => "[1,2,3]",
            'shoesData' => "[1,2,3]",
            'color' => "[1,2,3]",
            'is_sell' => "0",
            'price' => $this->faker->randomNumber(3),
            'priceType' => $this->faker->randomElement(['3','2','1']),
            'subCategoryId' => Category::where('parent_id',"!=",NULL)->get()->random()->id,
            'rackId' => Rack::all()->random()->id,
            'category_id' => Category::where('parent_id',"=",NULL)->get()->random()->id,
            'latitude' => $this->faker->latitude(20,25),
            'longitude' =>$this->faker->longitude(10,74),
        ];

        return $data;
    }
}
