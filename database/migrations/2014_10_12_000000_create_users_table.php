<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('image')->nullable();
            $table->string('user_name')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('mobile_number')->nullable();
            $table->date('dob')->nullable();
            $table->integer('age')->default(0);
            $table->integer('is_profile_setup')->default(0)->comment("0=Not Set, 1=SetUp");
            $table->tinyInteger('device_type')->comment("0 = Web, 1 = iOS, 2 = Android");
            $table->string('device_token')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('location')->nullable();
            $table->string('clothCountry')->nullable();
            $table->string('shoesCountry')->nullable();
            $table->text('clothData')->nullable();
            $table->text('shoesData')->nullable();
            $table->string('referralCode')->unique();
            $table->string('joinReferralCode')->nullable();
            $table->tinyInteger('visibility')->default(1)->comment("1 = Public ,2 = Privet");
            $table->string('status')->default('Active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
