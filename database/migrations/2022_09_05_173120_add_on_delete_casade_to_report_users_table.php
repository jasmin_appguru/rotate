<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToReportUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_users', function (Blueprint $table) {
            DB::statement('alter table report_users drop FOREIGN KEY report_users_user_id_foreign;');
            DB::statement('alter table report_users add constraint report_users_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_users', function (Blueprint $table) {
            DB::statement('alter table report_users drop FOREIGN KEY report_users_user_id_foreign;');
            DB::statement('alter table report_users add constraint report_users_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
