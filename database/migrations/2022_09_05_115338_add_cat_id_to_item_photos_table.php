<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatIdToItemPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_photos', function (Blueprint $table) {
            $table->unsignedBigInteger('rack_id')->nullable(); 
            $table->foreign('rack_id')->references('id')->on('racks');
            $table->unsignedBigInteger('category_id')->nullable(); 
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_photos', function (Blueprint $table) {
            $table->dropForeign(['category_id','rack_id']);
            $table->dropColumn('category_id');
            $table->dropColumn('rack_id');
            // $table->dropColumn('category_id');
        });
    }
}
