<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('admins')->insert(
            array(
                'name' => 'Admin',
                'mobile' => '+61 7485 5246',
                'password' => '$2y$10$8pRZzR2pVIkBWN3HnPpGb.IB0jy6m.bbXY3qrkBkD9U9ldcVU4tgi',
                'email' => 'admin@yopmail.com'
            )
        );
        //Default Password
        //admin@yopmail.com
        //123123
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
