<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToArchiveItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archive_items', function (Blueprint $table) {
            DB::statement('alter table archive_items drop FOREIGN KEY archive_items_user_id_foreign;');
            DB::statement('alter table archive_items add constraint archive_items_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archive_items', function (Blueprint $table) {
            DB::statement('alter table archive_items drop FOREIGN KEY archive_items_user_id_foreign;');
            DB::statement('alter table archive_items add constraint archive_items_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
