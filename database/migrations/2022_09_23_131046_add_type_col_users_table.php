<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeColUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_notifications', function (Blueprint $table) {
            $table->tinyInteger('type')->default(1)->comment('1 : Chat 2 : Admin 3 : Happy Birthday 4 : Favorite item available for Sale. 5 : Referral Joined 6 : Reminder item  7 : new item 8 : Reminder posting 9 : Referring ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_notifications', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
