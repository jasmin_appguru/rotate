<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function (Blueprint $table) {
            DB::statement('alter table notifications drop FOREIGN KEY notifications_user_id_foreign;');
            DB::statement('alter table notifications add constraint notifications_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function (Blueprint $table) {
            DB::statement('alter table notifications drop FOREIGN KEY notifications_user_id_foreign;');
            DB::statement('alter table notifications add constraint notifications_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
