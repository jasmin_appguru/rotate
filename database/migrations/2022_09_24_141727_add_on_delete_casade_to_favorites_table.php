<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('favorites', function (Blueprint $table) {
            DB::statement('alter table favorites drop FOREIGN KEY favorites_user_id_foreign;');
            DB::statement('alter table favorites add constraint favorites_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('favorites', function (Blueprint $table) {
            DB::statement('alter table favorites drop FOREIGN KEY favorites_user_id_foreign;');
            DB::statement('alter table favorites add constraint favorites_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
