<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToItemPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_photos', function (Blueprint $table) {
            DB::statement('alter table item_photos drop FOREIGN KEY item_photos_user_id_foreign;');
            DB::statement('alter table item_photos add constraint item_photos_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_photos', function (Blueprint $table) {
            DB::statement('alter table item_photos drop FOREIGN KEY item_photos_user_id_foreign;');
            DB::statement('alter table item_photos add constraint item_photos_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
