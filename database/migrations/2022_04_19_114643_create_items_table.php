<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->text('captions');
            $table->unsignedBigInteger('brand_Id')->nullable(); 
            $table->foreign('brand_Id')->references('id')->on('brands');
            $table->string('shopLink');
            $table->string('clothCountry');
            $table->string('shoesCountry');
            $table->text('clothData');
            $table->text('shoesData');
            $table->string('style');
            $table->tinyInteger('is_sell')->default(0)->comment("0: Not for Sell 1: For Sell");
            $table->decimal('price',8,2);
            $table->string('priceType');
            $table->string('subCategoryId');
            $table->string('rackId');
            $table->string('latitude');
            $table->string('longitude');
            $table->unsignedBigInteger('category_id')->nullable(); 
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('user_id')->nullable(); 
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('visibility',['0','1'])->default(0)->comment("0: Visible 1: Hidden");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
