<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToApplicationNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_notifications', function (Blueprint $table) {
            DB::statement('alter table application_notifications drop FOREIGN KEY application_notifications_user_id_foreign;');
            DB::statement('alter table application_notifications add constraint application_notifications_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_notifications', function (Blueprint $table) {
            DB::statement('alter table application_notifications drop FOREIGN KEY application_notifications_user_id_foreign;');
            DB::statement('alter table application_notifications add constraint application_notifications_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
