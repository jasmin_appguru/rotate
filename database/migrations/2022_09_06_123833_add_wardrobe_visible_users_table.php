<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWardrobeVisibleUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('wardrobe_visibility')->comment('0: Private 1: Public')->default(1);
            $table->integer('radius')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->tinyInteger('wardrobe_visibility')->comment('0 = Private 1 = Public')->default(0);
            $table->dropColumn('wardrobe_visibility');
            $table->dropColumn('radius');
        });
    }
}
