<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feedback', function (Blueprint $table) {
            DB::statement('alter table feedback drop FOREIGN KEY feedback_user_id_foreign;');
            DB::statement('alter table feedback add constraint feedback_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
            DB::statement('alter table feedback drop FOREIGN KEY feedback_user_id_foreign;');
            DB::statement('alter table feedback add constraint feedback_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
