<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteCasadeToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            DB::statement('alter table items drop FOREIGN KEY items_user_id_foreign;');
            DB::statement('alter table items add constraint items_user_id_foreign
                foreign key (user_id)
                references users(id)
                on delete cascade;'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            DB::statement('alter table items drop FOREIGN KEY items_user_id_foreign;');
            DB::statement('alter table items add constraint items_user_id_foreign
                foreign key (user_id)
                references users(id);'
            );
        });
    }
}
