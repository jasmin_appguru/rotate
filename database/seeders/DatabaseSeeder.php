<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Artisan::call('passport:install');
       // \App\Models\User::factory(10)->create();
        Category::create([
            "name" => 'clothing'
        ]);
        Category::create([
            "name" => 'accessories'
        ]);
        Category::create([
            "name" => 'shoes'
        ]);
        \App\Models\Category::factory(15)->create();
        \App\Models\Brand::factory(20)->create();
        //\App\Models\Rack::factory(20)->create();
       // \App\Models\Item::factory(500)->create();
    }
}
