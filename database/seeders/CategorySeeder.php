<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $main_cat=config('portae_admin.DefaultMainCat');
        //App Side userd Static IP 
        foreach($main_cat as $mcat)
        {
        $cat=Category::create([
                "name" => $mcat['name']
            ]);
        }
        foreach($main_cat as $mcat)
        {
            foreach($mcat['sub'] as $sub)
            {
                $Subcat=Category::create([
                    "name" => $sub,
                    "parent_id" => $mcat['id'],
                ]);
            }
        }
    }
}
