<?php

namespace Database\Seeders;

use App\Models\Colors;
use Illuminate\Database\Seeder;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Colors::truncate();
        $colors = [
            'Black',
            'White',
            'Blue',
            'Green',
            'Yellow',
            'Gold',
            'Neutral',
            'Grey',
            'Silver',
            'Pink',
            'Purple',
            'Brown',
            'Red',
            'Orange',
            'Multi',
        ];
        for ($i = 0; $i < count($colors); $i++) {
            $colorsArray[] = [
                'name' => $colors[$i],
                'sequence' => $i+1,
            ];
        }
        Colors::insert($colorsArray);
    }
}
