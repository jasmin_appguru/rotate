<?php

return [
    'TWILIO_ENABLE' => env('TWILIO_ENABLE', 0),
    'TWILIO_SID' => env('TWILIO_SID', 0),
    'TWILIO_TOKEN' => env('TWILIO_TOKEN', 0),
    'TWILIO_FROM' => env('TWILIO_FROM', 0),
    'FCM_TOKEN' => env('FCM_KEY','0'),
    'MAIL' => [
        "HEADER_COLOR" => "#ecf0f1",
        "THEME_COLOR" => "#ecf0f1",
        "THEME_FONT_COLOR" => "#000",
    ],
    "DefaultMainCat" => 
    [
        [
            "name" => "clothing",
            "id"=>"1",
            "sub" => [
                "Dresses",
                "Tops",
                "Bottoms",
                "Outerwear",
                "Activewear & Loungewear",
                "Lingerie & Sleepwear",
                "Swimsuits & Coverups",
                "Sets & Jumpsuits",
            ]
        ],
        [
            "name" => "accessories",
            "id"=>"2",
            "sub" => [
                "Belts",
                "Hats & Hair Accessories",
                "Sunglasses & Eyewear",
                "Bags",
                "Jewellery",
                "Beauty",
                "Scarves & Glove"
                ]
            ],
            [
                "name" => "shoes",
                "id"=>"3",
                "sub" => [
                    "Flats",
                    "Heels",
                    "Boots"
                ]
        ]
    ]
];
