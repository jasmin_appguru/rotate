<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],


    'firebase' => [
    'api_key' => 'AIzaSyDFOGLo7nx3MYD0YK8VsC_MI3FgeQYL3Bo',
    'auth_domain' => 'rotate-ee668.firebaseapp.com',
    'storage_bucket' => 'rotate-ee668.appspot.com',
    'project_id' => 'rotate-ee668',
    'messaging_sender_id' => '814838661757',
    "appId" => "1:814838661757:web:dbe6a42d75797e777ee714",
    "measurementId" => "G-KNXM2FKJBE"
    ],
    // apiKey: "AIzaSyDFOGLo7nx3MYD0YK8VsC_MI3FgeQYL3Bo",
    // authDomain: "rotate-ee668.firebaseapp.com",
    // projectId: "rotate-ee668",
    // storageBucket: "rotate-ee668.appspot.com",
    // messagingSenderId: "814838661757",
    // appId: "1:814838661757:web:dbe6a42d75797e777ee714",
    // measurementId: "G-KNXM2FKJBE"

    


];
